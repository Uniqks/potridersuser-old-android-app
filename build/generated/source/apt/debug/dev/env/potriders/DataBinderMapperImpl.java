package dev.env.potriders;

import android.databinding.DataBinderMapper;
import android.databinding.DataBindingComponent;
import android.databinding.ViewDataBinding;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import dev.env.potriders.databinding.ActivityAdvancedSearchBindingImpl;
import dev.env.potriders.databinding.ActivityChooseOptionBindingImpl;
import dev.env.potriders.databinding.ActivityChooseOptionBindingSw600dpImpl;
import dev.env.potriders.databinding.ActivityChooseOptionBindingSw720dpImpl;
import dev.env.potriders.databinding.ActivityForgotPasswordBindingImpl;
import dev.env.potriders.databinding.ActivityLoginBindingImpl;
import dev.env.potriders.databinding.ActivityMainBindingImpl;
import dev.env.potriders.databinding.ActivityRegisterBindingImpl;
import dev.env.potriders.databinding.ActivitySearchBindingImpl;
import dev.env.potriders.databinding.ActivitySplaceAlertBindingImpl;
import dev.env.potriders.databinding.ActivitySplashBindingImpl;
import dev.env.potriders.databinding.ActivitySplashBindingSw600dpImpl;
import dev.env.potriders.databinding.ActivitySplashBindingSw720dpImpl;
import dev.env.potriders.databinding.ActivityTermsConditionsBindingImpl;
import dev.env.potriders.databinding.AdapterSpinnerDateTimeBindingImpl;
import dev.env.potriders.databinding.AdapterSpinnerWeedMoreoptions1BindingImpl;
import dev.env.potriders.databinding.AdapterSpinnerWeedMoreoptionsBindingImpl;
import dev.env.potriders.databinding.ChooseItemBindingImpl;
import dev.env.potriders.databinding.DialogAddToCartBindingImpl;
import dev.env.potriders.databinding.DialogSplashAlertBindingImpl;
import dev.env.potriders.databinding.DialogThankYouBindingImpl;
import dev.env.potriders.databinding.FragmentAboutusBindingImpl;
import dev.env.potriders.databinding.FragmentAddressBindingImpl;
import dev.env.potriders.databinding.FragmentCartBindingImpl;
import dev.env.potriders.databinding.FragmentCheckoutBindingImpl;
import dev.env.potriders.databinding.FragmentContactUsBindingImpl;
import dev.env.potriders.databinding.FragmentFoodBindingImpl;
import dev.env.potriders.databinding.FragmentFoodItemDetailBindingImpl;
import dev.env.potriders.databinding.FragmentFoodProductBindingImpl;
import dev.env.potriders.databinding.FragmentHomeBindingImpl;
import dev.env.potriders.databinding.FragmentManageAddressBindingImpl;
import dev.env.potriders.databinding.FragmentMyOrdersBindingImpl;
import dev.env.potriders.databinding.FragmentOrderDetailBindingImpl;
import dev.env.potriders.databinding.FragmentPastOrdersBindingImpl;
import dev.env.potriders.databinding.FragmentPayNowBindingImpl;
import dev.env.potriders.databinding.FragmentPrivacyBindingImpl;
import dev.env.potriders.databinding.FragmentProfileBindingImpl;
import dev.env.potriders.databinding.FragmentRatingBindingImpl;
import dev.env.potriders.databinding.FragmentSelectDeliveryTimeBindingImpl;
import dev.env.potriders.databinding.FragmentSettingsBindingImpl;
import dev.env.potriders.databinding.FragmentTermsConditionsBindingImpl;
import dev.env.potriders.databinding.FragmentTrackDeliveryBindingImpl;
import dev.env.potriders.databinding.FragmentWeedBindingImpl;
import dev.env.potriders.databinding.FragmentWeedDescriptionBindingImpl;
import dev.env.potriders.databinding.FragmentWeedEffectBindingImpl;
import dev.env.potriders.databinding.FragmentWeedItemDetailBindingImpl;
import dev.env.potriders.databinding.FragmentWeedItemDetailNoneBindingImpl;
import dev.env.potriders.databinding.FragmentWeedProductBindingImpl;
import dev.env.potriders.databinding.FragmentWeedReviewBindingImpl;
import dev.env.potriders.databinding.LayoutListviewSearchableBindingImpl;
import dev.env.potriders.databinding.ListItemBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYADVANCEDSEARCH = 1;

  private static final int LAYOUT_ACTIVITYCHOOSEOPTION = 2;

  private static final int LAYOUT_ACTIVITYFORGOTPASSWORD = 3;

  private static final int LAYOUT_ACTIVITYLOGIN = 4;

  private static final int LAYOUT_ACTIVITYMAIN = 5;

  private static final int LAYOUT_ACTIVITYREGISTER = 6;

  private static final int LAYOUT_ACTIVITYSEARCH = 7;

  private static final int LAYOUT_ACTIVITYSPLACEALERT = 8;

  private static final int LAYOUT_ACTIVITYSPLASH = 9;

  private static final int LAYOUT_ACTIVITYTERMSCONDITIONS = 10;

  private static final int LAYOUT_ADAPTERSPINNERDATETIME = 11;

  private static final int LAYOUT_ADAPTERSPINNERWEEDMOREOPTIONS = 12;

  private static final int LAYOUT_ADAPTERSPINNERWEEDMOREOPTIONS1 = 13;

  private static final int LAYOUT_CHOOSEITEM = 14;

  private static final int LAYOUT_DIALOGADDTOCART = 15;

  private static final int LAYOUT_DIALOGSPLASHALERT = 16;

  private static final int LAYOUT_DIALOGTHANKYOU = 17;

  private static final int LAYOUT_FRAGMENTABOUTUS = 18;

  private static final int LAYOUT_FRAGMENTADDRESS = 19;

  private static final int LAYOUT_FRAGMENTCART = 20;

  private static final int LAYOUT_FRAGMENTCHECKOUT = 21;

  private static final int LAYOUT_FRAGMENTCONTACTUS = 22;

  private static final int LAYOUT_FRAGMENTFOOD = 23;

  private static final int LAYOUT_FRAGMENTFOODITEMDETAIL = 24;

  private static final int LAYOUT_FRAGMENTFOODPRODUCT = 25;

  private static final int LAYOUT_FRAGMENTHOME = 26;

  private static final int LAYOUT_FRAGMENTMANAGEADDRESS = 27;

  private static final int LAYOUT_FRAGMENTMYORDERS = 28;

  private static final int LAYOUT_FRAGMENTORDERDETAIL = 29;

  private static final int LAYOUT_FRAGMENTPASTORDERS = 30;

  private static final int LAYOUT_FRAGMENTPAYNOW = 31;

  private static final int LAYOUT_FRAGMENTPRIVACY = 32;

  private static final int LAYOUT_FRAGMENTPROFILE = 33;

  private static final int LAYOUT_FRAGMENTRATING = 34;

  private static final int LAYOUT_FRAGMENTSELECTDELIVERYTIME = 35;

  private static final int LAYOUT_FRAGMENTSETTINGS = 36;

  private static final int LAYOUT_FRAGMENTTERMSCONDITIONS = 37;

  private static final int LAYOUT_FRAGMENTTRACKDELIVERY = 38;

  private static final int LAYOUT_FRAGMENTWEED = 39;

  private static final int LAYOUT_FRAGMENTWEEDDESCRIPTION = 40;

  private static final int LAYOUT_FRAGMENTWEEDEFFECT = 41;

  private static final int LAYOUT_FRAGMENTWEEDITEMDETAIL = 42;

  private static final int LAYOUT_FRAGMENTWEEDITEMDETAILNONE = 43;

  private static final int LAYOUT_FRAGMENTWEEDPRODUCT = 44;

  private static final int LAYOUT_FRAGMENTWEEDREVIEW = 45;

  private static final int LAYOUT_LAYOUTLISTVIEWSEARCHABLE = 46;

  private static final int LAYOUT_LISTITEM = 47;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(47);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_advanced_search, LAYOUT_ACTIVITYADVANCEDSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_choose_option, LAYOUT_ACTIVITYCHOOSEOPTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_forgot_password, LAYOUT_ACTIVITYFORGOTPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_login, LAYOUT_ACTIVITYLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_register, LAYOUT_ACTIVITYREGISTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_search, LAYOUT_ACTIVITYSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_splace_alert, LAYOUT_ACTIVITYSPLACEALERT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_splash, LAYOUT_ACTIVITYSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.activity_terms_conditions, LAYOUT_ACTIVITYTERMSCONDITIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.adapter_spinner_date_time, LAYOUT_ADAPTERSPINNERDATETIME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.adapter_spinner_weed_moreoptions, LAYOUT_ADAPTERSPINNERWEEDMOREOPTIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.adapter_spinner_weed_moreoptions1, LAYOUT_ADAPTERSPINNERWEEDMOREOPTIONS1);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.choose_item, LAYOUT_CHOOSEITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.dialog_add_to_cart, LAYOUT_DIALOGADDTOCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.dialog_splash_alert, LAYOUT_DIALOGSPLASHALERT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.dialog_thank_you, LAYOUT_DIALOGTHANKYOU);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_aboutus, LAYOUT_FRAGMENTABOUTUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_address, LAYOUT_FRAGMENTADDRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_cart, LAYOUT_FRAGMENTCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_checkout, LAYOUT_FRAGMENTCHECKOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_contact_us, LAYOUT_FRAGMENTCONTACTUS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_food, LAYOUT_FRAGMENTFOOD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_food_item_detail, LAYOUT_FRAGMENTFOODITEMDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_food_product, LAYOUT_FRAGMENTFOODPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_manage_address, LAYOUT_FRAGMENTMANAGEADDRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_my_orders, LAYOUT_FRAGMENTMYORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_order_detail, LAYOUT_FRAGMENTORDERDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_past_orders, LAYOUT_FRAGMENTPASTORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_pay_now, LAYOUT_FRAGMENTPAYNOW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_privacy, LAYOUT_FRAGMENTPRIVACY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_rating, LAYOUT_FRAGMENTRATING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_select_delivery_time, LAYOUT_FRAGMENTSELECTDELIVERYTIME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_settings, LAYOUT_FRAGMENTSETTINGS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_terms_conditions, LAYOUT_FRAGMENTTERMSCONDITIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_track_delivery, LAYOUT_FRAGMENTTRACKDELIVERY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed, LAYOUT_FRAGMENTWEED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed_description, LAYOUT_FRAGMENTWEEDDESCRIPTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed_effect, LAYOUT_FRAGMENTWEEDEFFECT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed_item_detail, LAYOUT_FRAGMENTWEEDITEMDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed_item_detail_none, LAYOUT_FRAGMENTWEEDITEMDETAILNONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed_product, LAYOUT_FRAGMENTWEEDPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.fragment_weed_review, LAYOUT_FRAGMENTWEEDREVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.layout_listview_searchable, LAYOUT_LAYOUTLISTVIEWSEARCHABLE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(dev.env.potriders.R.layout.list_item, LAYOUT_LISTITEM);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYADVANCEDSEARCH: {
          if ("layout/activity_advanced_search_0".equals(tag)) {
            return new ActivityAdvancedSearchBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_advanced_search is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYCHOOSEOPTION: {
          if ("layout-sw600dp/activity_choose_option_0".equals(tag)) {
            return new ActivityChooseOptionBindingSw600dpImpl(component, view);
          }
          if ("layout-sw720dp/activity_choose_option_0".equals(tag)) {
            return new ActivityChooseOptionBindingSw720dpImpl(component, view);
          }
          if ("layout/activity_choose_option_0".equals(tag)) {
            return new ActivityChooseOptionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_choose_option is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYFORGOTPASSWORD: {
          if ("layout/activity_forgot_password_0".equals(tag)) {
            return new ActivityForgotPasswordBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_forgot_password is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYLOGIN: {
          if ("layout/activity_login_0".equals(tag)) {
            return new ActivityLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYREGISTER: {
          if ("layout/activity_register_0".equals(tag)) {
            return new ActivityRegisterBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_register is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSEARCH: {
          if ("layout/activity_search_0".equals(tag)) {
            return new ActivitySearchBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_search is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSPLACEALERT: {
          if ("layout/activity_splace_alert_0".equals(tag)) {
            return new ActivitySplaceAlertBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_splace_alert is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYSPLASH: {
          if ("layout-sw720dp/activity_splash_0".equals(tag)) {
            return new ActivitySplashBindingSw720dpImpl(component, view);
          }
          if ("layout/activity_splash_0".equals(tag)) {
            return new ActivitySplashBindingImpl(component, view);
          }
          if ("layout-sw600dp/activity_splash_0".equals(tag)) {
            return new ActivitySplashBindingSw600dpImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYTERMSCONDITIONS: {
          if ("layout/activity_terms_conditions_0".equals(tag)) {
            return new ActivityTermsConditionsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_terms_conditions is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSPINNERDATETIME: {
          if ("layout/adapter_spinner_date_time_0".equals(tag)) {
            return new AdapterSpinnerDateTimeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_spinner_date_time is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSPINNERWEEDMOREOPTIONS: {
          if ("layout/adapter_spinner_weed_moreoptions_0".equals(tag)) {
            return new AdapterSpinnerWeedMoreoptionsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_spinner_weed_moreoptions is invalid. Received: " + tag);
        }
        case  LAYOUT_ADAPTERSPINNERWEEDMOREOPTIONS1: {
          if ("layout/adapter_spinner_weed_moreoptions1_0".equals(tag)) {
            return new AdapterSpinnerWeedMoreoptions1BindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for adapter_spinner_weed_moreoptions1 is invalid. Received: " + tag);
        }
        case  LAYOUT_CHOOSEITEM: {
          if ("layout/choose_item_0".equals(tag)) {
            return new ChooseItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for choose_item is invalid. Received: " + tag);
        }
        case  LAYOUT_DIALOGADDTOCART: {
          if ("layout/dialog_add_to_cart_0".equals(tag)) {
            return new DialogAddToCartBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for dialog_add_to_cart is invalid. Received: " + tag);
        }
        case  LAYOUT_DIALOGSPLASHALERT: {
          if ("layout/dialog_splash_alert_0".equals(tag)) {
            return new DialogSplashAlertBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for dialog_splash_alert is invalid. Received: " + tag);
        }
        case  LAYOUT_DIALOGTHANKYOU: {
          if ("layout/dialog_thank_you_0".equals(tag)) {
            return new DialogThankYouBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for dialog_thank_you is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTABOUTUS: {
          if ("layout/fragment_aboutus_0".equals(tag)) {
            return new FragmentAboutusBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_aboutus is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTADDRESS: {
          if ("layout/fragment_address_0".equals(tag)) {
            return new FragmentAddressBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_address is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCART: {
          if ("layout/fragment_cart_0".equals(tag)) {
            return new FragmentCartBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_cart is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCHECKOUT: {
          if ("layout/fragment_checkout_0".equals(tag)) {
            return new FragmentCheckoutBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_checkout is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCONTACTUS: {
          if ("layout/fragment_contact_us_0".equals(tag)) {
            return new FragmentContactUsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_contact_us is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFOOD: {
          if ("layout/fragment_food_0".equals(tag)) {
            return new FragmentFoodBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_food is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFOODITEMDETAIL: {
          if ("layout/fragment_food_item_detail_0".equals(tag)) {
            return new FragmentFoodItemDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_food_item_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFOODPRODUCT: {
          if ("layout/fragment_food_product_0".equals(tag)) {
            return new FragmentFoodProductBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_food_product is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTHOME: {
          if ("layout/fragment_home_0".equals(tag)) {
            return new FragmentHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMANAGEADDRESS: {
          if ("layout/fragment_manage_address_0".equals(tag)) {
            return new FragmentManageAddressBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_manage_address is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMYORDERS: {
          if ("layout/fragment_my_orders_0".equals(tag)) {
            return new FragmentMyOrdersBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_my_orders is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTORDERDETAIL: {
          if ("layout/fragment_order_detail_0".equals(tag)) {
            return new FragmentOrderDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_order_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPASTORDERS: {
          if ("layout/fragment_past_orders_0".equals(tag)) {
            return new FragmentPastOrdersBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_past_orders is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPAYNOW: {
          if ("layout/fragment_pay_now_0".equals(tag)) {
            return new FragmentPayNowBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_pay_now is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPRIVACY: {
          if ("layout/fragment_privacy_0".equals(tag)) {
            return new FragmentPrivacyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_privacy is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPROFILE: {
          if ("layout/fragment_profile_0".equals(tag)) {
            return new FragmentProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTRATING: {
          if ("layout/fragment_rating_0".equals(tag)) {
            return new FragmentRatingBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_rating is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSELECTDELIVERYTIME: {
          if ("layout/fragment_select_delivery_time_0".equals(tag)) {
            return new FragmentSelectDeliveryTimeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_select_delivery_time is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSETTINGS: {
          if ("layout/fragment_settings_0".equals(tag)) {
            return new FragmentSettingsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_settings is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTTERMSCONDITIONS: {
          if ("layout/fragment_terms_conditions_0".equals(tag)) {
            return new FragmentTermsConditionsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_terms_conditions is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTTRACKDELIVERY: {
          if ("layout/fragment_track_delivery_0".equals(tag)) {
            return new FragmentTrackDeliveryBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_track_delivery is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEED: {
          if ("layout/fragment_weed_0".equals(tag)) {
            return new FragmentWeedBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEEDDESCRIPTION: {
          if ("layout/fragment_weed_description_0".equals(tag)) {
            return new FragmentWeedDescriptionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed_description is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEEDEFFECT: {
          if ("layout/fragment_weed_effect_0".equals(tag)) {
            return new FragmentWeedEffectBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed_effect is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEEDITEMDETAIL: {
          if ("layout/fragment_weed_item_detail_0".equals(tag)) {
            return new FragmentWeedItemDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed_item_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEEDITEMDETAILNONE: {
          if ("layout/fragment_weed_item_detail_none_0".equals(tag)) {
            return new FragmentWeedItemDetailNoneBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed_item_detail_none is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEEDPRODUCT: {
          if ("layout/fragment_weed_product_0".equals(tag)) {
            return new FragmentWeedProductBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed_product is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTWEEDREVIEW: {
          if ("layout/fragment_weed_review_0".equals(tag)) {
            return new FragmentWeedReviewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_weed_review is invalid. Received: " + tag);
        }
        case  LAYOUT_LAYOUTLISTVIEWSEARCHABLE: {
          if ("layout/layout_listview_searchable_0".equals(tag)) {
            return new LayoutListviewSearchableBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for layout_listview_searchable is invalid. Received: " + tag);
        }
        case  LAYOUT_LISTITEM: {
          if ("layout/list_item_0".equals(tag)) {
            return new ListItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for list_item is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new com.android.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(2);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(51);

    static {
      sKeys.put("layout/activity_advanced_search_0", dev.env.potriders.R.layout.activity_advanced_search);
      sKeys.put("layout-sw600dp/activity_choose_option_0", dev.env.potriders.R.layout.activity_choose_option);
      sKeys.put("layout-sw720dp/activity_choose_option_0", dev.env.potriders.R.layout.activity_choose_option);
      sKeys.put("layout/activity_choose_option_0", dev.env.potriders.R.layout.activity_choose_option);
      sKeys.put("layout/activity_forgot_password_0", dev.env.potriders.R.layout.activity_forgot_password);
      sKeys.put("layout/activity_login_0", dev.env.potriders.R.layout.activity_login);
      sKeys.put("layout/activity_main_0", dev.env.potriders.R.layout.activity_main);
      sKeys.put("layout/activity_register_0", dev.env.potriders.R.layout.activity_register);
      sKeys.put("layout/activity_search_0", dev.env.potriders.R.layout.activity_search);
      sKeys.put("layout/activity_splace_alert_0", dev.env.potriders.R.layout.activity_splace_alert);
      sKeys.put("layout-sw720dp/activity_splash_0", dev.env.potriders.R.layout.activity_splash);
      sKeys.put("layout/activity_splash_0", dev.env.potriders.R.layout.activity_splash);
      sKeys.put("layout-sw600dp/activity_splash_0", dev.env.potriders.R.layout.activity_splash);
      sKeys.put("layout/activity_terms_conditions_0", dev.env.potriders.R.layout.activity_terms_conditions);
      sKeys.put("layout/adapter_spinner_date_time_0", dev.env.potriders.R.layout.adapter_spinner_date_time);
      sKeys.put("layout/adapter_spinner_weed_moreoptions_0", dev.env.potriders.R.layout.adapter_spinner_weed_moreoptions);
      sKeys.put("layout/adapter_spinner_weed_moreoptions1_0", dev.env.potriders.R.layout.adapter_spinner_weed_moreoptions1);
      sKeys.put("layout/choose_item_0", dev.env.potriders.R.layout.choose_item);
      sKeys.put("layout/dialog_add_to_cart_0", dev.env.potriders.R.layout.dialog_add_to_cart);
      sKeys.put("layout/dialog_splash_alert_0", dev.env.potriders.R.layout.dialog_splash_alert);
      sKeys.put("layout/dialog_thank_you_0", dev.env.potriders.R.layout.dialog_thank_you);
      sKeys.put("layout/fragment_aboutus_0", dev.env.potriders.R.layout.fragment_aboutus);
      sKeys.put("layout/fragment_address_0", dev.env.potriders.R.layout.fragment_address);
      sKeys.put("layout/fragment_cart_0", dev.env.potriders.R.layout.fragment_cart);
      sKeys.put("layout/fragment_checkout_0", dev.env.potriders.R.layout.fragment_checkout);
      sKeys.put("layout/fragment_contact_us_0", dev.env.potriders.R.layout.fragment_contact_us);
      sKeys.put("layout/fragment_food_0", dev.env.potriders.R.layout.fragment_food);
      sKeys.put("layout/fragment_food_item_detail_0", dev.env.potriders.R.layout.fragment_food_item_detail);
      sKeys.put("layout/fragment_food_product_0", dev.env.potriders.R.layout.fragment_food_product);
      sKeys.put("layout/fragment_home_0", dev.env.potriders.R.layout.fragment_home);
      sKeys.put("layout/fragment_manage_address_0", dev.env.potriders.R.layout.fragment_manage_address);
      sKeys.put("layout/fragment_my_orders_0", dev.env.potriders.R.layout.fragment_my_orders);
      sKeys.put("layout/fragment_order_detail_0", dev.env.potriders.R.layout.fragment_order_detail);
      sKeys.put("layout/fragment_past_orders_0", dev.env.potriders.R.layout.fragment_past_orders);
      sKeys.put("layout/fragment_pay_now_0", dev.env.potriders.R.layout.fragment_pay_now);
      sKeys.put("layout/fragment_privacy_0", dev.env.potriders.R.layout.fragment_privacy);
      sKeys.put("layout/fragment_profile_0", dev.env.potriders.R.layout.fragment_profile);
      sKeys.put("layout/fragment_rating_0", dev.env.potriders.R.layout.fragment_rating);
      sKeys.put("layout/fragment_select_delivery_time_0", dev.env.potriders.R.layout.fragment_select_delivery_time);
      sKeys.put("layout/fragment_settings_0", dev.env.potriders.R.layout.fragment_settings);
      sKeys.put("layout/fragment_terms_conditions_0", dev.env.potriders.R.layout.fragment_terms_conditions);
      sKeys.put("layout/fragment_track_delivery_0", dev.env.potriders.R.layout.fragment_track_delivery);
      sKeys.put("layout/fragment_weed_0", dev.env.potriders.R.layout.fragment_weed);
      sKeys.put("layout/fragment_weed_description_0", dev.env.potriders.R.layout.fragment_weed_description);
      sKeys.put("layout/fragment_weed_effect_0", dev.env.potriders.R.layout.fragment_weed_effect);
      sKeys.put("layout/fragment_weed_item_detail_0", dev.env.potriders.R.layout.fragment_weed_item_detail);
      sKeys.put("layout/fragment_weed_item_detail_none_0", dev.env.potriders.R.layout.fragment_weed_item_detail_none);
      sKeys.put("layout/fragment_weed_product_0", dev.env.potriders.R.layout.fragment_weed_product);
      sKeys.put("layout/fragment_weed_review_0", dev.env.potriders.R.layout.fragment_weed_review);
      sKeys.put("layout/layout_listview_searchable_0", dev.env.potriders.R.layout.layout_listview_searchable);
      sKeys.put("layout/list_item_0", dev.env.potriders.R.layout.list_item);
    }
  }
}
