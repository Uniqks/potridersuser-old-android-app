package dev.env.potriders.databinding;
import dev.env.potriders.R;
import dev.env.potriders.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCheckoutBindingImpl extends FragmentCheckoutBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.card_view, 1);
        sViewsWithIds.put(R.id.emailTransferRbtn, 2);
        sViewsWithIds.put(R.id.cashRbtn, 3);
        sViewsWithIds.put(R.id.selectPayRL, 4);
        sViewsWithIds.put(R.id.spnPayment, 5);
        sViewsWithIds.put(R.id.payCardLl, 6);
        sViewsWithIds.put(R.id.cardNoEt, 7);
        sViewsWithIds.put(R.id.expDateEt, 8);
        sViewsWithIds.put(R.id.cvvEt, 9);
        sViewsWithIds.put(R.id.cardHoderNmEt, 10);
        sViewsWithIds.put(R.id.emailCardLl, 11);
        sViewsWithIds.put(R.id.emailEt, 12);
        sViewsWithIds.put(R.id.editIv, 13);
        sViewsWithIds.put(R.id.addressCityTV, 14);
        sViewsWithIds.put(R.id.landmarkTV, 15);
        sViewsWithIds.put(R.id.belowLL, 16);
        sViewsWithIds.put(R.id.subTotalTV, 17);
        sViewsWithIds.put(R.id.taxTV, 18);
        sViewsWithIds.put(R.id.totalPriceTV, 19);
        sViewsWithIds.put(R.id.placeOrderTV, 20);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCheckoutBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentCheckoutBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[14]
            , (android.support.v7.widget.CardView) bindings[16]
            , (android.widget.EditText) bindings[10]
            , (android.widget.EditText) bindings[7]
            , (android.support.v7.widget.CardView) bindings[1]
            , (android.widget.RadioButton) bindings[3]
            , (android.widget.EditText) bindings[9]
            , (android.widget.ImageView) bindings[13]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.EditText) bindings[12]
            , (android.widget.RadioButton) bindings[2]
            , (android.widget.EditText) bindings[8]
            , (android.widget.TextView) bindings[15]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.TextView) bindings[20]
            , (android.widget.RelativeLayout) bindings[4]
            , (android.widget.Spinner) bindings[5]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[19]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}