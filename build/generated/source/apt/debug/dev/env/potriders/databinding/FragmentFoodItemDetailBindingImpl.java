package dev.env.potriders.databinding;
import dev.env.potriders.R;
import dev.env.potriders.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentFoodItemDetailBindingImpl extends FragmentFoodItemDetailBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.llTop, 1);
        sViewsWithIds.put(R.id.layoutTop, 2);
        sViewsWithIds.put(R.id.viewPagerWeed, 3);
        sViewsWithIds.put(R.id.imageViewMain, 4);
        sViewsWithIds.put(R.id.card_view, 5);
        sViewsWithIds.put(R.id.weedNameTV, 6);
        sViewsWithIds.put(R.id.vegNonVegTV, 7);
        sViewsWithIds.put(R.id.ratingTV, 8);
        sViewsWithIds.put(R.id.categoriesTV, 9);
        sViewsWithIds.put(R.id.priceRangeTV, 10);
        sViewsWithIds.put(R.id.moreTV, 11);
        sViewsWithIds.put(R.id.decrementIV, 12);
        sViewsWithIds.put(R.id.quantityTV, 13);
        sViewsWithIds.put(R.id.incrementIV, 14);
        sViewsWithIds.put(R.id.addtocartLL, 15);
        sViewsWithIds.put(R.id.layoutBottom, 16);
        sViewsWithIds.put(R.id.llTabs, 17);
        sViewsWithIds.put(R.id.descriptionTV, 18);
        sViewsWithIds.put(R.id.effectsTV, 19);
        sViewsWithIds.put(R.id.reviewsTV, 20);
        sViewsWithIds.put(R.id.fragment_container, 21);
        sViewsWithIds.put(R.id.cartLL, 22);
        sViewsWithIds.put(R.id.cartIV, 23);
        sViewsWithIds.put(R.id.totalItemsTV, 24);
        sViewsWithIds.put(R.id.priceTV, 25);
        sViewsWithIds.put(R.id.goToCartTV, 26);
        sViewsWithIds.put(R.id.layoutTopa, 27);
        sViewsWithIds.put(R.id.layoutBottoma, 28);
        sViewsWithIds.put(R.id.categoryRL, 29);
        sViewsWithIds.put(R.id.subCategoriesRV, 30);
        sViewsWithIds.put(R.id.cartLLa, 31);
        sViewsWithIds.put(R.id.cartIVa, 32);
        sViewsWithIds.put(R.id.totalItemsTVa, 33);
        sViewsWithIds.put(R.id.priceTVa, 34);
        sViewsWithIds.put(R.id.goToCartTVa, 35);
        sViewsWithIds.put(R.id.overlapImage, 36);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentFoodItemDetailBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 37, sIncludes, sViewsWithIds));
    }
    private FragmentFoodItemDetailBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[15]
            , (android.support.v7.widget.CardView) bindings[5]
            , (android.widget.ImageView) bindings[23]
            , (android.widget.ImageView) bindings[32]
            , (android.widget.LinearLayout) bindings[22]
            , (android.widget.LinearLayout) bindings[31]
            , (android.widget.TextView) bindings[9]
            , (android.support.v7.widget.RecyclerView) bindings[29]
            , (android.widget.ImageView) bindings[12]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[19]
            , (android.widget.FrameLayout) bindings[21]
            , (android.widget.TextView) bindings[26]
            , (android.widget.TextView) bindings[35]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.RelativeLayout) bindings[16]
            , (android.widget.RelativeLayout) bindings[28]
            , (android.widget.RelativeLayout) bindings[2]
            , (android.widget.RelativeLayout) bindings[27]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.LinearLayout) bindings[1]
            , (android.widget.TextView) bindings[11]
            , (android.widget.ImageView) bindings[36]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[25]
            , (android.widget.TextView) bindings[34]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[20]
            , (android.support.v7.widget.RecyclerView) bindings[30]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[33]
            , (android.widget.TextView) bindings[7]
            , (android.support.v4.view.ViewPager) bindings[3]
            , (android.widget.TextView) bindings[6]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}