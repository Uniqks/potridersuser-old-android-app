package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class FragmentWeedReviewBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView rvReviews;

  protected FragmentWeedReviewBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, RecyclerView rvReviews) {
    super(_bindingComponent, _root, _localFieldCount);
    this.rvReviews = rvReviews;
  }

  @NonNull
  public static FragmentWeedReviewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedReviewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedReviewBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_review, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedReviewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedReviewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedReviewBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_review, null, false, component);
  }

  public static FragmentWeedReviewBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedReviewBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedReviewBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed_review);
  }
}
