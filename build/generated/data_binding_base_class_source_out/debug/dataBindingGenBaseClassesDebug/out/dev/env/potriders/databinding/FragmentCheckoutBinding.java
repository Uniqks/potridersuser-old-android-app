package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public abstract class FragmentCheckoutBinding extends ViewDataBinding {
  @NonNull
  public final TextView addressCityTV;

  @NonNull
  public final CardView belowLL;

  @NonNull
  public final EditText cardHoderNmEt;

  @NonNull
  public final EditText cardNoEt;

  @NonNull
  public final CardView cardView;

  @NonNull
  public final RadioButton cashRbtn;

  @NonNull
  public final EditText cvvEt;

  @NonNull
  public final ImageView editIv;

  @NonNull
  public final LinearLayout emailCardLl;

  @NonNull
  public final EditText emailEt;

  @NonNull
  public final RadioButton emailTransferRbtn;

  @NonNull
  public final EditText expDateEt;

  @NonNull
  public final TextView landmarkTV;

  @NonNull
  public final LinearLayout payCardLl;

  @NonNull
  public final TextView placeOrderTV;

  @NonNull
  public final RelativeLayout selectPayRL;

  @NonNull
  public final Spinner spnPayment;

  @NonNull
  public final TextView subTotalTV;

  @NonNull
  public final TextView taxTV;

  @NonNull
  public final TextView totalPriceTV;

  protected FragmentCheckoutBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView addressCityTV, CardView belowLL, EditText cardHoderNmEt,
      EditText cardNoEt, CardView cardView, RadioButton cashRbtn, EditText cvvEt, ImageView editIv,
      LinearLayout emailCardLl, EditText emailEt, RadioButton emailTransferRbtn, EditText expDateEt,
      TextView landmarkTV, LinearLayout payCardLl, TextView placeOrderTV,
      RelativeLayout selectPayRL, Spinner spnPayment, TextView subTotalTV, TextView taxTV,
      TextView totalPriceTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addressCityTV = addressCityTV;
    this.belowLL = belowLL;
    this.cardHoderNmEt = cardHoderNmEt;
    this.cardNoEt = cardNoEt;
    this.cardView = cardView;
    this.cashRbtn = cashRbtn;
    this.cvvEt = cvvEt;
    this.editIv = editIv;
    this.emailCardLl = emailCardLl;
    this.emailEt = emailEt;
    this.emailTransferRbtn = emailTransferRbtn;
    this.expDateEt = expDateEt;
    this.landmarkTV = landmarkTV;
    this.payCardLl = payCardLl;
    this.placeOrderTV = placeOrderTV;
    this.selectPayRL = selectPayRL;
    this.spnPayment = spnPayment;
    this.subTotalTV = subTotalTV;
    this.taxTV = taxTV;
    this.totalPriceTV = totalPriceTV;
  }

  @NonNull
  public static FragmentCheckoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentCheckoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentCheckoutBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_checkout, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentCheckoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentCheckoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentCheckoutBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_checkout, null, false, component);
  }

  public static FragmentCheckoutBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentCheckoutBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentCheckoutBinding)bind(component, view, dev.env.potriders.R.layout.fragment_checkout);
  }
}
