package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class DialogAddToCartBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout addtocartLL;

  @NonNull
  public final TextView categoriesTV;

  @NonNull
  public final ImageView closeIV;

  @NonNull
  public final ImageView decrementIV;

  @NonNull
  public final ImageView imageViewMain;

  @NonNull
  public final ImageView incrementIV;

  @NonNull
  public final RelativeLayout layoutTop;

  @NonNull
  public final TextView priceRangeTV;

  @NonNull
  public final TextView quantityTV;

  @NonNull
  public final RelativeLayout topRl;

  protected DialogAddToCartBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout addtocartLL, TextView categoriesTV, ImageView closeIV,
      ImageView decrementIV, ImageView imageViewMain, ImageView incrementIV,
      RelativeLayout layoutTop, TextView priceRangeTV, TextView quantityTV, RelativeLayout topRl) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addtocartLL = addtocartLL;
    this.categoriesTV = categoriesTV;
    this.closeIV = closeIV;
    this.decrementIV = decrementIV;
    this.imageViewMain = imageViewMain;
    this.incrementIV = incrementIV;
    this.layoutTop = layoutTop;
    this.priceRangeTV = priceRangeTV;
    this.quantityTV = quantityTV;
    this.topRl = topRl;
  }

  @NonNull
  public static DialogAddToCartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static DialogAddToCartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<DialogAddToCartBinding>inflate(inflater, dev.env.potriders.R.layout.dialog_add_to_cart, root, attachToRoot, component);
  }

  @NonNull
  public static DialogAddToCartBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static DialogAddToCartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<DialogAddToCartBinding>inflate(inflater, dev.env.potriders.R.layout.dialog_add_to_cart, null, false, component);
  }

  public static DialogAddToCartBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static DialogAddToCartBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (DialogAddToCartBinding)bind(component, view, dev.env.potriders.R.layout.dialog_add_to_cart);
  }
}
