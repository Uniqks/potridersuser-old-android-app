package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

public abstract class FragmentWeedBinding extends ViewDataBinding {
  @NonNull
  public final ImageView clearImg;

  @NonNull
  public final ImageView imgSearch;

  @NonNull
  public final LinearLayout llNoItem;

  @NonNull
  public final SwipeRefreshLayout refreshSwipe;

  @NonNull
  public final RecyclerView rvWeedList;

  @NonNull
  public final EditText searchEdt;

  protected FragmentWeedBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView clearImg, ImageView imgSearch, LinearLayout llNoItem,
      SwipeRefreshLayout refreshSwipe, RecyclerView rvWeedList, EditText searchEdt) {
    super(_bindingComponent, _root, _localFieldCount);
    this.clearImg = clearImg;
    this.imgSearch = imgSearch;
    this.llNoItem = llNoItem;
    this.refreshSwipe = refreshSwipe;
    this.rvWeedList = rvWeedList;
    this.searchEdt = searchEdt;
  }

  @NonNull
  public static FragmentWeedBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed, null, false, component);
  }

  public static FragmentWeedBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed);
  }
}
