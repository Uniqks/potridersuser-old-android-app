package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public abstract class FragmentManageAddressBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView addressList;

  @NonNull
  public final Button buttonAddNewAddress;

  protected FragmentManageAddressBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, RecyclerView addressList, Button buttonAddNewAddress) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addressList = addressList;
    this.buttonAddNewAddress = buttonAddNewAddress;
  }

  @NonNull
  public static FragmentManageAddressBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentManageAddressBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentManageAddressBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_manage_address, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentManageAddressBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentManageAddressBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentManageAddressBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_manage_address, null, false, component);
  }

  public static FragmentManageAddressBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentManageAddressBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentManageAddressBinding)bind(component, view, dev.env.potriders.R.layout.fragment_manage_address);
  }
}
