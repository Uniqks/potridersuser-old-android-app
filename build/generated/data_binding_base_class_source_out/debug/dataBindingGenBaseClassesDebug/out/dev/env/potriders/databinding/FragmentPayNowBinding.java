package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class FragmentPayNowBinding extends ViewDataBinding {
  @NonNull
  public final EditText edtCardNumber;

  @NonNull
  public final EditText edtName;

  @NonNull
  public final LinearLayout llTop;

  @NonNull
  public final TextView payNowTV;

  @NonNull
  public final TextView tvTotal;

  protected FragmentPayNowBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, EditText edtCardNumber, EditText edtName, LinearLayout llTop,
      TextView payNowTV, TextView tvTotal) {
    super(_bindingComponent, _root, _localFieldCount);
    this.edtCardNumber = edtCardNumber;
    this.edtName = edtName;
    this.llTop = llTop;
    this.payNowTV = payNowTV;
    this.tvTotal = tvTotal;
  }

  @NonNull
  public static FragmentPayNowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPayNowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPayNowBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_pay_now, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentPayNowBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPayNowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPayNowBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_pay_now, null, false, component);
  }

  public static FragmentPayNowBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentPayNowBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentPayNowBinding)bind(component, view, dev.env.potriders.R.layout.fragment_pay_now);
  }
}
