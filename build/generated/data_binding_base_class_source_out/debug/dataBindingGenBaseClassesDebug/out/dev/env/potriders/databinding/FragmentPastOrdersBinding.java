package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class FragmentPastOrdersBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView ordersRV;

  protected FragmentPastOrdersBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, RecyclerView ordersRV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.ordersRV = ordersRV;
  }

  @NonNull
  public static FragmentPastOrdersBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPastOrdersBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPastOrdersBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_past_orders, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentPastOrdersBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPastOrdersBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPastOrdersBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_past_orders, null, false, component);
  }

  public static FragmentPastOrdersBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentPastOrdersBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentPastOrdersBinding)bind(component, view, dev.env.potriders.R.layout.fragment_past_orders);
  }
}
