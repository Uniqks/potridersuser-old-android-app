package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class ActivityChooseOptionBinding extends ViewDataBinding {
  @Nullable
  public final ImageView bannerIV;

  @NonNull
  public final TextView getStartedBT;

  @Nullable
  public final TabLayout indicator;

  @Nullable
  public final TextView loginTV;

  @Nullable
  public final RelativeLayout logo;

  @Nullable
  public final FrameLayout onboardingBackgroundContainer;

  @Nullable
  public final FrameLayout onboardingContentIconContainer;

  @Nullable
  public final FrameLayout onboardingContentTextContainer;

  @Nullable
  public final LinearLayout onboardingPagerIconsContainer;

  @Nullable
  public final RelativeLayout onboardingRootView;

  @Nullable
  public final TextView signUpTV;

  @Nullable
  public final ViewPager viewPager;

  @Nullable
  public final LinearLayout viewPagerCountDots;

  @Nullable
  public final LinearLayout viewPagerIndicator;

  protected ActivityChooseOptionBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView bannerIV, TextView getStartedBT, TabLayout indicator,
      TextView loginTV, RelativeLayout logo, FrameLayout onboardingBackgroundContainer,
      FrameLayout onboardingContentIconContainer, FrameLayout onboardingContentTextContainer,
      LinearLayout onboardingPagerIconsContainer, RelativeLayout onboardingRootView,
      TextView signUpTV, ViewPager viewPager, LinearLayout viewPagerCountDots,
      LinearLayout viewPagerIndicator) {
    super(_bindingComponent, _root, _localFieldCount);
    this.bannerIV = bannerIV;
    this.getStartedBT = getStartedBT;
    this.indicator = indicator;
    this.loginTV = loginTV;
    this.logo = logo;
    this.onboardingBackgroundContainer = onboardingBackgroundContainer;
    this.onboardingContentIconContainer = onboardingContentIconContainer;
    this.onboardingContentTextContainer = onboardingContentTextContainer;
    this.onboardingPagerIconsContainer = onboardingPagerIconsContainer;
    this.onboardingRootView = onboardingRootView;
    this.signUpTV = signUpTV;
    this.viewPager = viewPager;
    this.viewPagerCountDots = viewPagerCountDots;
    this.viewPagerIndicator = viewPagerIndicator;
  }

  @NonNull
  public static ActivityChooseOptionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityChooseOptionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityChooseOptionBinding>inflate(inflater, dev.env.potriders.R.layout.activity_choose_option, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityChooseOptionBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityChooseOptionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityChooseOptionBinding>inflate(inflater, dev.env.potriders.R.layout.activity_choose_option, null, false, component);
  }

  public static ActivityChooseOptionBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityChooseOptionBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityChooseOptionBinding)bind(component, view, dev.env.potriders.R.layout.activity_choose_option);
  }
}
