package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class DialogThankYouBinding extends ViewDataBinding {
  @NonNull
  public final ImageView logoIV;

  @NonNull
  public final RelativeLayout topRl;

  @NonNull
  public final TextView trackOrderTV;

  protected DialogThankYouBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView logoIV, RelativeLayout topRl, TextView trackOrderTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.logoIV = logoIV;
    this.topRl = topRl;
    this.trackOrderTV = trackOrderTV;
  }

  @NonNull
  public static DialogThankYouBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static DialogThankYouBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<DialogThankYouBinding>inflate(inflater, dev.env.potriders.R.layout.dialog_thank_you, root, attachToRoot, component);
  }

  @NonNull
  public static DialogThankYouBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static DialogThankYouBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<DialogThankYouBinding>inflate(inflater, dev.env.potriders.R.layout.dialog_thank_you, null, false, component);
  }

  public static DialogThankYouBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static DialogThankYouBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (DialogThankYouBinding)bind(component, view, dev.env.potriders.R.layout.dialog_thank_you);
  }
}
