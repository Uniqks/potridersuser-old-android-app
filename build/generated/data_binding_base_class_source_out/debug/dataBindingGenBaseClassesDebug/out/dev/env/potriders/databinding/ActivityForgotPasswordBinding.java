package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class ActivityForgotPasswordBinding extends ViewDataBinding {
  @NonNull
  public final TextView btnSubmit;

  @NonNull
  public final EditText edtEmail;

  @NonNull
  public final ImageView imgBack;

  @NonNull
  public final LinearLayout llMain;

  protected ActivityForgotPasswordBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView btnSubmit, EditText edtEmail, ImageView imgBack,
      LinearLayout llMain) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnSubmit = btnSubmit;
    this.edtEmail = edtEmail;
    this.imgBack = imgBack;
    this.llMain = llMain;
  }

  @NonNull
  public static ActivityForgotPasswordBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityForgotPasswordBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityForgotPasswordBinding>inflate(inflater, dev.env.potriders.R.layout.activity_forgot_password, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityForgotPasswordBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityForgotPasswordBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityForgotPasswordBinding>inflate(inflater, dev.env.potriders.R.layout.activity_forgot_password, null, false, component);
  }

  public static ActivityForgotPasswordBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityForgotPasswordBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityForgotPasswordBinding)bind(component, view, dev.env.potriders.R.layout.activity_forgot_password);
  }
}
