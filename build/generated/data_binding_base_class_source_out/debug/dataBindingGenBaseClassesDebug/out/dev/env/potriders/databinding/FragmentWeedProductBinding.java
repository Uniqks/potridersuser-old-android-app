package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

public abstract class FragmentWeedProductBinding extends ViewDataBinding {
  @NonNull
  public final ImageView clearImg;

  @NonNull
  public final RelativeLayout dropdownIV;

  @NonNull
  public final ImageView imgSearch;

  @NonNull
  public final LinearLayout llNoItem;

  @NonNull
  public final SwipeRefreshLayout refreshSwipe;

  @NonNull
  public final RecyclerView rvWeedProList;

  @NonNull
  public final EditText searchEdt;

  @NonNull
  public final LinearLayout searchLL;

  @NonNull
  public final Spinner spnBrand;

  @NonNull
  public final Spinner spnType;

  @NonNull
  public final Spinner spnWeight;

  protected FragmentWeedProductBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView clearImg, RelativeLayout dropdownIV, ImageView imgSearch,
      LinearLayout llNoItem, SwipeRefreshLayout refreshSwipe, RecyclerView rvWeedProList,
      EditText searchEdt, LinearLayout searchLL, Spinner spnBrand, Spinner spnType,
      Spinner spnWeight) {
    super(_bindingComponent, _root, _localFieldCount);
    this.clearImg = clearImg;
    this.dropdownIV = dropdownIV;
    this.imgSearch = imgSearch;
    this.llNoItem = llNoItem;
    this.refreshSwipe = refreshSwipe;
    this.rvWeedProList = rvWeedProList;
    this.searchEdt = searchEdt;
    this.searchLL = searchLL;
    this.spnBrand = spnBrand;
    this.spnType = spnType;
    this.spnWeight = spnWeight;
  }

  @NonNull
  public static FragmentWeedProductBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedProductBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedProductBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_product, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedProductBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedProductBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedProductBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_product, null, false, component);
  }

  public static FragmentWeedProductBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedProductBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedProductBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed_product);
  }
}
