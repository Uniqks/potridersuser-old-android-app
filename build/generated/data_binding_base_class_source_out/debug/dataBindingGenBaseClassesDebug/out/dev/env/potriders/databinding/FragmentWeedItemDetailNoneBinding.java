package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class FragmentWeedItemDetailNoneBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout belowLL;

  @NonNull
  public final CardView cardView;

  @NonNull
  public final ImageView cartIV;

  @NonNull
  public final TextView categoriesTV;

  @NonNull
  public final TextView decreamentIV;

  @NonNull
  public final TextView descriptionTV;

  @NonNull
  public final TextView detailsTV;

  @NonNull
  public final TextView effectsTV;

  @NonNull
  public final FrameLayout fragmentContainer;

  @NonNull
  public final TextView goToCartTV;

  @NonNull
  public final TextView increamentIV;

  @NonNull
  public final RelativeLayout layoutBottom;

  @NonNull
  public final RelativeLayout layoutTop;

  @NonNull
  public final TextView priceRangeTV;

  @NonNull
  public final TextView priceTV;

  @NonNull
  public final TextView quantityTV;

  @NonNull
  public final TextView ratingTV;

  @NonNull
  public final TextView reviewsTV;

  @NonNull
  public final TextView totalItemsTV;

  @NonNull
  public final TextView typeTV;

  @NonNull
  public final TextView vegNonVegTV;

  @NonNull
  public final TextView weedNameTV;

  @NonNull
  public final TextView weightTV;

  protected FragmentWeedItemDetailNoneBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout belowLL, CardView cardView, ImageView cartIV,
      TextView categoriesTV, TextView decreamentIV, TextView descriptionTV, TextView detailsTV,
      TextView effectsTV, FrameLayout fragmentContainer, TextView goToCartTV, TextView increamentIV,
      RelativeLayout layoutBottom, RelativeLayout layoutTop, TextView priceRangeTV,
      TextView priceTV, TextView quantityTV, TextView ratingTV, TextView reviewsTV,
      TextView totalItemsTV, TextView typeTV, TextView vegNonVegTV, TextView weedNameTV,
      TextView weightTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.belowLL = belowLL;
    this.cardView = cardView;
    this.cartIV = cartIV;
    this.categoriesTV = categoriesTV;
    this.decreamentIV = decreamentIV;
    this.descriptionTV = descriptionTV;
    this.detailsTV = detailsTV;
    this.effectsTV = effectsTV;
    this.fragmentContainer = fragmentContainer;
    this.goToCartTV = goToCartTV;
    this.increamentIV = increamentIV;
    this.layoutBottom = layoutBottom;
    this.layoutTop = layoutTop;
    this.priceRangeTV = priceRangeTV;
    this.priceTV = priceTV;
    this.quantityTV = quantityTV;
    this.ratingTV = ratingTV;
    this.reviewsTV = reviewsTV;
    this.totalItemsTV = totalItemsTV;
    this.typeTV = typeTV;
    this.vegNonVegTV = vegNonVegTV;
    this.weedNameTV = weedNameTV;
    this.weightTV = weightTV;
  }

  @NonNull
  public static FragmentWeedItemDetailNoneBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedItemDetailNoneBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedItemDetailNoneBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_item_detail_none, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedItemDetailNoneBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedItemDetailNoneBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedItemDetailNoneBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_item_detail_none, null, false, component);
  }

  public static FragmentWeedItemDetailNoneBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedItemDetailNoneBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedItemDetailNoneBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed_item_detail_none);
  }
}
