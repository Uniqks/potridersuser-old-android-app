package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class ActivityLoginBinding extends ViewDataBinding {
  @NonNull
  public final AppCompatButton btnLogin;

  @NonNull
  public final Button btnRegister;

  @NonNull
  public final Button btnSkip;

  @NonNull
  public final EditText edtEmail;

  @NonNull
  public final EditText edtPwd;

  @NonNull
  public final RelativeLayout layoutRL;

  @NonNull
  public final LinearLayout llMainTop;

  @NonNull
  public final TextView txtFpwd;

  protected ActivityLoginBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, AppCompatButton btnLogin, Button btnRegister, Button btnSkip,
      EditText edtEmail, EditText edtPwd, RelativeLayout layoutRL, LinearLayout llMainTop,
      TextView txtFpwd) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnLogin = btnLogin;
    this.btnRegister = btnRegister;
    this.btnSkip = btnSkip;
    this.edtEmail = edtEmail;
    this.edtPwd = edtPwd;
    this.layoutRL = layoutRL;
    this.llMainTop = llMainTop;
    this.txtFpwd = txtFpwd;
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityLoginBinding>inflate(inflater, dev.env.potriders.R.layout.activity_login, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityLoginBinding>inflate(inflater, dev.env.potriders.R.layout.activity_login, null, false, component);
  }

  public static ActivityLoginBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityLoginBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityLoginBinding)bind(component, view, dev.env.potriders.R.layout.activity_login);
  }
}
