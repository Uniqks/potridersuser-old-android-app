package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class AdapterSpinnerDateTimeBinding extends ViewDataBinding {
  @NonNull
  public final TextView tvContry;

  protected AdapterSpinnerDateTimeBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView tvContry) {
    super(_bindingComponent, _root, _localFieldCount);
    this.tvContry = tvContry;
  }

  @NonNull
  public static AdapterSpinnerDateTimeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static AdapterSpinnerDateTimeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<AdapterSpinnerDateTimeBinding>inflate(inflater, dev.env.potriders.R.layout.adapter_spinner_date_time, root, attachToRoot, component);
  }

  @NonNull
  public static AdapterSpinnerDateTimeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static AdapterSpinnerDateTimeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<AdapterSpinnerDateTimeBinding>inflate(inflater, dev.env.potriders.R.layout.adapter_spinner_date_time, null, false, component);
  }

  public static AdapterSpinnerDateTimeBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static AdapterSpinnerDateTimeBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (AdapterSpinnerDateTimeBinding)bind(component, view, dev.env.potriders.R.layout.adapter_spinner_date_time);
  }
}
