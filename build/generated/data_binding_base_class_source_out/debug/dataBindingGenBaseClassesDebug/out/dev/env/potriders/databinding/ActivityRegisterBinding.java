package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;

public abstract class ActivityRegisterBinding extends ViewDataBinding {
  @NonNull
  public final Button btnSignup;

  @NonNull
  public final CheckBox checkTerms;

  @NonNull
  public final EditText edtEmail;

  @NonNull
  public final EditText edtFname;

  @NonNull
  public final EditText edtLname;

  @NonNull
  public final EditText edtPhone;

  @NonNull
  public final EditText edtPwd;

  @NonNull
  public final ImageView imgCamera;

  @NonNull
  public final CircleImageView imgPro;

  @NonNull
  public final LinearLayout llLogin;

  @NonNull
  public final LinearLayout llMain;

  @NonNull
  public final TextView txtTerms;

  protected ActivityRegisterBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, Button btnSignup, CheckBox checkTerms, EditText edtEmail,
      EditText edtFname, EditText edtLname, EditText edtPhone, EditText edtPwd, ImageView imgCamera,
      CircleImageView imgPro, LinearLayout llLogin, LinearLayout llMain, TextView txtTerms) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnSignup = btnSignup;
    this.checkTerms = checkTerms;
    this.edtEmail = edtEmail;
    this.edtFname = edtFname;
    this.edtLname = edtLname;
    this.edtPhone = edtPhone;
    this.edtPwd = edtPwd;
    this.imgCamera = imgCamera;
    this.imgPro = imgPro;
    this.llLogin = llLogin;
    this.llMain = llMain;
    this.txtTerms = txtTerms;
  }

  @NonNull
  public static ActivityRegisterBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityRegisterBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityRegisterBinding>inflate(inflater, dev.env.potriders.R.layout.activity_register, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityRegisterBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityRegisterBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityRegisterBinding>inflate(inflater, dev.env.potriders.R.layout.activity_register, null, false, component);
  }

  public static ActivityRegisterBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityRegisterBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityRegisterBinding)bind(component, view, dev.env.potriders.R.layout.activity_register);
  }
}
