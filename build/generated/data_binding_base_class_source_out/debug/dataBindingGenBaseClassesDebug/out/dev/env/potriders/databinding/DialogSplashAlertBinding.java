package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public abstract class DialogSplashAlertBinding extends ViewDataBinding {
  @NonNull
  public final RelativeLayout noRl;

  @NonNull
  public final RelativeLayout topRl;

  @NonNull
  public final RelativeLayout yesRl;

  protected DialogSplashAlertBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, RelativeLayout noRl, RelativeLayout topRl, RelativeLayout yesRl) {
    super(_bindingComponent, _root, _localFieldCount);
    this.noRl = noRl;
    this.topRl = topRl;
    this.yesRl = yesRl;
  }

  @NonNull
  public static DialogSplashAlertBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static DialogSplashAlertBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<DialogSplashAlertBinding>inflate(inflater, dev.env.potriders.R.layout.dialog_splash_alert, root, attachToRoot, component);
  }

  @NonNull
  public static DialogSplashAlertBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static DialogSplashAlertBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<DialogSplashAlertBinding>inflate(inflater, dev.env.potriders.R.layout.dialog_splash_alert, null, false, component);
  }

  public static DialogSplashAlertBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static DialogSplashAlertBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (DialogSplashAlertBinding)bind(component, view, dev.env.potriders.R.layout.dialog_splash_alert);
  }
}
