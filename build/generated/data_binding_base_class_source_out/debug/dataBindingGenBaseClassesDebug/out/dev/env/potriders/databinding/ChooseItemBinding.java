package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class ChooseItemBinding extends ViewDataBinding {
  @NonNull
  public final ImageView bannerIV;

  @NonNull
  public final TextView description;

  @NonNull
  public final ImageView logo;

  @NonNull
  public final TextView title;

  protected ChooseItemBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView bannerIV, TextView description, ImageView logo,
      TextView title) {
    super(_bindingComponent, _root, _localFieldCount);
    this.bannerIV = bannerIV;
    this.description = description;
    this.logo = logo;
    this.title = title;
  }

  @NonNull
  public static ChooseItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ChooseItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ChooseItemBinding>inflate(inflater, dev.env.potriders.R.layout.choose_item, root, attachToRoot, component);
  }

  @NonNull
  public static ChooseItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ChooseItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ChooseItemBinding>inflate(inflater, dev.env.potriders.R.layout.choose_item, null, false, component);
  }

  public static ChooseItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ChooseItemBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ChooseItemBinding)bind(component, view, dev.env.potriders.R.layout.choose_item);
  }
}
