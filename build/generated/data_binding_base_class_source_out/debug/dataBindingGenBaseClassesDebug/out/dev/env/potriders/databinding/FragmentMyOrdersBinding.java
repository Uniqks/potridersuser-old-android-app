package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class FragmentMyOrdersBinding extends ViewDataBinding {
  @NonNull
  public final TabLayout bubbleTab;

  @NonNull
  public final ViewPager viewPager;

  protected FragmentMyOrdersBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TabLayout bubbleTab, ViewPager viewPager) {
    super(_bindingComponent, _root, _localFieldCount);
    this.bubbleTab = bubbleTab;
    this.viewPager = viewPager;
  }

  @NonNull
  public static FragmentMyOrdersBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentMyOrdersBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentMyOrdersBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_my_orders, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentMyOrdersBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentMyOrdersBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentMyOrdersBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_my_orders, null, false, component);
  }

  public static FragmentMyOrdersBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentMyOrdersBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentMyOrdersBinding)bind(component, view, dev.env.potriders.R.layout.fragment_my_orders);
  }
}
