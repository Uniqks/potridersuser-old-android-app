package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class FragmentOrderDetailBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout belowLL;

  @NonNull
  public final TextView dateTV;

  @NonNull
  public final TextView orderNoTV;

  @NonNull
  public final TextView priceTV;

  @NonNull
  public final TextView subTotalTV;

  @NonNull
  public final TextView taxTV;

  @NonNull
  public final TextView totalPriceTV;

  @NonNull
  public final TextView tvDeliverOn;

  @NonNull
  public final TextView tvDeliverTo;

  protected FragmentOrderDetailBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout belowLL, TextView dateTV, TextView orderNoTV,
      TextView priceTV, TextView subTotalTV, TextView taxTV, TextView totalPriceTV,
      TextView tvDeliverOn, TextView tvDeliverTo) {
    super(_bindingComponent, _root, _localFieldCount);
    this.belowLL = belowLL;
    this.dateTV = dateTV;
    this.orderNoTV = orderNoTV;
    this.priceTV = priceTV;
    this.subTotalTV = subTotalTV;
    this.taxTV = taxTV;
    this.totalPriceTV = totalPriceTV;
    this.tvDeliverOn = tvDeliverOn;
    this.tvDeliverTo = tvDeliverTo;
  }

  @NonNull
  public static FragmentOrderDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentOrderDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentOrderDetailBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_order_detail, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentOrderDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentOrderDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentOrderDetailBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_order_detail, null, false, component);
  }

  public static FragmentOrderDetailBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentOrderDetailBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentOrderDetailBinding)bind(component, view, dev.env.potriders.R.layout.fragment_order_detail);
  }
}
