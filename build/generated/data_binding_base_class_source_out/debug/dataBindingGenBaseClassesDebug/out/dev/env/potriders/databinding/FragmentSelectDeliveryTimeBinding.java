package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

public abstract class FragmentSelectDeliveryTimeBinding extends ViewDataBinding {
  @NonNull
  public final TextView doneTV;

  @NonNull
  public final Spinner spinnerSelectDate;

  @NonNull
  public final Spinner spinnerSelectTime;

  @NonNull
  public final CheckBox termsConditionsCB;

  protected FragmentSelectDeliveryTimeBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView doneTV, Spinner spinnerSelectDate, Spinner spinnerSelectTime,
      CheckBox termsConditionsCB) {
    super(_bindingComponent, _root, _localFieldCount);
    this.doneTV = doneTV;
    this.spinnerSelectDate = spinnerSelectDate;
    this.spinnerSelectTime = spinnerSelectTime;
    this.termsConditionsCB = termsConditionsCB;
  }

  @NonNull
  public static FragmentSelectDeliveryTimeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentSelectDeliveryTimeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentSelectDeliveryTimeBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_select_delivery_time, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentSelectDeliveryTimeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentSelectDeliveryTimeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentSelectDeliveryTimeBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_select_delivery_time, null, false, component);
  }

  public static FragmentSelectDeliveryTimeBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentSelectDeliveryTimeBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentSelectDeliveryTimeBinding)bind(component, view, dev.env.potriders.R.layout.fragment_select_delivery_time);
  }
}
