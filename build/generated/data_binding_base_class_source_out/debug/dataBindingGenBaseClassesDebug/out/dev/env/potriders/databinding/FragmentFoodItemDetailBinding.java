package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class FragmentFoodItemDetailBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout addtocartLL;

  @NonNull
  public final CardView cardView;

  @NonNull
  public final ImageView cartIV;

  @NonNull
  public final ImageView cartIVa;

  @NonNull
  public final LinearLayout cartLL;

  @NonNull
  public final LinearLayout cartLLa;

  @NonNull
  public final TextView categoriesTV;

  @NonNull
  public final RecyclerView categoryRL;

  @NonNull
  public final ImageView decrementIV;

  @NonNull
  public final TextView descriptionTV;

  @NonNull
  public final TextView effectsTV;

  @NonNull
  public final FrameLayout fragmentContainer;

  @NonNull
  public final TextView goToCartTV;

  @NonNull
  public final TextView goToCartTVa;

  @NonNull
  public final ImageView imageViewMain;

  @NonNull
  public final ImageView incrementIV;

  @NonNull
  public final RelativeLayout layoutBottom;

  @NonNull
  public final RelativeLayout layoutBottoma;

  @NonNull
  public final RelativeLayout layoutTop;

  @NonNull
  public final RelativeLayout layoutTopa;

  @NonNull
  public final LinearLayout llTabs;

  @NonNull
  public final LinearLayout llTop;

  @NonNull
  public final TextView moreTV;

  @NonNull
  public final ImageView overlapImage;

  @NonNull
  public final TextView priceRangeTV;

  @NonNull
  public final TextView priceTV;

  @NonNull
  public final TextView priceTVa;

  @NonNull
  public final TextView quantityTV;

  @NonNull
  public final TextView ratingTV;

  @NonNull
  public final TextView reviewsTV;

  @NonNull
  public final RecyclerView subCategoriesRV;

  @NonNull
  public final TextView totalItemsTV;

  @NonNull
  public final TextView totalItemsTVa;

  @NonNull
  public final TextView vegNonVegTV;

  @NonNull
  public final ViewPager viewPagerWeed;

  @NonNull
  public final TextView weedNameTV;

  protected FragmentFoodItemDetailBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout addtocartLL, CardView cardView, ImageView cartIV,
      ImageView cartIVa, LinearLayout cartLL, LinearLayout cartLLa, TextView categoriesTV,
      RecyclerView categoryRL, ImageView decrementIV, TextView descriptionTV, TextView effectsTV,
      FrameLayout fragmentContainer, TextView goToCartTV, TextView goToCartTVa,
      ImageView imageViewMain, ImageView incrementIV, RelativeLayout layoutBottom,
      RelativeLayout layoutBottoma, RelativeLayout layoutTop, RelativeLayout layoutTopa,
      LinearLayout llTabs, LinearLayout llTop, TextView moreTV, ImageView overlapImage,
      TextView priceRangeTV, TextView priceTV, TextView priceTVa, TextView quantityTV,
      TextView ratingTV, TextView reviewsTV, RecyclerView subCategoriesRV, TextView totalItemsTV,
      TextView totalItemsTVa, TextView vegNonVegTV, ViewPager viewPagerWeed, TextView weedNameTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addtocartLL = addtocartLL;
    this.cardView = cardView;
    this.cartIV = cartIV;
    this.cartIVa = cartIVa;
    this.cartLL = cartLL;
    this.cartLLa = cartLLa;
    this.categoriesTV = categoriesTV;
    this.categoryRL = categoryRL;
    this.decrementIV = decrementIV;
    this.descriptionTV = descriptionTV;
    this.effectsTV = effectsTV;
    this.fragmentContainer = fragmentContainer;
    this.goToCartTV = goToCartTV;
    this.goToCartTVa = goToCartTVa;
    this.imageViewMain = imageViewMain;
    this.incrementIV = incrementIV;
    this.layoutBottom = layoutBottom;
    this.layoutBottoma = layoutBottoma;
    this.layoutTop = layoutTop;
    this.layoutTopa = layoutTopa;
    this.llTabs = llTabs;
    this.llTop = llTop;
    this.moreTV = moreTV;
    this.overlapImage = overlapImage;
    this.priceRangeTV = priceRangeTV;
    this.priceTV = priceTV;
    this.priceTVa = priceTVa;
    this.quantityTV = quantityTV;
    this.ratingTV = ratingTV;
    this.reviewsTV = reviewsTV;
    this.subCategoriesRV = subCategoriesRV;
    this.totalItemsTV = totalItemsTV;
    this.totalItemsTVa = totalItemsTVa;
    this.vegNonVegTV = vegNonVegTV;
    this.viewPagerWeed = viewPagerWeed;
    this.weedNameTV = weedNameTV;
  }

  @NonNull
  public static FragmentFoodItemDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentFoodItemDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentFoodItemDetailBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_food_item_detail, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentFoodItemDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentFoodItemDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentFoodItemDetailBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_food_item_detail, null, false, component);
  }

  public static FragmentFoodItemDetailBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentFoodItemDetailBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentFoodItemDetailBinding)bind(component, view, dev.env.potriders.R.layout.fragment_food_item_detail);
  }
}
