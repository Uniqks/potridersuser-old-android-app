package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class ActivityTermsConditionsBinding extends ViewDataBinding {
  @NonNull
  public final TextView aboutUsTV;

  @NonNull
  public final LinearLayout lyToolbar;

  @NonNull
  public final LinearLayout topLL;

  protected ActivityTermsConditionsBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView aboutUsTV, LinearLayout lyToolbar, LinearLayout topLL) {
    super(_bindingComponent, _root, _localFieldCount);
    this.aboutUsTV = aboutUsTV;
    this.lyToolbar = lyToolbar;
    this.topLL = topLL;
  }

  @NonNull
  public static ActivityTermsConditionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityTermsConditionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityTermsConditionsBinding>inflate(inflater, dev.env.potriders.R.layout.activity_terms_conditions, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityTermsConditionsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityTermsConditionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityTermsConditionsBinding>inflate(inflater, dev.env.potriders.R.layout.activity_terms_conditions, null, false, component);
  }

  public static ActivityTermsConditionsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityTermsConditionsBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityTermsConditionsBinding)bind(component, view, dev.env.potriders.R.layout.activity_terms_conditions);
  }
}
