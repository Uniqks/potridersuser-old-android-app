package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class FragmentPrivacyBinding extends ViewDataBinding {
  @NonNull
  public final TextView privacyTV;

  @NonNull
  public final LinearLayout topLL;

  protected FragmentPrivacyBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView privacyTV, LinearLayout topLL) {
    super(_bindingComponent, _root, _localFieldCount);
    this.privacyTV = privacyTV;
    this.topLL = topLL;
  }

  @NonNull
  public static FragmentPrivacyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPrivacyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPrivacyBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_privacy, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentPrivacyBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentPrivacyBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentPrivacyBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_privacy, null, false, component);
  }

  public static FragmentPrivacyBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentPrivacyBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentPrivacyBinding)bind(component, view, dev.env.potriders.R.layout.fragment_privacy);
  }
}
