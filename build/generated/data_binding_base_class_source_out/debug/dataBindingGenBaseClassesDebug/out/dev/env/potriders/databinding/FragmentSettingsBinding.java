package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class FragmentSettingsBinding extends ViewDataBinding {
  @NonNull
  public final ImageView aboutUsIV;

  @NonNull
  public final RelativeLayout aboutUsRL;

  @NonNull
  public final TextView aboutUsTV;

  @NonNull
  public final TextView changePasswordTV;

  @NonNull
  public final ImageView contactUsIV;

  @NonNull
  public final RelativeLayout contactUsRL;

  @NonNull
  public final TextView contactUsTV;

  @NonNull
  public final LinearLayout llMyAddress;

  @NonNull
  public final LinearLayout llTop;

  @NonNull
  public final RelativeLayout privacyPolicyRL;

  @NonNull
  public final TextView termsConditionsTV;

  @NonNull
  public final View view;

  protected FragmentSettingsBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView aboutUsIV, RelativeLayout aboutUsRL, TextView aboutUsTV,
      TextView changePasswordTV, ImageView contactUsIV, RelativeLayout contactUsRL,
      TextView contactUsTV, LinearLayout llMyAddress, LinearLayout llTop,
      RelativeLayout privacyPolicyRL, TextView termsConditionsTV, View view) {
    super(_bindingComponent, _root, _localFieldCount);
    this.aboutUsIV = aboutUsIV;
    this.aboutUsRL = aboutUsRL;
    this.aboutUsTV = aboutUsTV;
    this.changePasswordTV = changePasswordTV;
    this.contactUsIV = contactUsIV;
    this.contactUsRL = contactUsRL;
    this.contactUsTV = contactUsTV;
    this.llMyAddress = llMyAddress;
    this.llTop = llTop;
    this.privacyPolicyRL = privacyPolicyRL;
    this.termsConditionsTV = termsConditionsTV;
    this.view = view;
  }

  @NonNull
  public static FragmentSettingsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentSettingsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentSettingsBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_settings, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentSettingsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentSettingsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentSettingsBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_settings, null, false, component);
  }

  public static FragmentSettingsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentSettingsBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentSettingsBinding)bind(component, view, dev.env.potriders.R.layout.fragment_settings);
  }
}
