package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public abstract class ActivitySplaceAlertBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout alertLL;

  @NonNull
  public final LinearLayout centerLL;

  @NonNull
  public final ImageView logoIV;

  @NonNull
  public final AppCompatButton noBTN;

  @NonNull
  public final AppCompatButton yesBTN;

  protected ActivitySplaceAlertBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout alertLL, LinearLayout centerLL, ImageView logoIV,
      AppCompatButton noBTN, AppCompatButton yesBTN) {
    super(_bindingComponent, _root, _localFieldCount);
    this.alertLL = alertLL;
    this.centerLL = centerLL;
    this.logoIV = logoIV;
    this.noBTN = noBTN;
    this.yesBTN = yesBTN;
  }

  @NonNull
  public static ActivitySplaceAlertBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivitySplaceAlertBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivitySplaceAlertBinding>inflate(inflater, dev.env.potriders.R.layout.activity_splace_alert, root, attachToRoot, component);
  }

  @NonNull
  public static ActivitySplaceAlertBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivitySplaceAlertBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivitySplaceAlertBinding>inflate(inflater, dev.env.potriders.R.layout.activity_splace_alert, null, false, component);
  }

  public static ActivitySplaceAlertBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivitySplaceAlertBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivitySplaceAlertBinding)bind(component, view, dev.env.potriders.R.layout.activity_splace_alert);
  }
}
