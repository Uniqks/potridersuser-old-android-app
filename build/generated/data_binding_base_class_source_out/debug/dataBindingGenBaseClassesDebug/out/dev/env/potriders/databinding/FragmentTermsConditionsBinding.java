package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class FragmentTermsConditionsBinding extends ViewDataBinding {
  @NonNull
  public final TextView aboutUsTV;

  @NonNull
  public final LinearLayout topLL;

  protected FragmentTermsConditionsBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView aboutUsTV, LinearLayout topLL) {
    super(_bindingComponent, _root, _localFieldCount);
    this.aboutUsTV = aboutUsTV;
    this.topLL = topLL;
  }

  @NonNull
  public static FragmentTermsConditionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentTermsConditionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentTermsConditionsBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_terms_conditions, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentTermsConditionsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentTermsConditionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentTermsConditionsBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_terms_conditions, null, false, component);
  }

  public static FragmentTermsConditionsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentTermsConditionsBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentTermsConditionsBinding)bind(component, view, dev.env.potriders.R.layout.fragment_terms_conditions);
  }
}
