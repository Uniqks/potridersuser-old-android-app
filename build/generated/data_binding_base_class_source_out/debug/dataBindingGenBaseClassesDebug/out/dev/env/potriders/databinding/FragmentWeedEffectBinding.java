package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public abstract class FragmentWeedEffectBinding extends ViewDataBinding {
  @NonNull
  public final TextView myTextEuphoric;

  @NonNull
  public final TextView myTextHappy;

  @NonNull
  public final TextView myTextRelaxed;

  @NonNull
  public final TextView myTextUplifted;

  @NonNull
  public final ProgressBar pbEuphoric;

  @NonNull
  public final ProgressBar pbHappy;

  @NonNull
  public final ProgressBar pbRelaxed;

  @NonNull
  public final ProgressBar pbUplifted;

  protected FragmentWeedEffectBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView myTextEuphoric, TextView myTextHappy, TextView myTextRelaxed,
      TextView myTextUplifted, ProgressBar pbEuphoric, ProgressBar pbHappy, ProgressBar pbRelaxed,
      ProgressBar pbUplifted) {
    super(_bindingComponent, _root, _localFieldCount);
    this.myTextEuphoric = myTextEuphoric;
    this.myTextHappy = myTextHappy;
    this.myTextRelaxed = myTextRelaxed;
    this.myTextUplifted = myTextUplifted;
    this.pbEuphoric = pbEuphoric;
    this.pbHappy = pbHappy;
    this.pbRelaxed = pbRelaxed;
    this.pbUplifted = pbUplifted;
  }

  @NonNull
  public static FragmentWeedEffectBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedEffectBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedEffectBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_effect, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedEffectBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedEffectBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedEffectBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_effect, null, false, component);
  }

  public static FragmentWeedEffectBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedEffectBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedEffectBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed_effect);
  }
}
