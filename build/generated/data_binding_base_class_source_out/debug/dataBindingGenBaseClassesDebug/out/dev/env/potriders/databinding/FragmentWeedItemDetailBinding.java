package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class FragmentWeedItemDetailBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout addtocartLL;

  @NonNull
  public final CardView cardView;

  @NonNull
  public final ImageView cartIV;

  @NonNull
  public final LinearLayout cartLL;

  @NonNull
  public final TextView categoriesTV;

  @NonNull
  public final ImageView decrementIV;

  @NonNull
  public final TextView descriptionTV;

  @NonNull
  public final TextView effectsTV;

  @NonNull
  public final FrameLayout fragmentContainer;

  @NonNull
  public final TextView goToCartTV;

  @NonNull
  public final ImageView imageViewMain;

  @NonNull
  public final ImageView incrementIV;

  @NonNull
  public final RelativeLayout layoutBottom;

  @NonNull
  public final RelativeLayout layoutTop;

  @NonNull
  public final LinearLayout llTabs;

  @NonNull
  public final LinearLayout llTop;

  @NonNull
  public final TextView moreTV;

  @NonNull
  public final TextView priceRangeTV;

  @NonNull
  public final TextView priceTV;

  @NonNull
  public final TextView quantityTV;

  @NonNull
  public final TextView ratingTV;

  @NonNull
  public final TextView reviewsTV;

  @NonNull
  public final TextView totalItemsTV;

  @NonNull
  public final TextView vegNonVegTV;

  @NonNull
  public final ViewPager viewPagerWeed;

  @NonNull
  public final TextView weedNameTV;

  protected FragmentWeedItemDetailBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout addtocartLL, CardView cardView, ImageView cartIV,
      LinearLayout cartLL, TextView categoriesTV, ImageView decrementIV, TextView descriptionTV,
      TextView effectsTV, FrameLayout fragmentContainer, TextView goToCartTV,
      ImageView imageViewMain, ImageView incrementIV, RelativeLayout layoutBottom,
      RelativeLayout layoutTop, LinearLayout llTabs, LinearLayout llTop, TextView moreTV,
      TextView priceRangeTV, TextView priceTV, TextView quantityTV, TextView ratingTV,
      TextView reviewsTV, TextView totalItemsTV, TextView vegNonVegTV, ViewPager viewPagerWeed,
      TextView weedNameTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addtocartLL = addtocartLL;
    this.cardView = cardView;
    this.cartIV = cartIV;
    this.cartLL = cartLL;
    this.categoriesTV = categoriesTV;
    this.decrementIV = decrementIV;
    this.descriptionTV = descriptionTV;
    this.effectsTV = effectsTV;
    this.fragmentContainer = fragmentContainer;
    this.goToCartTV = goToCartTV;
    this.imageViewMain = imageViewMain;
    this.incrementIV = incrementIV;
    this.layoutBottom = layoutBottom;
    this.layoutTop = layoutTop;
    this.llTabs = llTabs;
    this.llTop = llTop;
    this.moreTV = moreTV;
    this.priceRangeTV = priceRangeTV;
    this.priceTV = priceTV;
    this.quantityTV = quantityTV;
    this.ratingTV = ratingTV;
    this.reviewsTV = reviewsTV;
    this.totalItemsTV = totalItemsTV;
    this.vegNonVegTV = vegNonVegTV;
    this.viewPagerWeed = viewPagerWeed;
    this.weedNameTV = weedNameTV;
  }

  @NonNull
  public static FragmentWeedItemDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedItemDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedItemDetailBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_item_detail, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedItemDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedItemDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedItemDetailBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_item_detail, null, false, component);
  }

  public static FragmentWeedItemDetailBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedItemDetailBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedItemDetailBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed_item_detail);
  }
}
