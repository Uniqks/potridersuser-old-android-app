package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;

public abstract class FragmentProfileBinding extends ViewDataBinding {
  @NonNull
  public final TextView buttonChangePassword;

  @NonNull
  public final ImageView cameraIV;

  @NonNull
  public final EditText emailET;

  @NonNull
  public final EditText firstNameET;

  @NonNull
  public final EditText lastNameET;

  @NonNull
  public final EditText phoneNoET;

  @NonNull
  public final LinearLayout topLL;

  @NonNull
  public final CircleImageView userIV;

  protected FragmentProfileBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView buttonChangePassword, ImageView cameraIV, EditText emailET,
      EditText firstNameET, EditText lastNameET, EditText phoneNoET, LinearLayout topLL,
      CircleImageView userIV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.buttonChangePassword = buttonChangePassword;
    this.cameraIV = cameraIV;
    this.emailET = emailET;
    this.firstNameET = firstNameET;
    this.lastNameET = lastNameET;
    this.phoneNoET = phoneNoET;
    this.topLL = topLL;
    this.userIV = userIV;
  }

  @NonNull
  public static FragmentProfileBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentProfileBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentProfileBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_profile, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentProfileBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentProfileBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentProfileBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_profile, null, false, component);
  }

  public static FragmentProfileBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentProfileBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentProfileBinding)bind(component, view, dev.env.potriders.R.layout.fragment_profile);
  }
}
