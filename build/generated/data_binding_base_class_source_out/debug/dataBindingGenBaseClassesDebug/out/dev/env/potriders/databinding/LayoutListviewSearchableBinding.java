package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

public abstract class LayoutListviewSearchableBinding extends ViewDataBinding {
  @NonNull
  public final EditText etSearch;

  @NonNull
  public final ListView listView;

  protected LayoutListviewSearchableBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, EditText etSearch, ListView listView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.etSearch = etSearch;
    this.listView = listView;
  }

  @NonNull
  public static LayoutListviewSearchableBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static LayoutListviewSearchableBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<LayoutListviewSearchableBinding>inflate(inflater, dev.env.potriders.R.layout.layout_listview_searchable, root, attachToRoot, component);
  }

  @NonNull
  public static LayoutListviewSearchableBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static LayoutListviewSearchableBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<LayoutListviewSearchableBinding>inflate(inflater, dev.env.potriders.R.layout.layout_listview_searchable, null, false, component);
  }

  public static LayoutListviewSearchableBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static LayoutListviewSearchableBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (LayoutListviewSearchableBinding)bind(component, view, dev.env.potriders.R.layout.layout_listview_searchable);
  }
}
