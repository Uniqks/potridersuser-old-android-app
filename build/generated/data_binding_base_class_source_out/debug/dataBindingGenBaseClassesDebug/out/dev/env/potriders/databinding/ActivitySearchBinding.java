package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class ActivitySearchBinding extends ViewDataBinding {
  @NonNull
  public final CardView CVSearch;

  @NonNull
  public final CardView CVSearch1;

  @NonNull
  public final ImageView backImg;

  @NonNull
  public final EditText etSearch;

  @NonNull
  public final ImageView ivSearch;

  @NonNull
  public final RecyclerView lvResults;

  @NonNull
  public final TextView title;

  @NonNull
  public final Toolbar toolbar;

  protected ActivitySearchBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, CardView CVSearch, CardView CVSearch1, ImageView backImg,
      EditText etSearch, ImageView ivSearch, RecyclerView lvResults, TextView title,
      Toolbar toolbar) {
    super(_bindingComponent, _root, _localFieldCount);
    this.CVSearch = CVSearch;
    this.CVSearch1 = CVSearch1;
    this.backImg = backImg;
    this.etSearch = etSearch;
    this.ivSearch = ivSearch;
    this.lvResults = lvResults;
    this.title = title;
    this.toolbar = toolbar;
  }

  @NonNull
  public static ActivitySearchBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivitySearchBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivitySearchBinding>inflate(inflater, dev.env.potriders.R.layout.activity_search, root, attachToRoot, component);
  }

  @NonNull
  public static ActivitySearchBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivitySearchBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivitySearchBinding>inflate(inflater, dev.env.potriders.R.layout.activity_search, null, false, component);
  }

  public static ActivitySearchBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivitySearchBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivitySearchBinding)bind(component, view, dev.env.potriders.R.layout.activity_search);
  }
}
