package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class FragmentWeedDescriptionBinding extends ViewDataBinding {
  @NonNull
  public final TextView descriptionTV;

  protected FragmentWeedDescriptionBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView descriptionTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.descriptionTV = descriptionTV;
  }

  @NonNull
  public static FragmentWeedDescriptionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedDescriptionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedDescriptionBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_description, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentWeedDescriptionBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentWeedDescriptionBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentWeedDescriptionBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_weed_description, null, false, component);
  }

  public static FragmentWeedDescriptionBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentWeedDescriptionBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentWeedDescriptionBinding)bind(component, view, dev.env.potriders.R.layout.fragment_weed_description);
  }
}
