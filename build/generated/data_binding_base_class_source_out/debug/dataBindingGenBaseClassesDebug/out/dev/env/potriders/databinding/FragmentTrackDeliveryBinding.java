package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;

public abstract class FragmentTrackDeliveryBinding extends ViewDataBinding {
  @NonNull
  public final ImageView callIv;

  @NonNull
  public final CardView cardView;

  @NonNull
  public final LinearLayout lyRatingBar;

  @NonNull
  public final RatingBar ratingBar;

  @NonNull
  public final TextView tvCarId;

  @NonNull
  public final TextView tvCarName;

  @NonNull
  public final TextView tvDriverName;

  @NonNull
  public final TextView tvRatingValue;

  @NonNull
  public final CircleImageView userIV;

  protected FragmentTrackDeliveryBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView callIv, CardView cardView, LinearLayout lyRatingBar,
      RatingBar ratingBar, TextView tvCarId, TextView tvCarName, TextView tvDriverName,
      TextView tvRatingValue, CircleImageView userIV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.callIv = callIv;
    this.cardView = cardView;
    this.lyRatingBar = lyRatingBar;
    this.ratingBar = ratingBar;
    this.tvCarId = tvCarId;
    this.tvCarName = tvCarName;
    this.tvDriverName = tvDriverName;
    this.tvRatingValue = tvRatingValue;
    this.userIV = userIV;
  }

  @NonNull
  public static FragmentTrackDeliveryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentTrackDeliveryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentTrackDeliveryBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_track_delivery, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentTrackDeliveryBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentTrackDeliveryBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentTrackDeliveryBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_track_delivery, null, false, component);
  }

  public static FragmentTrackDeliveryBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentTrackDeliveryBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentTrackDeliveryBinding)bind(component, view, dev.env.potriders.R.layout.fragment_track_delivery);
  }
}
