package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class ListItemBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout lySearchItem;

  @NonNull
  public final TextView productName;

  protected ListItemBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout lySearchItem, TextView productName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.lySearchItem = lySearchItem;
    this.productName = productName;
  }

  @NonNull
  public static ListItemBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ListItemBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ListItemBinding>inflate(inflater, dev.env.potriders.R.layout.list_item, root, attachToRoot, component);
  }

  @NonNull
  public static ListItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ListItemBinding>inflate(inflater, dev.env.potriders.R.layout.list_item, null, false, component);
  }

  public static ListItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ListItemBinding bind(@NonNull View view, @Nullable DataBindingComponent component) {
    return (ListItemBinding)bind(component, view, dev.env.potriders.R.layout.list_item);
  }
}
