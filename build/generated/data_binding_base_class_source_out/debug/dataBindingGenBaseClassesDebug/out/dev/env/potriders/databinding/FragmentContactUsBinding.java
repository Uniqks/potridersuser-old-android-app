package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public abstract class FragmentContactUsBinding extends ViewDataBinding {
  @NonNull
  public final EditText emailET;

  @NonNull
  public final EditText messageET;

  @NonNull
  public final EditText nameET;

  @NonNull
  public final Button sendBT;

  @NonNull
  public final LinearLayout topLL;

  protected FragmentContactUsBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, EditText emailET, EditText messageET, EditText nameET, Button sendBT,
      LinearLayout topLL) {
    super(_bindingComponent, _root, _localFieldCount);
    this.emailET = emailET;
    this.messageET = messageET;
    this.nameET = nameET;
    this.sendBT = sendBT;
    this.topLL = topLL;
  }

  @NonNull
  public static FragmentContactUsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentContactUsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentContactUsBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_contact_us, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentContactUsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentContactUsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentContactUsBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_contact_us, null, false, component);
  }

  public static FragmentContactUsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentContactUsBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentContactUsBinding)bind(component, view, dev.env.potriders.R.layout.fragment_contact_us);
  }
}
