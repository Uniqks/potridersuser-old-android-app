package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

public abstract class FragmentRatingBinding extends ViewDataBinding {
  @NonNull
  public final ImageView galleryIV;

  @NonNull
  public final RecyclerView imagesList;

  @NonNull
  public final EditText messageET;

  @NonNull
  public final RatingBar ratingBar;

  @NonNull
  public final TextView submitTV;

  @NonNull
  public final LinearLayout uploadImagesLL;

  protected FragmentRatingBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageView galleryIV, RecyclerView imagesList, EditText messageET,
      RatingBar ratingBar, TextView submitTV, LinearLayout uploadImagesLL) {
    super(_bindingComponent, _root, _localFieldCount);
    this.galleryIV = galleryIV;
    this.imagesList = imagesList;
    this.messageET = messageET;
    this.ratingBar = ratingBar;
    this.submitTV = submitTV;
    this.uploadImagesLL = uploadImagesLL;
  }

  @NonNull
  public static FragmentRatingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentRatingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentRatingBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_rating, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentRatingBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentRatingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentRatingBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_rating, null, false, component);
  }

  public static FragmentRatingBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentRatingBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentRatingBinding)bind(component, view, dev.env.potriders.R.layout.fragment_rating);
  }
}
