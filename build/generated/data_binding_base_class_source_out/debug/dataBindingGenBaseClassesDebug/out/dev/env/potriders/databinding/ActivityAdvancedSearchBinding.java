package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public abstract class ActivityAdvancedSearchBinding extends ViewDataBinding {
  @NonNull
  public final TextView detailsTVBrand;

  @NonNull
  public final TextView detailsTVType;

  @NonNull
  public final TextView detailsTVWeight;

  @NonNull
  public final TextView doneTV;

  @NonNull
  public final LinearLayout lyToolbar;

  @NonNull
  public final Spinner spnBrand;

  @NonNull
  public final Spinner spnType;

  @NonNull
  public final Spinner spnWeight;

  protected ActivityAdvancedSearchBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView detailsTVBrand, TextView detailsTVType,
      TextView detailsTVWeight, TextView doneTV, LinearLayout lyToolbar, Spinner spnBrand,
      Spinner spnType, Spinner spnWeight) {
    super(_bindingComponent, _root, _localFieldCount);
    this.detailsTVBrand = detailsTVBrand;
    this.detailsTVType = detailsTVType;
    this.detailsTVWeight = detailsTVWeight;
    this.doneTV = doneTV;
    this.lyToolbar = lyToolbar;
    this.spnBrand = spnBrand;
    this.spnType = spnType;
    this.spnWeight = spnWeight;
  }

  @NonNull
  public static ActivityAdvancedSearchBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityAdvancedSearchBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityAdvancedSearchBinding>inflate(inflater, dev.env.potriders.R.layout.activity_advanced_search, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityAdvancedSearchBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityAdvancedSearchBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityAdvancedSearchBinding>inflate(inflater, dev.env.potriders.R.layout.activity_advanced_search, null, false, component);
  }

  public static ActivityAdvancedSearchBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityAdvancedSearchBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityAdvancedSearchBinding)bind(component, view, dev.env.potriders.R.layout.activity_advanced_search);
  }
}
