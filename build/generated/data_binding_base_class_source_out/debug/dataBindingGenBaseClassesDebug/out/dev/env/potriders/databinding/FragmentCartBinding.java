package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class FragmentCartBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout belowLL;

  @NonNull
  public final LinearLayout preOrderLL;

  @NonNull
  public final TextView preOrderNoDataFoundTV;

  @NonNull
  public final TextView proceedTV;

  @NonNull
  public final LinearLayout readyLL;

  @NonNull
  public final TextView readyNoDataFoundTV;

  @NonNull
  public final RecyclerView rvCartFood;

  @NonNull
  public final RecyclerView rvCartWeed;

  @NonNull
  public final TextView subTotalTV;

  @NonNull
  public final TextView taxTV;

  @NonNull
  public final TextView totalPriceTV;

  protected FragmentCartBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LinearLayout belowLL, LinearLayout preOrderLL,
      TextView preOrderNoDataFoundTV, TextView proceedTV, LinearLayout readyLL,
      TextView readyNoDataFoundTV, RecyclerView rvCartFood, RecyclerView rvCartWeed,
      TextView subTotalTV, TextView taxTV, TextView totalPriceTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.belowLL = belowLL;
    this.preOrderLL = preOrderLL;
    this.preOrderNoDataFoundTV = preOrderNoDataFoundTV;
    this.proceedTV = proceedTV;
    this.readyLL = readyLL;
    this.readyNoDataFoundTV = readyNoDataFoundTV;
    this.rvCartFood = rvCartFood;
    this.rvCartWeed = rvCartWeed;
    this.subTotalTV = subTotalTV;
    this.taxTV = taxTV;
    this.totalPriceTV = totalPriceTV;
  }

  @NonNull
  public static FragmentCartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentCartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentCartBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_cart, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentCartBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentCartBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentCartBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_cart, null, false, component);
  }

  public static FragmentCartBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentCartBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentCartBinding)bind(component, view, dev.env.potriders.R.layout.fragment_cart);
  }
}
