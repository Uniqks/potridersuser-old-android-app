package dev.env.potriders.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public abstract class FragmentAddressBinding extends ViewDataBinding {
  @NonNull
  public final EditText cityET;

  @NonNull
  public final TextView cityTV;

  @NonNull
  public final EditText houseDetailET;

  @NonNull
  public final TextView houseDetailTV;

  @NonNull
  public final EditText landmarkET;

  @NonNull
  public final TextView landmarkTV;

  @NonNull
  public final EditText pincodeET;

  @NonNull
  public final TextView pincodeTV;

  @NonNull
  public final TextView saveTV;

  @NonNull
  public final EditText stateET;

  @NonNull
  public final TextView stateTV;

  protected FragmentAddressBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, EditText cityET, TextView cityTV, EditText houseDetailET,
      TextView houseDetailTV, EditText landmarkET, TextView landmarkTV, EditText pincodeET,
      TextView pincodeTV, TextView saveTV, EditText stateET, TextView stateTV) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cityET = cityET;
    this.cityTV = cityTV;
    this.houseDetailET = houseDetailET;
    this.houseDetailTV = houseDetailTV;
    this.landmarkET = landmarkET;
    this.landmarkTV = landmarkTV;
    this.pincodeET = pincodeET;
    this.pincodeTV = pincodeTV;
    this.saveTV = saveTV;
    this.stateET = stateET;
    this.stateTV = stateTV;
  }

  @NonNull
  public static FragmentAddressBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentAddressBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentAddressBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_address, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentAddressBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentAddressBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentAddressBinding>inflate(inflater, dev.env.potriders.R.layout.fragment_address, null, false, component);
  }

  public static FragmentAddressBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentAddressBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentAddressBinding)bind(component, view, dev.env.potriders.R.layout.fragment_address);
  }
}
