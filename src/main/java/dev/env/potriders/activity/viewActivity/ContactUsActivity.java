package dev.env.potriders.activity.viewActivity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentContactUsBinding;


public class ContactUsActivity extends AppCompatActivity implements OnClickListener {
    FragmentContactUsBinding binding;

    String strName, strEmail, strMessage;
    ImageView imageViewBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_contact_us);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.textViewTitle);
        imageViewBack = toolbar.findViewById(R.id.imageViewBack);
        imageViewBack.setOnClickListener(this);
        textViewTitle.setText(getString(R.string.str_contact_us));


//        binding.aboutUsTV.setText(getString(R.string.str_lorem_ipsum));

        init();
    }

    private void init() {
        binding.sendBT.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setanimationView(binding.topLL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                onBackPressed();
                break;
            case R.id.sendBT:
                strName = binding.nameET.getText().toString().trim();
                strEmail = binding.emailET.getText().toString().trim();
                strMessage = binding.messageET.getText().toString().trim();
                if (strName != null && !strName.equals("")) {
                    if (strEmail != null && !strEmail.equals("")) {
                        if (strMessage != null && !strMessage.equals("")) {

                        } else {
                            Toast.makeText(this, getString(R.string.err_empty_message), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.err_empty_email), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.err_empty_name), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    public void setTitle(String title) {
        //binding.lyToolbar.findViewById(R.id.title).setVisibility(View.VISIBLE);
//        binding.lyToolbar.findViewById(R.id.titleIV).setVisibility(View.GONE);
        //TextView tvTitle = binding.lyToolbar.findViewById(R.id.title);
       // tvTitle.setText(title.toUpperCase());
    }

}
