package dev.env.potriders.activity.viewActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import dev.env.potriders.R;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.LogUtils;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TermsConditionsActivity extends BaseActivity implements OnClickListener {


    ImageView imageViewBack;
    TextView aboutUsTV;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);


        initUI();
    }

    private void initUI() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.textViewTitle);
        imageViewBack = toolbar.findViewById(R.id.imageViewBack);
        aboutUsTV = findViewById(R.id.aboutUsTV);
        imageViewBack.setOnClickListener(this);
        textViewTitle.setText(getString(R.string.terms));
        if (Utils.isNetworkAvailable(TermsConditionsActivity.this)) {
            getTermsContentService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), TermsConditionsActivity.this);
        }

    }

    private void getTermsContentService() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);
        Call<String> call = apiInterface.getPages(2);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();
                LogUtils.d("=response==>>" + response.body());
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String content = jsonObject1.getString("content");
                        aboutUsTV.setText(Html.fromHtml(content));
                    } else {
                        CommonUtils.makeToast(msg, TermsConditionsActivity.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), TermsConditionsActivity.this);
                hideProgressDialog();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                onBackPressed();
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(TermsConditionsActivity.this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


}
