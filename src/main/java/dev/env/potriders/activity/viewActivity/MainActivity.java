package dev.env.potriders.activity.viewActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;

import dev.env.potriders.R;
import dev.env.potriders.activity.Login;
import dev.env.potriders.activity.adapter.DrawerAdapter;
import dev.env.potriders.activity.adapter.DrawerItem;
import dev.env.potriders.activity.adapter.SimpleItem;
import dev.env.potriders.activity.fragment.CartFragment;
import dev.env.potriders.activity.fragment.HomeFragment;
import dev.env.potriders.activity.fragment.MyOrdersFragment;
import dev.env.potriders.activity.fragment.ProfileFragment;
import dev.env.potriders.activity.fragment.SettingsFragment;
import dev.env.potriders.activity.fragment.TrackDeliveryFragment;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;
import spencerstudios.com.bungeelib.Bungee;


public class MainActivity extends BaseActivity implements OnClickListener, DrawerAdapter.OnItemSelectedListener {

    private static final int POS_HOME = 0;
    private static final int POS_ORDERS = 1;
    private static final int POS_SETTINGS = 2;
    private static final int POS_LOGOUT = 3;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    public SlidingRootNav slidingRootNav;
    private RecyclerView mMenuList;
    CartEditProfileClick cartEditProfileClick;

    boolean isEditClicked = false, isProfileClicked = false;
    boolean doubleBackToExitPressedOnce = false;


    public Toolbar mToolbar;
    public TextView mTitleTxt, mEmailTxt, mUnameTxt;
    public ImageView mProImg, mMenuImg, mFilterImg, mBackImg, mCartImg, mEditImg, mSaveImg, mLogoImg;

    public interface CartEditProfileClick {
        void onEditIconClick(boolean isEdit);
    }

    public void setCartEditProfileClick(CartEditProfileClick cartEditProfileClick) {
        this.cartEditProfileClick = cartEditProfileClick;
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = findViewById(R.id.toolbar);


        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(mToolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();





        initControls();
        setDrawerData();
    }


    /**
     *  Method is used to initialized drawer data....
     */
    public void setDrawerData() {

        String url = "", name = "", propic = "", email = "";
        url = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceKeys.IMAGE_URL, "");

        name = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceKeys.FIRST_NAME, "")
                + " " + PreferenceUtil.getPref(MainActivity.this).getString(PreferenceKeys.LAST_NAME, "");

        email = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceKeys.EMAIL, "");

        propic = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceKeys.PROPIC, "");

        if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(propic)) {
            Glide.with(MainActivity.this)
                    .load(url + "" + propic)
                    .asBitmap()
                    .into(mProImg);
        }
        if (!TextUtils.isEmpty(name)) {
            mUnameTxt.setText(name);
        }
        if (!TextUtils.isEmpty(email)) {
            mEmailTxt.setText(email);
        }
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mToolbar.setNavigationIcon(null);
        mTitleTxt = findViewById(R.id.txt_title);

        mBackImg = findViewById(R.id.imgBack);
        mBackImg.setOnClickListener(this);

        mLogoImg = findViewById(R.id.img_logo);

        mMenuImg = findViewById(R.id.img_menu);
        mMenuImg.setOnClickListener(this);

        mFilterImg = findViewById(R.id.img_filter);
        mFilterImg.setOnClickListener(this);

        mCartImg = findViewById(R.id.img_cart);
        mCartImg.setOnClickListener(this);

        mEditImg = findViewById(R.id.img_edit);
        mEditImg.setOnClickListener(this);

        mSaveImg = findViewById(R.id.img_save);
        mSaveImg.setOnClickListener(this);

        pushFragment(new HomeFragment(), false, true, false,
                HomeFragment.class.getSimpleName(), null, false);

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_HOME).setChecked(true),
                createItemFor(POS_ORDERS),
                createItemFor(POS_SETTINGS),
                createItemFor(POS_LOGOUT)));
        adapter.setListener(this);

        mMenuList = findViewById(R.id.list_menu);
        mMenuList.setNestedScrollingEnabled(false);
        mMenuList.setLayoutManager(new LinearLayoutManager(this));
        mMenuList.setAdapter(adapter);


        findViewById(R.id.rl_view_pro).setOnClickListener(this);
        mUnameTxt = findViewById(R.id.txt_uname);
        mEmailTxt = findViewById(R.id.txt_email);
        mProImg = findViewById(R.id.img_pro);
    }


    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.colorPrimary))
                .withTextTint(color(R.color.white))
                .withSelectedIconTint(color(R.color.colorPrimary))
                .withSelectedTextTint(color(R.color.white));
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    private String[] loadScreenTitles() {
        if (PreferenceUtil.getPref(MainActivity.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {
            return getResources().getStringArray(R.array.ScreenLogout);
        } else {
            return getResources().getStringArray(R.array.ScreenLogin);
        }
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgBack:
                hideKeyboard();
                onBackPressed();
                slidingRootNav.setMenuLocked(false);
                break;

            case R.id.img_menu:
                hideKeyboard();
                slidingRootNav.openMenu();
                break;

            case R.id.rl_view_pro:
                slidingRootNav.closeMenu();
                isProfileClicked = true;

                if (PreferenceUtil.getPref(MainActivity.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {
                    PreferenceUtil.getPref(MainActivity.this).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                    performProfileClick();
                } else {
                    PreferenceUtil.getPref(MainActivity.this).edit()
                            .putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, true).apply();
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    startActivity(intent);
                }
                break;

            case R.id.img_cart:
                //slidingRootNav.setMenuLocked(true);
                if (PreferenceUtil.getPref(MainActivity.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {
                    PreferenceUtil.getPref(MainActivity.this).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                    performCartClick();
                } else {
                    PreferenceUtil.getPref(MainActivity.this).edit()
                            .putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, true).apply();
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    startActivity(intent);
                }
                break;

            case R.id.img_filter:
                startActivity(new Intent(this, AdvancedSearchActivity.class));
                break;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public void onItemSelected(int position) {
        if (position == 2) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();
            performSettingClick();

        } else if (position == 0) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();
            performHomeClick();

        } else if (position == 1) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();

            if (PreferenceUtil.getPref(MainActivity.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {
                PreferenceUtil.getPref(MainActivity.this).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                performMyOrderClick();
            } else {
                PreferenceUtil.getPref(MainActivity.this).edit()
                        .putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, true).apply();
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }

        } else if (position == 3) {

            slidingRootNav.closeMenu();
            if (PreferenceUtil.getPref(MainActivity.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {

                final View dialogView = View.inflate(MainActivity.this, R.layout.dialog_signout,null);

                final Dialog dialog = new Dialog(MainActivity.this, R.style.MyAlertDialogStyle);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);

                TextView mNoTxt, mYesTxt, mMessageTxt;

                mMessageTxt = dialog.findViewById(R.id.txt_msg);
                mMessageTxt.setText("Are you sure you want to Logout?");

                mNoTxt = dialog.findViewById(R.id.txt_no);
                mYesTxt = dialog.findViewById(R.id.txt_yes);

                mNoTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                mYesTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        PreferenceUtil.getPref(MainActivity.this).edit().putBoolean(PreferenceKeys.IS_LOGIN, false)
                                .putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false)
                                .putInt(PreferenceKeys.USER_ID, 0)
                                .putString(PreferenceKeys.FIRST_NAME, "")
                                .putString(PreferenceKeys.EMAIL, "")
                                .putString(PreferenceKeys.LAST_NAME, "")
                                .putString(PreferenceKeys.PROPIC, "")
                                .putString(PreferenceKeys.PHONE, "")
                                .putBoolean(PreferenceKeys.IS_STARTED, false).apply();

                        Intent intent = new Intent(MainActivity.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Bungee.slideRight(MainActivity.this);
                    }
                });
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();

            } else {
                PreferenceUtil.getPref(MainActivity.this).edit()
                        .putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, true).apply();

                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        }
    }


    /**
     *  Method is used to initialized Order click event
     */
    private void performMyOrderClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(MyOrdersFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new MyOrdersFragment(), true, false, true,
                MyOrdersFragment.class.getSimpleName(), null, false);

    }

    /**
     *  Method is used to initialized Home click event
     */
    private void performHomeClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(HomeFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();

            System.out.println("data home -->" + getSupportFragmentManager().getBackStackEntryCount());
        }
        pushFragment(new HomeFragment(), false, true, false,
                HomeFragment.class.getSimpleName(), null, false);
    }

    /**
     *  Method is used to initialized Settings click event
     */
    private void performSettingClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(SettingsFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new SettingsFragment(), true, false, true,
                SettingsFragment.class.getSimpleName(), null, false);
    }

    /**
     *  Method is used to initialized Profile click event
     */
    private void performProfileClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(ProfileFragment.class.getSimpleName())) {
                return;
            }
        }


        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }

        pushFragment(new ProfileFragment(), true, false, true,
                ProfileFragment.class.getSimpleName(), null, false);
    }

    /**
     * Method is used to initialized Cart click event...
     */
    private void performCartClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(ProfileFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new CartFragment(), true, false, true,
                CartFragment.class.getSimpleName(), null, false);
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            if (TrackDeliveryFragment.mGetClass != null && !TrackDeliveryFragment.mGetClass.equals("")) {
                if (TrackDeliveryFragment.mGetClass.equals("TrackOrder")) {

                    //System.out.println("on  back  track--->" + getSupportFragmentManager().getBackStackEntryCount());
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                } else {
                    TrackDeliveryFragment.mGetClass = "";
                    isEditClicked = false;
                    slidingRootNav.closeMenu();
                    getSupportFragmentManager().popBackStack();

                   // System.out.println("on  back --->" + getSupportFragmentManager().getBackStackEntryCount());
                }
            } else {

                TrackDeliveryFragment.mGetClass = "";
                isEditClicked = false;
                slidingRootNav.closeMenu();
                getSupportFragmentManager().popBackStack();

                System.out.println("on  back --->" + getSupportFragmentManager().getBackStackEntryCount());
            }


        } else {

            System.out.println("on  back1 --->" + getSupportFragmentManager().getBackStackEntryCount());
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;

            slidingRootNav.closeMenu();
            final View dialogView = View.inflate(MainActivity.this, R.layout.dialog_signout,null);

            final Dialog dialog = new Dialog(MainActivity.this, R.style.MyAlertDialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);

            TextView mNoTxt, mYesTxt, mMessageTxt;

            mMessageTxt = dialog.findViewById(R.id.txt_msg);
            mMessageTxt.setText("Are you sure you want to exit?");

            mNoTxt = dialog.findViewById(R.id.txt_no);
            mYesTxt = dialog.findViewById(R.id.txt_yes);

            mNoTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            mYesTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();


            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);

        }
    }




    @Override
    protected void onResume() {
        super.onResume();
    }

}
