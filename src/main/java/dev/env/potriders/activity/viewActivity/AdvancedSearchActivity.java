package dev.env.potriders.activity.viewActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.databinding.ActivityAdvancedSearchBinding;
import dev.env.potriders.model.WeightModel;
import dev.env.potriders.utils.Utils;

public class AdvancedSearchActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    ActivityAdvancedSearchBinding binding;
    ImageView imageViewBack;
    private List<WeightModel> brandModels = new ArrayList<>();
    private List<WeightModel> typeModels = new ArrayList<>();
    private List<WeightModel> weightModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_advanced_search);





        initControls();
        setSpinnerBrandList();
        setSpinnerTypeList();
        setSpinnerWeightList();
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.detailsTVBrand.setOnClickListener(this);
        binding.detailsTVType.setOnClickListener(this);
        binding.detailsTVWeight.setOnClickListener(this);
        binding.doneTV.setOnClickListener(this);

        binding.spnBrand.setOnItemSelectedListener(this);
        binding.spnBrand.setEnabled(true);

        binding.spnType.setOnItemSelectedListener(this);
        binding.spnType.setEnabled(true);

        binding.spnWeight.setOnItemSelectedListener(this);
        binding.spnWeight.setEnabled(true);



        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.textViewTitle);
        imageViewBack = toolbar.findViewById(R.id.imageViewBack);
        textViewTitle.setText(getString(R.string.str_advanced_search));
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.doneTV:
                onBackPressed();
                break;

            case R.id.detailsTVBrand:
                Intent intent = new Intent(this, SearchActivity.class);
                intent.putExtra(Utils.EXTRA_SEARCH_TITLE, Utils.TITLE_BRAND);
                startActivityForResult(intent, Utils.REQUEST_CODE_SEARCH_BRAND_WEED_ADVANCED);
                break;

            case R.id.detailsTVType:
                Intent intent1 = new Intent(this, SearchActivity.class);
                intent1.putExtra(Utils.EXTRA_SEARCH_TITLE, Utils.TITLE_TYPE);
                startActivityForResult(intent1, Utils.REQUEST_CODE_SEARCH_TYPE_WEED_ADVANCED);
                break;

            case R.id.detailsTVWeight:
                Intent intent2 = new Intent(this, SearchActivity.class);
                intent2.putExtra(Utils.EXTRA_SEARCH_TITLE, Utils.TITLE_WEIGHT);
                startActivityForResult(intent2, Utils.REQUEST_CODE_SEARCH_WEIGHT_WEED_ADVANCED);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == Utils.REQUEST_CODE_SEARCH_BRAND_WEED_ADVANCED && resultCode == Activity.RESULT_OK) {


        } else if (requestCode == Utils.REQUEST_CODE_SEARCH_TYPE_WEED_ADVANCED && resultCode == Activity.RESULT_OK) {


        } else if (requestCode == Utils.REQUEST_CODE_SEARCH_WEIGHT_WEED_ADVANCED && resultCode == Activity.RESULT_OK) {


        }
    }


    /**
     *  Method is used to set the brand list...
     */
    private void setSpinnerBrandList() {

        brandModels.add(new WeightModel("Select Brand"));
        brandModels.add(new WeightModel("Brand 1"));
        brandModels.add(new WeightModel("Brand 2"));
        brandModels.add(new WeightModel("Brand 3"));
        brandModels.add(new WeightModel("Brand 4"));
        brandModels.add(new WeightModel("Brand 5"));
        brandModels.add(new WeightModel("Brand 6"));
        brandModels.add(new WeightModel("Brand 7"));
        brandModels.add(new WeightModel("Brand 8"));
        brandModels.add(new WeightModel("Brand 9"));

        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(AdvancedSearchActivity.this, R.id.txt_title, brandModels, getResources());
        binding.spnBrand.setAdapter(categoryListAdapter);
    }


    /**
     *  Method is used to set the type list...
     */
    private void setSpinnerTypeList() {

        typeModels.add(new WeightModel("Select Type"));
        typeModels.add(new WeightModel("Savita"));
        typeModels.add(new WeightModel("Indica"));
        typeModels.add(new WeightModel("Edibles"));
        typeModels.add(new WeightModel("Hybrid"));
        typeModels.add(new WeightModel("Concentrate"));
        typeModels.add(new WeightModel("Tincture"));


        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(AdvancedSearchActivity.this, R.id.txt_title, typeModels, getResources());
        binding.spnType.setAdapter(categoryListAdapter);
    }


    /**
     *  Method is used to set the weight list...
     */
    private void setSpinnerWeightList() {

        weightModels.add(new WeightModel("Select Weight"));
        weightModels.add(new WeightModel("50 grams"));
        weightModels.add(new WeightModel("100 grams"));
        weightModels.add(new WeightModel("1 kg"));
        weightModels.add(new WeightModel("2 kg"));
        weightModels.add(new WeightModel("5 kg"));


        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(AdvancedSearchActivity.this, R.id.txt_title, weightModels, getResources());
        binding.spnWeight.setAdapter(categoryListAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.spnBrand:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /**
     *  category list adapter...
     */
    public class CategoryListAdapter extends ArrayAdapter<WeightModel> {
        private Context context;
        private List<WeightModel> data;
        public Resources res;
        WeightModel userData = null;
        LayoutInflater inflater;

        public CategoryListAdapter(Context context, int textViewResourceId, List<WeightModel> data, Resources res) {
            super(context, textViewResourceId, data);

            this.context = context;
            this.data = data;
            this.res = res;
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            View view = inflater.inflate(R.layout.row_spinner_list, parent, false);

            userData = null;
            userData = (WeightModel) data.get(position);
            TextView mCatNameTxt;

            ((TextView) view.findViewById(R.id.txt_cat)).setText(userData.getName());

            return view;
        }
    }
}
