package dev.env.potriders.activity.viewActivity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.SearchableAdapter;
import dev.env.potriders.databinding.ActivitySearchBinding;
import dev.env.potriders.activity.interfaces.ClickListener;
import dev.env.potriders.model.CommonModel;
import dev.env.potriders.model.WeightModel;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.RecyclerTouchListener;
import dev.env.potriders.utils.SimpleDividerItemDecoration;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity implements SearchableAdapter.SearchItemClick {


    ActivitySearchBinding binding;
    SearchableAdapter searchableAdapter;
    ArrayList<WeightModel> searchFromList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);



        //searchableAdapter = new SearchableAdapter(this, searchFromList);
        //searchableAdapter.setSearchItemClick(this);
        binding.lvResults.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.lvResults.setAdapter(searchableAdapter);
        binding.lvResults.addItemDecoration(new SimpleDividerItemDecoration(this));

        binding.lvResults.addOnItemTouchListener(new RecyclerTouchListener(this,
                binding.lvResults, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                Intent intent = getIntent();
                intent.putExtra(Utils.EXTRA_SELECTED_SEARCH_POS, String.valueOf(position));
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                        searchableAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().getString(Utils.EXTRA_SEARCH_TITLE) != null && !getIntent().getExtras().getString(Utils.EXTRA_SEARCH_TITLE).equals(""))
                binding.title.setText(getString(R.string.str_search) + " " + getIntent().getExtras().getString(Utils.EXTRA_SEARCH_TITLE));
            String title = getIntent().getExtras().getString(Utils.EXTRA_SEARCH_TITLE);
            if (Utils.isNetworkAvailable(SearchActivity.this)) {
                if (title.equalsIgnoreCase(Utils.TITLE_WEIGHT)) {
//                    getWeightList();
                } else if (title.equalsIgnoreCase(Utils.TITLE_TYPE)) {

                } else if (title.equalsIgnoreCase(Utils.TITLE_BRAND)) {

                }
            } else {
                CommonUtils.makeToast(getString(R.string.no_internet_connection), SearchActivity.this);
            }
        }

        binding.backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    private void getWeightList() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonModel<List<WeightModel>>> call = apiInterface.getWeightList();
        showProgressDialog();
        call.enqueue(new Callback<CommonModel<List<WeightModel>>>() {
            @Override
            public void onResponse(Call<CommonModel<List<WeightModel>>> call, Response<CommonModel<List<WeightModel>>> response) {
                hideProgressDialog();

                if (response.body().getStatus() == Constants.SUCCESS) {
                    searchFromList.addAll(response.body().getData());
//                    searchableAdapter.notifyDataSetChanged();
                } else {
                    CommonUtils.makeToast(response.body().getMsg(), SearchActivity.this);
                }
            }

            @Override
            public void onFailure(Call<CommonModel<List<WeightModel>>> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), SearchActivity.this);
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onSearchItemClick(int position) {
        Intent intent = getIntent();
        intent.putExtra(Utils.EXTRA_SELECTED_SEARCH_POS, String.valueOf(position));
        setResult(RESULT_OK, intent);
        finish();
    }
}
