package dev.env.potriders.activity.viewActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import dev.env.potriders.R;

public class FullDescriptionActivity extends AppCompatActivity implements View.OnClickListener {


	Toolbar toolbar;
	ImageView ivBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_description);
		toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar();
		getSupportActionBar().setTitle(R.string.str_full_description);








		initControls();
	}


    /**
     *   Method is used to initialized all the controls...
     */
    private void initControls() {

        findViewById(R.id.ivBack).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivBack:
                onBackPressed();
                break;

        }
    }
}
