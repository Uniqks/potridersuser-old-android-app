package dev.env.potriders.activity.viewActivity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import com.ramotion.paperonboarding.PaperOnboardingEngine;
import com.ramotion.paperonboarding.PaperOnboardingPage;

import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.activity.Login;
import dev.env.potriders.databinding.ActivityChooseOptionBinding;
import spencerstudios.com.bungeelib.Bungee;


public class ChooseOptionActivity extends AppCompatActivity implements OnClickListener {

    ActivityChooseOptionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_choose_option);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.signup));
        }

        PaperOnboardingEngine engine = new PaperOnboardingEngine(findViewById(R.id.onboardingRootView), getDataForOnboarding(), getApplicationContext());
        binding.getStartedBT.setOnClickListener(this);
    }


    /**
     * Just example data for Onboarding
     */
    private ArrayList<PaperOnboardingPage> getDataForOnboarding() {
        PaperOnboardingPage scr1 = new PaperOnboardingPage(getString(R.string.slider_title1), getString(R.string.slider_description1),
                getResources().getColor(R.color.signup), R.drawable.bnr_weed, R.drawable.ic_d2);

        PaperOnboardingPage scr3 = new PaperOnboardingPage(getString(R.string.slider_title2), getString(R.string.slider_description2),
                getResources().getColor(R.color.signup), R.drawable.bnr_plate, R.drawable.ic_d1);

        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(scr1);
        elements.add(scr3);
        return elements;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.getStartedBT:
                startActivity(new Intent(ChooseOptionActivity.this, Login.class));
                finish();
                Bungee.slideLeft(ChooseOptionActivity.this);
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

}
