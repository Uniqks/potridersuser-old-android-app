package dev.env.potriders.activity.viewActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;


import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.FullScreenImageAdapter;
import dev.env.potriders.utils.Utils;

public class FullScreenViewActivity extends AppCompatActivity {

	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	ArrayList<String> arr_img_list = new ArrayList<>();
	Toolbar toolbar;
	ImageView ivBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);

		toolbar = findViewById(R.id.toolbar);
		ivBack = findViewById(R.id.ivBack);
		setSupportActionBar(toolbar);
		getSupportActionBar();

		ivBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});


		viewPager = (ViewPager) findViewById(R.id.pager);

		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);
		arr_img_list = i.getStringArrayListExtra(Utils.EXTRA_IMG_LIST);


		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
				arr_img_list);

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(position);
	}
}
