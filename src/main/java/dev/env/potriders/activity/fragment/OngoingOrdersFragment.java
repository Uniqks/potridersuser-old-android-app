package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.OngoingOrdersAdapter;
import dev.env.potriders.databinding.FragmentPastOrdersBinding;
import dev.env.potriders.model.CategoryDetails;
import dev.env.potriders.utils.Constants;

public class OngoingOrdersFragment extends BaseFragment implements OngoingOrdersAdapter.WeedItemCick {

    FragmentPastOrdersBinding binding;
    OngoingOrdersAdapter ongoingOrdersAdapter;
    ArrayList<CategoryDetails> arrWeedList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_past_orders, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        for (int i = 0; i < 3; i++) {
            arrWeedList.add(new CategoryDetails("1", "TPDE AFGHAN HAWAIIAN INDICA", "Vegetarian", "Indica", "100 grams", "$10-$150", "", "3.0"));
        }

        binding.ordersRV.setHasFixedSize(true);
        binding.ordersRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        getData();
    }

    private void getData() {
        ongoingOrdersAdapter = new OngoingOrdersAdapter(arrWeedList, getActivity());
        ongoingOrdersAdapter.setWeedItemCick(this);
        binding.ordersRV.setAdapter(ongoingOrdersAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setanimationView(binding.ordersRV);
            }
        }, 500);
    }



    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        //setanimationView(binding.topLL);
    }


    @Override
    public void onItemClick(int position) {

        mainActivity.slidingRootNav.setMenuLocked(true);
        mainActivity.pushFragment(new OrderDetailFragment(), true, false, true,
                OrderDetailFragment.class.getSimpleName(), null, false);
    }

    @Override
    public void onTrackDeliveryClick(int position) {

        //Bungee.zoom(getActivity());
        mainActivity.slidingRootNav.setMenuLocked(true);
        TrackDeliveryFragment trackDeliveryFragment = new TrackDeliveryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.mCLASS, "OngoingOrder");
        trackDeliveryFragment.setArguments(bundle);
        mainActivity.pushFragment(trackDeliveryFragment, true, false, true,
                TrackDeliveryFragment.class.getSimpleName(), null, false);
    }

    @Override
    public void onViewAddOns(int position) {

    }
}
