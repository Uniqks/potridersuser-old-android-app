package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.CartFoodAdapter;
import dev.env.potriders.activity.adapter.CartWeedAdapter;
import dev.env.potriders.databinding.FragmentCartBinding;
import dev.env.potriders.model.CategoryDetails;

public class CartFragment extends BaseFragment implements View.OnClickListener, CartFoodAdapter.WeedItemCick, CartWeedAdapter.WeedItemCick {

    FragmentCartBinding binding;

    CartFoodAdapter cartFoodAdapter;
    CartWeedAdapter cartWeedAdapter;

    List<CategoryDetails> arrWeedList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.proceedTV.setOnClickListener(this);

        setToolbarIconTitle("My Cart", false, true, false,
                false, false, false, false);

        binding.rvCartFood.setHasFixedSize(true);
        binding.rvCartFood.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        binding.rvCartWeed.setHasFixedSize(true);
        binding.rvCartWeed.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        getData();
    }

    private void getData() {
        cartFoodAdapter = new CartFoodAdapter(arrWeedList, getActivity());
        cartFoodAdapter.setWeedItemCick(this);
        binding.rvCartFood.setAdapter(cartFoodAdapter);
        cartWeedAdapter = new CartWeedAdapter(arrWeedList, getActivity());
        cartWeedAdapter.setWeedItemCick(this);
        binding.rvCartWeed.setAdapter(cartWeedAdapter);
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setanimationView(binding.rvCartFood);
                binding.rvCartFood.setVisibility(View.VISIBLE);
            }
        },500);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.proceedTV:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new AddressFragment(), true, false, true,
                        AddressFragment.class.getSimpleName(), null, false);
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        //setanimationView(binding.topLL);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("My Cart", false, true, false,
                    false, false, false, false);
        }
    }


    @Override
    public void onItemClick(int position) {

    }
}
