package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.ManageAddressAdapter;
import dev.env.potriders.databinding.FragmentManageAddressBinding;
import dev.env.potriders.model.Address;
import dev.env.potriders.model.CommonModel;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageAddressFragment extends BaseFragment implements View.OnClickListener {

    private List<Address> addressList;
    ManageAddressAdapter addressListAdapter;

    FragmentManageAddressBinding binding;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manage_address, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.manage_addresses), false, true, false,
                false, false, false, false);

        binding.buttonAddNewAddress.setOnClickListener(this);

        addressList = new ArrayList<>();
        addressList.add(new Address("C-204, EDEN, Godrej Garden City, Nr. Nirma University, Jagatpur, Ahmedabad, Gujarat, India, 382421", "Home"));
        addressList.add(new Address("C-204, EDEN, Godrej Garden City, Nr. Nirma University, Jagatpur, Ahmedabad, Gujarat, India, 382421", "Home"));
        addressList.add(new Address("C-204, EDEN, Godrej Garden City, Nr. Nirma University, Jagatpur, Ahmedabad, Gujarat, India, 382421", "Home"));
        addressList.add(new Address("C-204, EDEN, Godrej Garden City, Nr. Nirma University, Jagatpur, Ahmedabad, Gujarat, India, 382421", "Home"));
        addressList.add(new Address("C-204, EDEN, Godrej Garden City, Nr. Nirma University, Jagatpur, Ahmedabad, Gujarat, India, 382421", "Home"));
        binding.addressList.setLayoutManager(new LinearLayoutManager(mainActivity));
        addressListAdapter = new ManageAddressAdapter(addressList, mainActivity);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setanimationView(binding.addressList);
                binding.addressList.setVisibility(View.VISIBLE);
            }
        }, 500);

        binding.addressList.setAdapter(addressListAdapter);



        if (Utils.isNetworkAvailable(mainActivity)) {
           // getAddressList();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }

    private void getAddressList() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonModel<List<Address>>> call = apiInterface.getAddressList(
                PreferenceUtil.getPref(mainActivity).getInt(PreferenceKeys.USER_ID, 0));
        showProgressDialog();
        call.enqueue(new Callback<CommonModel<List<Address>>>() {
            @Override
            public void onResponse(Call<CommonModel<List<Address>>> call, Response<CommonModel<List<Address>>> response) {
                hideProgressDialog();

                if (response.body().getStatus() == Constants.SUCCESS) {
                    addressList.addAll(response.body().getData());
                    if (addressList.size() > 0) {
                        addressListAdapter.notifyDataSetChanged();
                    }
                } else {
                    CommonUtils.makeToast(response.body().getMsg(), mainActivity);
                }
            }

            @Override
            public void onFailure(Call<CommonModel<List<Address>>> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buttonAddNewAddress:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }
}
