package dev.env.potriders.activity.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.RateGalleryAdapter;
import dev.env.potriders.databinding.FragmentRatingBinding;
import dev.env.potriders.utils.LogUtils;
import dev.env.potriders.utils.Utils;


public class RateFragment extends BaseFragment implements OnClickListener, RateGalleryAdapter.WeedItemCick {

    FragmentRatingBinding binding;

    private int STORAGE_PERMISSION_CODE = 11;
    public static final int SELECT_PHOTO = 1;
    public static final int TAKENPHOTO = 0;
    Uri uri;
    File photofile;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public String photo_path = "";

    ArrayList<String> arrImagePaths = new ArrayList<>();
    RateGalleryAdapter rateGalleryAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rating, container, false);




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_rating), false, true, false,
                false, false, false, false);

        binding.uploadImagesLL.setOnClickListener(this);
        binding.submitTV.setOnClickListener(this);

        rateGalleryAdapter = new RateGalleryAdapter(arrImagePaths, mainActivity);
        rateGalleryAdapter.setWeedItemCick(this);
        binding.imagesList.setLayoutManager(new LinearLayoutManager(mainActivity, LinearLayoutManager.HORIZONTAL, false));
        binding.imagesList.setAdapter(rateGalleryAdapter);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == STORAGE_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            showPopup();
            //takePhotoFromCamera() ;
        else {
            Utils.getInstance(mainActivity).Toast(mainActivity, getString(R.string.enable_perm), false);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;

            case R.id.uploadImagesLL:
                if (!hasPermissions(mainActivity, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(mainActivity, PERMISSIONS, STORAGE_PERMISSION_CODE);
                } else {
                    showPopup();
                }
                break;

            case R.id.submitTV:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void showPopup() {

        Builder builder = new Builder(mainActivity);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.str_select_one));
        builder.setMessage(getString(R.string.str_select_gallery_msg));
        builder.setNegativeButton(getString(R.string.str_camera).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                takePhotoFromCamera();
            }
        });
        builder.setPositiveButton(getString(R.string.str_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), SELECT_PHOTO);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity,
                R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    private void takePhotoFromCamera() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            photofile = Utils.getInstance(mainActivity).create_file(".jpg");
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            uri = FileProvider.getUriForFile(mainActivity, "dev.env.potriders.fileprovider", photofile);
        } else {
            photofile = Utils.getInstance(mainActivity).create_Image_File(".jpg");
            uri = Uri.fromFile(photofile);

        }
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(Intent.createChooser(cameraIntent, getString(R.string.lbl_capture)), TAKENPHOTO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == TAKENPHOTO) && (resultCode == Activity.RESULT_OK)) {

            if (uri != null) {

                photo_path = photofile.getAbsolutePath();//uri.getPath();

                LogUtils.e(uri.getPath() + " imageFilePath " + new File(uri.toString()).getAbsolutePath());
                if (photo_path != null && photo_path.length() > 0) {
                    arrImagePaths.add(photo_path);
                }
            } else {

                Utils.getInstance(mainActivity).Toast(mainActivity, getString(R.string.toast_unable_to_selct_image), false);
            }

        } else if ((requestCode == SELECT_PHOTO) && (resultCode == Activity.RESULT_OK)) {


            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = mainActivity.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            photo_path = cursor.getString(columnIndex);
            try {
                LogUtils.i("", "SELECT_PHOTO " + photo_path);
                arrImagePaths.add(photo_path);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (arrImagePaths.size() > 0)
            for (int i = 0; i < arrImagePaths.size(); i++)
                LogUtils.i("RateFragment" + " onActivityResult arrImagePaths " + arrImagePaths.get(i));

        if (arrImagePaths.size() > 0) {
            rateGalleryAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onClearClick(int position) {
        if (arrImagePaths.size() > 0) {
            arrImagePaths.remove(position);
            rateGalleryAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
            setToolbarIconTitle(getString(R.string.str_rating), false, true, false,
                    false, false, false, false);
        }
    }
}
