package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentWeedDescriptionBinding;

public class WeedDescriptionFragment extends Fragment {



    FragmentWeedDescriptionBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weed_description, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to intialized all the controls...
     */
    private void initControls() {

        Bundle bundle = this.getArguments();
        String description = bundle.getString("description");
        //binding.descriptionTV.setText(Html.fromHtml(description));
    }



    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


}
