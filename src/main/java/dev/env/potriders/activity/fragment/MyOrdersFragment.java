package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.CartFoodAdapter;
import dev.env.potriders.activity.adapter.CartWeedAdapter;
import dev.env.potriders.activity.adapter.ViewPagerAdapter;
import dev.env.potriders.databinding.FragmentMyOrdersBinding;

public class MyOrdersFragment extends BaseFragment implements CartFoodAdapter.WeedItemCick, CartWeedAdapter.WeedItemCick {

    FragmentMyOrdersBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);







        initControls();
        return binding.getRoot();
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new OngoingOrdersFragment(), "Ongoing Orders");
        adapter.addFrag(new PastOrdersFragment(), "Past Orders");
        viewPager.setAdapter(adapter);
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("My Orders", true, false, false,
                false, true, false, false);
        setupViewPager(binding.viewPager);
        binding.bubbleTab.setupWithViewPager(binding.viewPager);
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
            setToolbarIconTitle("My Orders", true, false, false,
                    false, true, false, false);
        }
    }
}
