package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.ViewPagerAdapter;
import dev.env.potriders.databinding.FragmentHomeBinding;


public class HomeFragment extends BaseFragment {


    FragmentHomeBinding binding;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        setToolbarIconTitle("", true, false, false, false,
                false, false, true);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {

        setToolbarIconTitle("", true, false, true, false,
                true, false, true);
        setupViewPager(binding.viewPager);
        binding.bubbleTab.setupWithViewPager(binding.viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new HomeWeedFragment(), "Weed");
        adapter.addFrag(new HomeFoodFragment(), "Food");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
        }

        return super.onOptionsItemSelected(item); // important line
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
            setToolbarIconTitle("", true, false, true, false,
                    true, false, true);
        }
    }
}
