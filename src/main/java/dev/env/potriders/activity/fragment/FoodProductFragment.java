package dev.env.potriders.activity.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.FoodProductAdapter;
import dev.env.potriders.activity.dialog.AddToCartDialog;
import dev.env.potriders.activity.viewActivity.SearchActivity;
import dev.env.potriders.databinding.FragmentFoodProductBinding;
import dev.env.potriders.model.CommonModel;
import dev.env.potriders.model.ProductDetails;
import dev.env.potriders.model.WeightModel;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FoodProductFragment extends BaseFragment implements View.OnClickListener, FoodProductAdapter.FoodItemCick
        , FoodProductAdapter.CartItemCick, TextWatcher, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener {


    FragmentFoodProductBinding binding;
    ArrayList<ProductDetails> arrWeedList = new ArrayList<>();
    FoodProductAdapter foodAdapter;

    private List<WeightModel> brandModels = new ArrayList<>();
    private List<WeightModel> typeModels = new ArrayList<>();
    private List<WeightModel> weightModels = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_food_product, container, false);






        initControls();
        setSpinnerBrandList();
        setSpinnerTypeList();
        setSpinnerWeightList();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("", false, true, false,
                false, true, false, true);

        binding.searchEdt.addTextChangedListener(this);
        binding.clearImg.setOnClickListener(this);

        binding.dropdownIV.setOnClickListener(this);

        binding.spnBrand.setOnItemSelectedListener(this);
        binding.spnBrand.setEnabled(true);

        binding.spnType.setOnItemSelectedListener(this);
        binding.spnType.setEnabled(true);

        binding.spnWeight.setOnItemSelectedListener(this);
        binding.spnWeight.setEnabled(true);

        binding.refreshSwipe.setOnRefreshListener(this);
        binding.refreshSwipe.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        binding.rvFoodProList.setHasFixedSize(true);
        foodAdapter = new FoodProductAdapter(arrWeedList, mainActivity);
        foodAdapter.setFoodItemCick(this);
        foodAdapter.setCartItemCick(this);
        binding.rvFoodProList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.rvFoodProList.setAdapter(foodAdapter);

        if (Utils.isNetworkAvailable(mainActivity)) {
            getFoodList();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }


    /**
     *  get food list....
     */
    private void getFoodList() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonModel<List<ProductDetails>>> call = apiInterface.getWeedItemList(1);
        showProgressDialog();
        call.enqueue(new Callback<CommonModel<List<ProductDetails>>>() {
            @Override
            public void onResponse(Call<CommonModel<List<ProductDetails>>> call, Response<CommonModel<List<ProductDetails>>> response) {
                hideProgressDialog();

                if (response.body().getStatus() == Constants.SUCCESS) {
                    arrWeedList.addAll(response.body().getData());

                    binding.refreshSwipe.setRefreshing(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setanimationView(binding.rvFoodProList);
                            binding.rvFoodProList.setVisibility(View.VISIBLE);
                        }
                    }, 500);
                    foodAdapter.notifyDataSetChanged();
                } else {

                    CommonUtils.makeToast(response.body().getMsg(), mainActivity);
                    binding.refreshSwipe.setVisibility(View.GONE);
                    binding.llNoItem.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<CommonModel<List<ProductDetails>>> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.refreshSwipe.setRefreshing(false);
                getFoodList();
            }
        }, 2600);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.dropdownIV:
                hideKeyboard();
                if (binding.searchLL.getVisibility() == View.VISIBLE) {
                    binding.searchLL.setVisibility(View.GONE);
                } else if (binding.searchLL.getVisibility() == View.GONE) {
                    setanimationView(binding.searchLL);
                    binding.searchLL.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.clearImg:
                hideKeyboard();
                binding.searchEdt.setText("");
                binding.clearImg.setVisibility(View.GONE);
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemClick(int position) {
        mainActivity.slidingRootNav.setMenuLocked(true);

        Bundle bundle = new Bundle();
        bundle.putInt("item_id", arrWeedList.get(position).getId());
        mainActivity.pushFragment(new FoodDetailFragment(), true, false, true,
                FoodDetailFragment.class.getSimpleName(), bundle, false);
    }

    @Override
    public void onCartClick(int position) {
        AddToCartDialog dialog = new AddToCartDialog();
        dialog.show(getFragmentManager(), "Add To Cart");
    }



    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (binding.searchEdt.getText().length() > 0) binding.clearImg.setVisibility(View.VISIBLE);
        else binding.clearImg.setVisibility(View.GONE);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    /**
     * Method is used to initialized hide keyboard...
     */
    private void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("", false, true, false,
                    false, true, false, true);
        }
    }


    /**
     *  Method is used to set the brand list...
     */
    private void setSpinnerBrandList() {

        brandModels.add(new WeightModel("Brand"));
        brandModels.add(new WeightModel("Brand 1"));
        brandModels.add(new WeightModel("Brand 2"));
        brandModels.add(new WeightModel("Brand 3"));
        brandModels.add(new WeightModel("Brand 4"));
        brandModels.add(new WeightModel("Brand 5"));
        brandModels.add(new WeightModel("Brand 6"));
        brandModels.add(new WeightModel("Brand 7"));
        brandModels.add(new WeightModel("Brand 8"));
        brandModels.add(new WeightModel("Brand 9"));

        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(getActivity(), R.id.txt_title, brandModels, getResources());
        binding.spnBrand.setAdapter(categoryListAdapter);
    }


    /**
     *  Method is used to set the type list...
     */
    private void setSpinnerTypeList() {

        typeModels.add(new WeightModel("Type"));
        typeModels.add(new WeightModel("Savita"));
        typeModels.add(new WeightModel("Indica"));
        typeModels.add(new WeightModel("Edibles"));
        typeModels.add(new WeightModel("Hybrid"));
        typeModels.add(new WeightModel("Concentrate"));
        typeModels.add(new WeightModel("Tincture"));


        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(getActivity(), R.id.txt_title, typeModels, getResources());
        binding.spnType.setAdapter(categoryListAdapter);
    }


    /**
     *  Method is used to set the weight list...
     */
    private void setSpinnerWeightList() {

        weightModels.add(new WeightModel("Weight"));
        weightModels.add(new WeightModel("50 grams"));
        weightModels.add(new WeightModel("100 grams"));
        weightModels.add(new WeightModel("1 kg"));
        weightModels.add(new WeightModel("2 kg"));
        weightModels.add(new WeightModel("5 kg"));


        CategoryListAdapter categoryListAdapter = new CategoryListAdapter(getActivity(), R.id.txt_title, weightModels, getResources());
        binding.spnWeight.setAdapter(categoryListAdapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.spnBrand:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }





    /**
     *  category list adapter...
     */
    public class CategoryListAdapter extends ArrayAdapter<WeightModel> {
        private Context context;
        private List<WeightModel> data;
        public Resources res;
        WeightModel userData = null;
        LayoutInflater inflater;

        public CategoryListAdapter(Context context, int textViewResourceId, List<WeightModel> data, Resources res) {
            super(context, textViewResourceId, data);

            this.context = context;
            this.data = data;
            this.res = res;
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            View view = inflater.inflate(R.layout.row_spinner_list, parent, false);

            userData = null;
            userData = (WeightModel) data.get(position);
            TextView mCatNameTxt;

            ((TextView) view.findViewById(R.id.txt_cat)).setText(userData.getName());

            return view;
        }
    }
}
