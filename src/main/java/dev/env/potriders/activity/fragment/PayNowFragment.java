package dev.env.potriders.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import dev.env.potriders.R;
import dev.env.potriders.activity.dialog.ThankYouDialog;
import dev.env.potriders.databinding.FragmentPayNowBinding;


public class PayNowFragment extends BaseFragment implements View.OnClickListener {


    FragmentPayNowBinding binding;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pay_now, container, false);



        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_payment), false, true, false,
                false, false, false, false);

        binding.payNowTV.setOnClickListener(this);

        setanimationView(binding.llTop);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;

            case R.id.payNowTV:
                mainActivity.slidingRootNav.setMenuLocked(true);
                ThankYouDialog thankYouFragment = new ThankYouDialog();
                thankYouFragment.show(getFragmentManager(), "Thank you");
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle(getString(R.string.str_payment), false, true, false,
                    false, false, false, false);
        }
    }
}
