package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentAboutusBinding;
import dev.env.potriders.databinding.FragmentPrivacyBinding;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsFragment extends BaseFragment {

    FragmentAboutusBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_aboutus, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {


        setToolbarIconTitle(getString(R.string.str_about_us), false, true, false,
                false, false, false, false);

        setAnimationView(binding.topLL);

        if (Utils.isNetworkAvailable(mainActivity)) {
            getAboutUsContentService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }



    private void getAboutUsContentService() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);
        Call<String> call = apiInterface.getPages(1);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String content = jsonObject1.getString("content");
                        binding.aboutUsTV.setText(Html.fromHtml(content));
                    } else {
                        CommonUtils.makeToast(msg, mainActivity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }


    public void setAnimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        setAnimationView(binding.topLL);
    }
}
