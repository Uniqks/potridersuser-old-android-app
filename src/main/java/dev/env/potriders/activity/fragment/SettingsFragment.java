package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentSettingsBinding;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;

public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    FragmentSettingsBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to intialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("Settings", true, false, false, false,
                false, false, false);

        setanimationView(binding.llTop);

        binding.privacyPolicyRL.setOnClickListener(this);
        binding.aboutUsRL.setOnClickListener(this);
        binding.contactUsRL.setOnClickListener(this);
        binding.llMyAddress.setOnClickListener(this);

        if (PreferenceUtil.getPref(mainActivity).getBoolean(PreferenceKeys.IS_LOGIN, false)) {
            //binding.llMyAddress.setVisibility(View.VISIBLE);
            binding.view.setVisibility(View.VISIBLE);
        } else {
           // binding.llMyAddress.setVisibility(View.GONE);
            binding.view.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.privacyPolicyRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new PrivacyFragment(), true, false, true,
                        PrivacyFragment.class.getSimpleName(), null, false);
                break;

            case R.id.aboutUsRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new AboutUsFragment(), true, false, true,
                        AboutUsFragment.class.getSimpleName(), null, false);
                break;

            case R.id.contactUsRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new ContactUsFragment(), true, false, true,
                        ContactUsFragment.class.getSimpleName(), null, false);
                break;

            case R.id.llMyAddress:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new ManageAddressFragment(), true, false, true,
                        ManageAddressFragment.class.getSimpleName(), null, false);
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("Settings", true, false, false, false,
                    false, false, false);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        setToolbarIconTitle("Settings", true, false, false, false,
                false, false, false);
    }
}
