package dev.env.potriders.activity.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.FullDescriptionActivity;
import dev.env.potriders.activity.adapter.WeedReviewsAdapter;
import dev.env.potriders.databinding.FragmentWeedReviewBinding;
import dev.env.potriders.model.WeedDetail;
import dev.env.potriders.model.WeedReviewsModel;

public class WeedReviewsFragment extends Fragment implements WeedReviewsAdapter.WeedItemCick {


    ArrayList<WeedReviewsModel> arrWeedReviewsList = new ArrayList<>();
    FragmentWeedReviewBinding binding;
    WeedReviewsAdapter weedReviewsAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weed_review, container, false);





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        Bundle bundle = this.getArguments();
        weedReviewsAdapter = new WeedReviewsAdapter(arrWeedReviewsList, getActivity());

        arrWeedReviewsList.add(new WeedReviewsModel("", "", "", "5", "", "", "0"));
        arrWeedReviewsList.add(new WeedReviewsModel("", "", "", "4", "", "", "1"));
        arrWeedReviewsList.add(new WeedReviewsModel("", "", "", "3", "", "", "2"));
        arrWeedReviewsList.add(new WeedReviewsModel("", "", "", "4.5", "", "", "3"));
        arrWeedReviewsList.add(new WeedReviewsModel("", "", "", "3.5", "", "", "4"));
        arrWeedReviewsList.add(new WeedReviewsModel("", "", "", "2.5", "", "", "7"));

        weedReviewsAdapter.setWeedItemCick(this);

        binding.rvReviews.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvReviews.setAdapter(weedReviewsAdapter);
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onSeeMoreClick() {
        startActivity(new Intent(getActivity(), FullDescriptionActivity.class));
    }
}
