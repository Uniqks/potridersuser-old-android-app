package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentWeedEffectBinding;

public class WeedEffectsFragment extends Fragment {


    FragmentWeedEffectBinding binding;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weed_effect, container, false);





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        Bundle bundle = this.getArguments();
       // int relaxed_per = bundle.getInt("relaxed_per", 0);
        int euphoric_per = bundle.getInt("euphoric_per", 0);
        int happy_per = bundle.getInt("happy_per", 0);
        int uplifted_per = bundle.getInt("uplifted_per", 0);

        binding.pbRelaxed.setProgress(89);
        binding.pbEuphoric.setProgress(euphoric_per);
        binding.pbHappy.setProgress(happy_per);
        binding.pbUplifted.setProgress(uplifted_per);

        setProgressDrawable(89, binding.pbRelaxed);
        setProgressDrawable(euphoric_per, binding.pbEuphoric);
        setProgressDrawable(happy_per, binding.pbHappy);
        setProgressDrawable(uplifted_per, binding.pbUplifted);

        binding.myTextRelaxed.setText("Relaxed " + 89 + "%");
        binding.myTextEuphoric.setText("Euphoric " + euphoric_per + "%");
        binding.myTextHappy.setText("Happy " + happy_per + "%");
        binding.myTextUplifted.setText("Uplifted " + uplifted_per + "%");


    }

    public void setProgressDrawable(int progress, ProgressBar progressBar) {
        if (progress < 30) {
            progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_bg_red));
        } else if (progress > 30 && progress < 80) {
            progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_bg_orange));
        } else {
            progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_bg_green));
        }
    }



    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


}
