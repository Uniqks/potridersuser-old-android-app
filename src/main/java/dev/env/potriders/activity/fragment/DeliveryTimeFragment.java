package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.SelectDateTimeAdapter;
import dev.env.potriders.databinding.FragmentSelectDeliveryTimeBinding;
import dev.env.potriders.utils.LogUtils;


public class DeliveryTimeFragment extends BaseFragment implements OnClickListener {

    FragmentSelectDeliveryTimeBinding binding;

    String[] fruits = {"More Options", "1G", "2G", "3G"};
    String[] fruits1 = {"6.00 PM - 6.30 PM", "7.00 PM - 7.30 PM", "8.00 PM - 8.30 PM"};

    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("MMM, E dd", Locale.getDefault());

    Calendar calendar;

    ArrayList<String> arr_date = new ArrayList<>();
    ArrayList<String> arr_time = new ArrayList<>();



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_delivery_time, container, false);





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_delivery_time), false, true, false,
                false, false, false, false);

        calendar = Calendar.getInstance();
        fruits[0] = dateFormatForDisplaying.format(new Date(calendar.getTimeInMillis()));

        calendar.add(Calendar.DAY_OF_WEEK, 1);

        fruits[1] = dateFormatForDisplaying.format(new Date(calendar.getTimeInMillis()));

        calendar.add(Calendar.DAY_OF_WEEK, 1);

        fruits[2] = dateFormatForDisplaying.format(new Date(calendar.getTimeInMillis()));

        calendar.add(Calendar.DAY_OF_WEEK, 1);

        fruits[3] = dateFormatForDisplaying.format(new Date(calendar.getTimeInMillis()));

        for (int i = 0; i < fruits.length; i++)
        arr_date = new ArrayList<>(Arrays.asList(fruits));
        arr_time = new ArrayList<>(Arrays.asList(fruits1));

        initCustomSpinner(arr_date, arr_time);
        binding.doneTV.setOnClickListener(this);
    }


    private void initCustomSpinner(ArrayList<String> countries, ArrayList<String> countries1) {

        SelectDateTimeAdapter weedMoreOptionsAdapter = new SelectDateTimeAdapter(mainActivity, countries);
        binding.spinnerSelectDate.setAdapter(weedMoreOptionsAdapter);
        binding.spinnerSelectDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SelectDateTimeAdapter weedMoreOptionsAdapter1 = new SelectDateTimeAdapter(mainActivity, countries1);
        binding.spinnerSelectTime.setAdapter(weedMoreOptionsAdapter1);
        binding.spinnerSelectTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;

            case R.id.doneTV:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new CheckoutFragment(), true, false, true,
                        CheckoutFragment.class.getSimpleName(), null, false);
                break;

        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle(getString(R.string.str_delivery_time), false, true, false,
                    false, false, false, false);
        }
    }
}
