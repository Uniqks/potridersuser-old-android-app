package dev.env.potriders.activity.fragment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.CustomPagerAdapter;
import dev.env.potriders.activity.adapter.FoodCatListAdapter;
import dev.env.potriders.activity.adapter.FoodItemAdapter;
import dev.env.potriders.activity.dialog.AddToCartDialog;
import dev.env.potriders.activity.viewActivity.SearchActivity;
import dev.env.potriders.databinding.FragmentFoodItemDetailBinding;
import dev.env.potriders.model.CommonModel;
import dev.env.potriders.model.WeedDetail;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoodDetailFragment extends BaseFragment implements View.OnClickListener {

    FragmentFoodItemDetailBinding binding;

    ArrayList<String> arr_more_options_weed = new ArrayList<>();
    List<WeedDetail.ItemReview> itemReviewList;

    private String description = "";
    private int uplifted_per = 0;
    private int happy_per = 0;
    private int euphoric_per = 0;
    private int relaxed_per = 19;

    private int mWeedCount = 1;
    ArrayList<String> mImageUrls;
    private int mGetItemId;

    /*ArrayList<String> arrCategoryList = new ArrayList<>();
    ArrayList<String> arrFoodList = new ArrayList<>();
    FoodCatListAdapter foodCategoryAdapter;
    FoodItemAdapter foodItemAdapter;*/

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_food_item_detail, container, false);

        if (getArguments() != null) {

            Bundle bundle = this.getArguments();
            mGetItemId = bundle.getInt("item_id", 0);
            if (Utils.isNetworkAvailable(mainActivity)) {
                getWeedDetail(mGetItemId);
            } else {
                CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
            }
        }





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("Fruits & Maple Oatmeal", false, true, false,
                false, false, false, false);


        setanimationView(binding.llTop);

        binding.descriptionTV.setOnClickListener(this);
        binding.effectsTV.setOnClickListener(this);
        binding.reviewsTV.setOnClickListener(this);

        binding.cartLL.setOnClickListener(this);
        binding.moreTV.setOnClickListener(this);
        binding.incrementIV.setOnClickListener(this);
        binding.decrementIV.setOnClickListener(this);
        binding.addtocartLL.setOnClickListener(this);

        itemReviewList = new ArrayList<>();

        arr_more_options_weed = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_more_options_weed)));



        Bundle bundle = new Bundle();
        bundle.putString("description", description);
        WeedDescriptionFragment fragment = new WeedDescriptionFragment();
        fragment.setArguments(bundle);
        FragmentTransaction mtransaction = getActivity().getSupportFragmentManager().beginTransaction();
        mtransaction.replace(R.id.fragment_container, fragment).commit();
        binding.descriptionTV.setBackgroundResource(R.drawable.category_blue_background);
        binding.effectsTV.setBackgroundResource(R.drawable.category_gray_background);
        binding.reviewsTV.setBackgroundResource(R.drawable.category_gray_background);

        binding.descriptionTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        binding.effectsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        binding.reviewsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

       /* foodCategoryAdapter = new FoodCatListAdapter(arrCategoryList, getActivity());
        foodItemAdapter = new FoodItemAdapter(arrFoodList, getActivity());

        for (int i = 0; i < 6; i++) {
            arrCategoryList.add("Breakfast");
        }
        for (int i = 0; i < 3; i++) {
            arrFoodList.add("Breakfast");
        }
        binding.categoryRL.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        binding.categoryRL.setAdapter(foodCategoryAdapter);

        binding.subCategoriesRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.subCategoriesRV.setAdapter(foodItemAdapter);*/
    }


    private void getWeedDetail(int item_id) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonModel<WeedDetail>> call = apiInterface.getWeedDetail(item_id);
        showProgressDialog();
        call.enqueue(new Callback<CommonModel<WeedDetail>>() {
            @Override
            public void onResponse(Call<CommonModel<WeedDetail>> call, Response<CommonModel<WeedDetail>> response) {

                hideProgressDialog();

                if (response.body().getStatus() == Constants.SUCCESS) {

                    binding.weedNameTV.setText(response.body().getData().getName());
                    //binding.priceRangeTV.setText(response.body().getData().getPrice());
                    //binding.categoriesTV.setText(response.body().getData().getMenuName());
                    description = response.body().getData().getDesc();
                    relaxed_per = response.body().getData().getRelaxedPer();
                    euphoric_per = response.body().getData().getEuphoricPer();
                    happy_per = response.body().getData().getHappyPer();
                    uplifted_per = response.body().getData().getUpliftedPer();
                    itemReviewList.addAll(response.body().getData().getItemReview());


                    //View Pager for additional images
                    mImageUrls = new ArrayList<>();
                    mImageUrls.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png");
                    mImageUrls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png");
                    mImageUrls.add("https://homepages.cae.wisc.edu/~ece533/images/fruits.png");
                    binding.viewPagerWeed.setAdapter(new CustomPagerAdapter(mainActivity, mImageUrls));

                    /*Glide.with(mainActivity)
                            .load(CommonUtils.getImageUrl(mainActivity) + "" + response.body().getData().getImage())
                            .into(binding.imageViewMain);*/

                    binding.descriptionTV.setBackgroundResource(R.drawable.category_blue_background);
                    binding.effectsTV.setBackgroundResource(R.drawable.category_gray_background);
                    binding.reviewsTV.setBackgroundResource(R.drawable.category_gray_background);

                    binding.descriptionTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    binding.effectsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                    binding.reviewsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                    Bundle bundle = new Bundle();
                    bundle.putString("description", description);
                    WeedDescriptionFragment fragment = new WeedDescriptionFragment();
                    fragment.setArguments(bundle);
                    FragmentTransaction mtransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    mtransaction.replace(R.id.fragment_container, fragment).commit();

                } else {
                    CommonUtils.makeToast(response.body().getMsg(), mainActivity);
                }
            }

            @Override
            public void onFailure(Call<CommonModel<WeedDetail>> call, Throwable t) {
                // CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {


            case R.id.descriptionTV:
                bundle.putString("description", description);
                WeedDescriptionFragment fragment = new WeedDescriptionFragment();
                fragment.setArguments(bundle);
                FragmentTransaction mtransaction = getActivity().getSupportFragmentManager().beginTransaction();
                mtransaction.replace(R.id.fragment_container, fragment).commit();
                binding.descriptionTV.setBackgroundResource(R.drawable.category_blue_background);
                binding.effectsTV.setBackgroundResource(R.drawable.category_gray_background);
                binding.reviewsTV.setBackgroundResource(R.drawable.category_gray_background);

                binding.descriptionTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                binding.effectsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                binding.reviewsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                break;

            case R.id.effectsTV:

                FragmentTransaction mtransaction1 = getActivity().getSupportFragmentManager().beginTransaction();
                bundle.putInt("relaxed_per", relaxed_per);
                bundle.putInt("euphoric_per", euphoric_per);
                bundle.putInt("happy_per", happy_per);
                bundle.putInt("uplifted_per", uplifted_per);
                WeedEffectsFragment fragment1 = new WeedEffectsFragment();
                fragment1.setArguments(bundle);
                mtransaction1.replace(R.id.fragment_container, fragment1).commit();
                binding.descriptionTV.setBackgroundResource(R.drawable.category_gray_background);
                binding.effectsTV.setBackgroundResource(R.drawable.category_blue_background);
                binding.reviewsTV.setBackgroundResource(R.drawable.category_gray_background);

                binding.descriptionTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                binding.effectsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                binding.reviewsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                break;


            case R.id.reviewsTV:

                FragmentTransaction mtransaction2 = getActivity().getSupportFragmentManager().beginTransaction();
                bundle.putParcelableArrayList("review_list", (ArrayList<? extends Parcelable>) itemReviewList);
                WeedReviewsFragment reviewsFragment = new WeedReviewsFragment();
                reviewsFragment.setArguments(bundle);
                mtransaction2.replace(R.id.fragment_container, reviewsFragment).commit();
                binding.descriptionTV.setBackgroundResource(R.drawable.category_gray_background);
                binding.effectsTV.setBackgroundResource(R.drawable.category_gray_background);
                binding.reviewsTV.setBackgroundResource(R.drawable.category_blue_background);

                binding.descriptionTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                binding.effectsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                binding.reviewsTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                break;

            case R.id.cartLL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new CartFragment(), true, false, true,
                        CartFragment.class.getSimpleName(), null, false);
                break;

            case R.id.moreTV:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(getActivity(), SearchActivity.class);
                intent.putStringArrayListExtra(Utils.EXTRA_SEARCH_LIST, arr_more_options_weed);
                intent.putExtra(Utils.EXTRA_SEARCH_TITLE, Utils.TITLE_MORE_OPTIONS);
                startActivityForResult(intent, Utils.REQUEST_CODE_SEARCH_MORE_OPTIONS_WEED);*/
                break;

            case R.id.incrementIV:
                changeQuantity(true);
                break;

            case R.id.decrementIV:
                changeQuantity(false);
                break;

            case R.id.addtocartLL:
                Toast.makeText(getActivity(), "item added successfully.", Toast.LENGTH_SHORT).show();
                //AddToCartDialog dialog = new AddToCartDialog();
                //dialog.show(getFragmentManager(), "Add To Cart");
                break;
        }
    }

    private void changeQuantity(boolean isPlus){
        if (isPlus){
            mWeedCount = mWeedCount + 1;
        } else {
            mWeedCount = mWeedCount - 1;
            if (mWeedCount < 1){
                mWeedCount = 1;
            }
        }
        binding.quantityTV.setText(String.valueOf(mWeedCount));
    }

    public void setanimationView(final View topLL) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
                topLL.startAnimation(bottomUp);
                topLL.setVisibility(View.VISIBLE);
            }
        }, 800);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == Utils.REQUEST_CODE_SEARCH_MORE_OPTIONS_WEED && resultCode == Activity.RESULT_OK) {

            if (data != null)
                if (data.getExtras().getString(Utils.EXTRA_SELECTED_SEARCH_POS) != null && !data.getExtras().getString(Utils.EXTRA_SELECTED_SEARCH_POS).equals(""))
                    binding.moreTV.setText(arr_more_options_weed.get(Integer.parseInt(data.getExtras().getString(Utils.EXTRA_SELECTED_SEARCH_POS))));
        }

    }




    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("Fruits & Maple Oatmeal", false, true, false,
                    false, false, false, false);
        }
    }
}
