package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentOrderDetailBinding;


public class OrderDetailFragment extends BaseFragment implements OnClickListener {

    FragmentOrderDetailBinding binding;
    private String mGetOrderNo;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_detail, container, false);







        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mGetOrderNo = "Order No. #9894";
        setToolbarIconTitle(mGetOrderNo, false, true, false,
                false, false, false, false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle(mGetOrderNo, false, true, false,
                    false, false, false, false);
        }
    }
}
