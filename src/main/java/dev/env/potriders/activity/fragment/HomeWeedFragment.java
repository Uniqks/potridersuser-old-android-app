package dev.env.potriders.activity.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.adapter.WeedCategoryAdapter;
import dev.env.potriders.app.Potrider;
import dev.env.potriders.databinding.FragmentWeedBinding;
import dev.env.potriders.model.CategoryDetails;
import dev.env.potriders.model.CommonModel;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeWeedFragment extends BaseFragment implements View.OnClickListener, WeedCategoryAdapter.WeedItemCick, TextWatcher,
        SwipeRefreshLayout.OnRefreshListener {


    private Potrider mPotrider;
    FragmentWeedBinding binding;

    private WeedCategoryAdapter weedCategoryAdapter;
    private List<CategoryDetails> arrWeedCatList = new ArrayList<>();
    private GridLayoutManager layoutManager;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weed, container, false);

        mPotrider = (Potrider) getActivity().getApplication();



        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {


        binding.searchEdt.addTextChangedListener(this);
        binding.clearImg.setOnClickListener(this);

        binding.refreshSwipe.setOnRefreshListener(this);
        binding.refreshSwipe.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        weedCategoryAdapter = new WeedCategoryAdapter(arrWeedCatList, mainActivity);
        weedCategoryAdapter.setWeedItemCick(this);
        binding.rvWeedList.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getActivity(), 2);
        binding.rvWeedList.setLayoutManager(layoutManager);
      //  binding.rvWeedList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
        binding.rvWeedList.setAdapter(weedCategoryAdapter);

        if (Utils.isNetworkAvailable(mainActivity)) {
            getWeedCatList();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }


    /**
     *  Method is used to initialized get weed category list....
     */
    private void getWeedCatList() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonModel<List<CategoryDetails>>> call = apiInterface.getCategoryList(2);
        showProgressDialog();
        call.enqueue(new Callback<CommonModel<List<CategoryDetails>>>() {
            @Override
            public void onResponse(Call<CommonModel<List<CategoryDetails>>> call, Response<CommonModel<List<CategoryDetails>>> response) {

                hideProgressDialog();

                if (response.body().getStatus() == Constants.SUCCESS) {
                    arrWeedCatList.addAll(response.body().getData());

                    if (binding.refreshSwipe.isRefreshing())
                        binding.refreshSwipe.setRefreshing(false);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setanimationView(binding.rvWeedList);
                            binding.rvWeedList.setVisibility(View.VISIBLE);
                        }
                    }, 500);
                    weedCategoryAdapter.notifyDataSetChanged();
                } else {

                    binding.refreshSwipe.setVisibility(View.GONE);
                    binding.llNoItem.setVisibility(View.VISIBLE);
                    CommonUtils.makeToast(response.body().getMsg(), mainActivity);
                }
            }

            @Override
            public void onFailure(Call<CommonModel<List<CategoryDetails>>> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                binding.refreshSwipe.setRefreshing(false);
                getWeedCatList();
            }
        }, 2600);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.clearImg:
                hideKeyboard();
                binding.searchEdt.setText("");
                binding.clearImg.setVisibility(View.GONE);
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemClick(int position) {
        mainActivity.slidingRootNav.setMenuLocked(true);

        Bundle bundle = new Bundle();
        bundle.putInt("item_id", arrWeedCatList.get(position).getId());
        mainActivity.pushFragment(new WeedProductFragment(), true, false, true,
                WeedProductFragment.class.getSimpleName(), bundle, false);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //int resultFound = weedCategoryAdapter.filter(s.toString());

        if (binding.searchEdt.getText().length() > 0) binding.clearImg.setVisibility(View.VISIBLE);
        else binding.clearImg.setVisibility(View.GONE);

        /*if (resultFound == 0) {
            Toast.makeText(getActivity(), "no item found", Toast.LENGTH_SHORT).show();
            binding.rvWeedList.setVisibility(View.GONE);
        }else {
            binding.rvWeedList.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    /**
     * Method is used to initialized hide keyboard...
     */
    private void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }


}
