package dev.env.potriders.activity.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.activity.dialog.ThankYouDialog;
import dev.env.potriders.activity.viewActivity.AdvancedSearchActivity;
import dev.env.potriders.databinding.FragmentCheckoutBinding;
import dev.env.potriders.model.WeightModel;


public class CheckoutFragment extends BaseFragment implements OnClickListener, AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {

    FragmentCheckoutBinding binding;
    private List<WeightModel> checkoutModels = new ArrayList<>();
    private String mGetPayCard, mGetSelectPayment = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkout, container, false);





        initControls();
        setSpinnerCheckoutList();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_checkout), false, true, false,
                false, false, false, false);

        binding.editIv.setOnClickListener(this);
        binding.placeOrderTV.setOnClickListener(this);

        binding.emailTransferRbtn.setOnCheckedChangeListener(this);
        binding.cashRbtn.setOnCheckedChangeListener(this);

        binding.spnPayment.setOnItemSelectedListener(this);
    }

    /**
     *  Method is used to set the checkout list...
     */
    private void setSpinnerCheckoutList() {

        checkoutModels.add(new WeightModel("Select Payment"));
        checkoutModels.add(new WeightModel("Credit Cards"));
        checkoutModels.add(new WeightModel("Email Transfer"));
        checkoutModels.add(new WeightModel("Cash"));


        CheckoutListAdapter checkoutListAdapter = new CheckoutListAdapter(getActivity(), R.id.txt_title, checkoutModels, getResources());
        binding.spnPayment.setAdapter(checkoutListAdapter);
    }


    /**
     *  Method is used to check validation ...
     */
    private boolean isValid() {

        if (mGetSelectPayment.equals("")) {
            Toast.makeText(getActivity(), "Select Payment Method", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.spnPayment:
                WeightModel model = (WeightModel) parent.getItemAtPosition(position);
                if (binding.spnPayment.getSelectedItemId() != 0) {
                    mGetPayCard = model.getName();

                    switch (mGetPayCard) {
                        case "Credit Cards":
                            binding.payCardLl.setVisibility(View.VISIBLE);
                            binding.emailCardLl.setVisibility(View.GONE);

                            break;
                        case "Email Transfer":
                            binding.payCardLl.setVisibility(View.GONE);
                            binding.emailCardLl.setVisibility(View.VISIBLE);

                            break;
                        case "Cash":
                            binding.payCardLl.setVisibility(View.GONE);
                            binding.emailCardLl.setVisibility(View.GONE);
                            break;
                    }
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;

            case R.id.editIv:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new AddressFragment(), true, false, true,
                        AddressFragment.class.getSimpleName(), null, false);
                break;

            case R.id.placeOrderTV:
                /*mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new PayNowFragment(), true, false, true,
                        PayNowFragment.class.getSimpleName(), null, false);*/

                if (isValid()) {
                    mainActivity.slidingRootNav.setMenuLocked(true);
                    ThankYouDialog thankYouFragment = new ThankYouDialog();
                    thankYouFragment.show(getFragmentManager(), "Thank you");
                }
                break;
        }
    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {

            case R.id.emailTransferRbtn:
                if (isChecked) {
                    binding.emailTransferRbtn.setChecked(true);
                    binding.emailCardLl.setVisibility(View.VISIBLE);
                    mGetSelectPayment = "Email Transfer";
                }
                break;

            case R.id.cashRbtn:
                if (isChecked) {
                    binding.cashRbtn.setChecked(true);
                    binding.emailCardLl.setVisibility(View.GONE);
                    mGetSelectPayment = "Cash";
                }
                break;
        }
    }



    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity,
                R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle(getString(R.string.str_checkout), false, true, false,
                    false, false, false, false);
        }
    }


    /**
     *  checkout list adapter...
     */
    public class CheckoutListAdapter extends ArrayAdapter<WeightModel> {
        private Context context;
        private List<WeightModel> data;
        public Resources res;
        WeightModel userData = null;
        LayoutInflater inflater;

        public CheckoutListAdapter(Context context, int textViewResourceId, List<WeightModel> data, Resources res) {
            super(context, textViewResourceId, data);

            this.context = context;
            this.data = data;
            this.res = res;
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            View view = inflater.inflate(R.layout.row_checkout_list, parent, false);

            userData = null;
            userData = (WeightModel) data.get(position);

            ((TextView) view.findViewById(R.id.txt_method)).setText(userData.getName());
            return view;
        }
    }
}
