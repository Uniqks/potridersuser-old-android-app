package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentContactUsBinding;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.Utils;
import dev.env.potriders.app.Validation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsFragment extends BaseFragment implements OnClickListener {


    FragmentContactUsBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);







        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_contact_us), false, true, false,
                false, false, false, false);

        setanimationView(binding.topLL);
        binding.sendBT.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;

            case R.id.sendBT:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        if (!isValidFName()) {
            return;
        }
        if (!isValidEmail()) {
            return;
        }
        if (!isValidMsg()) {
            return;
        }
        if (Utils.isNetworkAvailable(mainActivity)) {
            submitContact();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }

    private void submitContact() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);

        String name = binding.nameET.getText().toString().trim();
        String email = binding.emailET.getText().toString().trim();
        String msg = binding.messageET.getText().toString().trim();

        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("email", email);
        map.put("message", msg);

        Call<String> call = apiInterface.submitContactUsData(map);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {

                        CommonUtils.makeToast(msg, mainActivity);

                        binding.emailET.setText("");
                        binding.nameET.setText("");
                        binding.messageET.setText("");
                    } else {
                        CommonUtils.makeToast(msg, mainActivity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }

    private boolean isValidFName() {
        return Validation.hasText(binding.nameET, getString(R.string.err_empty_name));
    }

    private boolean isValidMsg() {
        return Validation.hasText(binding.messageET, getString(R.string.err_empty_message));
    }

    private boolean isValidEmail() {
        if (!Validation.hasText(binding.emailET, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(binding.emailET, true, getString(R.string.error_email_invalid));
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


}
