package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentPrivacyBinding;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.LogUtils;
import dev.env.potriders.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyFragment extends BaseFragment {

    FragmentPrivacyBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_privacy, container, false);







        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_privacy), false, true, false,
                false, false, false, false);
        if (Utils.isNetworkAvailable(mainActivity)) {
            getPrivacyPolicyContentService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }

    private void getPrivacyPolicyContentService() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);
        Call<String> call = apiInterface.getPages(3);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();
                LogUtils.d("=response==>>" + response.body());
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String content = jsonObject1.getString("content");
                        binding.privacyTV.setText(Html.fromHtml(content));
                    } else {
                        CommonUtils.makeToast(msg, mainActivity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        setanimationView(binding.topLL);
    }
}
