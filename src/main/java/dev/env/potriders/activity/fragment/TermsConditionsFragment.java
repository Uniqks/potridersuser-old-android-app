package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentTermsConditionsBinding;


public class TermsConditionsFragment extends BaseFragment implements OnClickListener {
    FragmentTermsConditionsBinding binding;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_conditions, container, false);

        initUI(binding.getRoot());
        return binding.getRoot();
    }

    private void initUI(View view) {
        setToolbarIconTitle(getString(R.string.str_terms_conditions), false, true, false,
                false, false, false, false);
        binding.aboutUsTV.setText(getString(R.string.str_lorem_ipsum));
        setanimationView(binding.topLL);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


}
