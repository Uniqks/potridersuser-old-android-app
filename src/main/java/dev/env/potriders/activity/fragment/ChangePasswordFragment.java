package dev.env.potriders.activity.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dev.env.potriders.R;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;
import dev.env.potriders.utils.Utils;
import dev.env.potriders.app.Validation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePasswordFragment extends BaseFragment implements OnClickListener {


    private View mView;
    private EditText currentPasswordET;
    private EditText newPasswordET;
    private EditText confirmPasswordET;
    private TextView doneTV;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_change_password, container, false);







        initControls();
        return mView;
    }


    /**
     * Method is used to initialized all the controls....
     */
    private void initControls() {

        setToolbarIconTitle("Change Password", false, true, false,
                false, false, false, false);
        currentPasswordET = mView.findViewById(R.id.currentPasswordET);
        newPasswordET = mView.findViewById(R.id.newPasswordET);
        confirmPasswordET = mView.findViewById(R.id.confirmPasswordET);
        doneTV = mView.findViewById(R.id.doneTV);
        doneTV.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.doneTV:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        if (!isValidatePassword()) {
            return;
        }
        if (!isValidNewPassword()) {
            return;
        }
        if (!isValidConfirmPassword()) {
            return;
        }
        if (Utils.isNetworkAvailable(mainActivity)) {
            changePasswordService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }

    }


    /**
     *  Method is used to initialized change password webservice...
     */
    private void changePasswordService() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);

        String strNewPwd = newPasswordET.getText().toString().trim();
        String strCurrentPwd = currentPasswordET.getText().toString().trim();
        String user_id = String.valueOf(PreferenceUtil.getPref(mainActivity).getInt(PreferenceKeys.USER_ID, 0));
        Map<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("new_password", strNewPwd);
        map.put("current_password", strCurrentPwd);

        Call<String> call = apiInterface.changePassword(map);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        CommonUtils.makeToast("Password changed successfully", mainActivity);
                        confirmPasswordET.setText("");
                        newPasswordET.setText("");
                        currentPasswordET.setText("");
                        mainActivity.hideKeyboard();
                    } else {
                        CommonUtils.makeToast(msg, mainActivity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }

    private boolean isValidConfirmPassword() {
        if (!Validation.hasText(confirmPasswordET, getString(R.string.err_confirm_pwd))) {
            return false;
        }
        if (!Validation.isValidPassword(confirmPasswordET, getString(R.string.error_pass_invalid))) {
            return false;
        }
        return Validation.isConfirmPassword(newPasswordET, confirmPasswordET, getString(R.string.password_not_match));
    }

    private boolean isValidNewPassword() {
        if (!Validation.hasText(newPasswordET, getString(R.string.err_new_pwd))) {
            return false;
        }
        return Validation.isValidPassword(newPasswordET, getString(R.string.error_pass_invalid));
    }

    private boolean isValidatePassword() {
        if (!Validation.hasText(currentPasswordET, getString(R.string.err_current_pwd))) {
            return false;
        }
        return Validation.isValidPassword(currentPasswordET, getString(R.string.error_pass_invalid));
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.hideKeyboard();
    }
}
