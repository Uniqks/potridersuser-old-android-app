package dev.env.potriders.activity.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import dev.env.potriders.R;
import dev.env.potriders.databinding.FragmentAddressBinding;


public class AddressFragment extends BaseFragment implements OnClickListener {

    FragmentAddressBinding binding;

    String strHouseDetails, strLandMark, strCity, strState, strPinCode;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_address, container, false);




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.str_address), false, true, false,
                false, false, false, false);

        binding.saveTV.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                mainActivity.onBackPressed();
                break;

            case R.id.saveTV:
                strHouseDetails = binding.houseDetailET.getText().toString().trim();
                strLandMark = binding.landmarkET.getText().toString().trim();
                strCity = binding.cityET.getText().toString().trim();
                strState = binding.stateET.getText().toString().trim();
                strPinCode = binding.pincodeET.getText().toString().trim();

                if (strHouseDetails != null && !strHouseDetails.equals("")) {
                    if (strLandMark != null && !strLandMark.equals("")) {
                        if (strCity != null && !strCity.equals("")) {
                            if (strState != null && !strState.equals("")) {
                                if (strPinCode != null && !strPinCode.equals("")) {

                                } else {
                                    Toast.makeText(mainActivity, getString(R.string.err_empty_pincode), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(mainActivity, getString(R.string.err_empty_state), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mainActivity, getString(R.string.err_empty_city), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mainActivity, getString(R.string.err_empty_landmark), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mainActivity.slidingRootNav.setMenuLocked(true);
                    mainActivity.pushFragment(new DeliveryTimeFragment(),true,false,true,
                            DeliveryTimeFragment.class.getSimpleName(),null,false);
                }
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle(getString(R.string.str_address), false, true, false,
                    false, false, false, false);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //setanimationView(binding.topLL);
        /*setanimationView(binding.houseDetailTV);
        setanimationView(binding.landmarkTV);
        setanimationView(binding.cityTV);
        setanimationView(binding.stateTV);
        setanimationView(binding.pincodeTV);*/
    }

}
