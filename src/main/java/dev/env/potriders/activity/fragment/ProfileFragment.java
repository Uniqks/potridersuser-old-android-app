package dev.env.potriders.activity.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import dev.env.potriders.R;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.LogUtils;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;
import dev.env.potriders.utils.Utils;
import dev.env.potriders.app.Validation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment implements View.OnClickListener/*, MainActivity.CartEditProfileClick */ {

    ImageView editProfileIV;

    private int STORAGE_PERMISSION_CODE = 11;
    public static final int SELECT_PHOTO = 1;
    public static final int TAKENPHOTO = 0;
    Uri uri;
    File photofile;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public String photo_path = "";

    public static String strFirstName, strLastName, strEmail, strPhoneNumber;
    private TextView buttonChangePassword;
    private EditText firstNameET;
    private EditText lastNameET;
    private EditText emailET;
    private EditText phoneNoET;
    private ImageView cameraIV;
    private ImageView userIV;
    private LinearLayout topLL;
    private View mView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_profile, container, false);





        initControls();
        return mView;
    }


    /**
     *  Method is used to initialized all the controls....
     */
    private void initControls() {
        buttonChangePassword = mView.findViewById(R.id.buttonChangePassword);
        firstNameET = mView.findViewById(R.id.firstNameET);
        lastNameET = mView.findViewById(R.id.lastNameET);
        emailET = mView.findViewById(R.id.emailET);
        phoneNoET = mView.findViewById(R.id.phoneNoET);
        userIV = mView.findViewById(R.id.userIV);
        topLL = mView.findViewById(R.id.topLL);
        cameraIV = mView.findViewById(R.id.cameraIV);
        cameraIV.setOnClickListener(this);

        buttonChangePassword.setOnClickListener(this);

        mainActivity.mEditImg.setOnClickListener(this);
        mainActivity.mSaveImg.setOnClickListener(this);

        setToolbarIconTitle("Profile", true, false, false,
                true, false, false, false);
        enableDisableFields(false);

        String fname = PreferenceUtil.getPref(mainActivity).getString(PreferenceKeys.FIRST_NAME, "");
        String lname = PreferenceUtil.getPref(mainActivity).getString(PreferenceKeys.LAST_NAME, "");
        String email = PreferenceUtil.getPref(mainActivity).getString(PreferenceKeys.EMAIL, "");
        String phone = PreferenceUtil.getPref(mainActivity).getString(PreferenceKeys.PHONE, "");
        String pro_pic = PreferenceUtil.getPref(mainActivity).getString(PreferenceKeys.PROPIC, "");
        String url = PreferenceUtil.getPref(mainActivity).getString(PreferenceKeys.IMAGE_URL, "");


        if (!TextUtils.isEmpty(fname))
            firstNameET.setText(fname);
        if (!TextUtils.isEmpty(lname))
            lastNameET.setText(lname);
        if (!TextUtils.isEmpty(email))
            emailET.setText(email);
        if (!TextUtils.isEmpty(phone))
            phoneNoET.setText(phone);

        if (!TextUtils.isEmpty(pro_pic) && !TextUtils.isEmpty(url)) {
            Glide.with(mainActivity)
                    .load(url + "" + pro_pic)
                    .asBitmap()
                    .placeholder(R.drawable.default_profile)
                    .error(R.drawable.default_profile)
                    .into(userIV);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buttonChangePassword:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new ChangePasswordFragment(), true, false,
                        true, ChangePasswordFragment.class.getSimpleName(), null, false);
                break;

            case R.id.cameraIV:
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, STORAGE_PERMISSION_CODE);
                } else {
                    show_popup();
                }
                break;

            case R.id.img_edit:
                enableDisableFields(true);
                cameraIV.setVisibility(View.VISIBLE);
                setToolbarIconTitle("Profile", true, false, false,
                        false, false, true, false);
                break;

            case R.id.img_save:
                checkValidation();
                break;
        }
    }


    private void checkValidation() {
        if (!isValidFName()) {
            return;
        }
        if (!isValidLName()) {
            return;
        }
        if (!isValidEmail()) {
            return;
        }

        if (!isValidPhone()) {
            return;
        }

        if (Utils.isNetworkAvailable(mainActivity)) {
            saveProfile();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), mainActivity);
        }
    }

    private void saveProfile() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);

        String name = firstNameET.getText().toString().trim();
        String lname = lastNameET.getText().toString().trim();
        String email = emailET.getText().toString().trim();
        String phone = phoneNoET.getText().toString().trim();

        String user_id = String.valueOf(PreferenceUtil.getPref(mainActivity).getInt(PreferenceKeys.USER_ID, 0));
        Map<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("fname", name);
        map.put("lname", lname);
        map.put("email", email);
        map.put("phone", phone);
        File file = new File(photo_path);
        Call<String> call;
        if (photo_path.equalsIgnoreCase("")) {

            call = apiInterface.updateProfile(map);
        } else {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
            call = apiInterface.updateProfileWithImage(user_id, name, lname, email, phone, body);

        }


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        int id = jsonObject1.getInt("id");
                        String fname = jsonObject1.getString("fname");
                        String lname = jsonObject1.getString("lname");
                        String email = jsonObject1.getString("email");
                        String phone = jsonObject1.getString("phone");
                        String image = jsonObject1.getString("image");


                        CommonUtils.makeToast("Profile saved successfully", mainActivity);
                        PreferenceUtil.getPref(mainActivity).edit().putInt(PreferenceKeys.USER_ID, id)
                                .putString(PreferenceKeys.FIRST_NAME, fname)
                                .putString(PreferenceKeys.LAST_NAME, lname)
                                .putString(PreferenceKeys.EMAIL, email)
                                .putString(PreferenceKeys.PHONE, phone)
                                .putString(PreferenceKeys.PROPIC, image).apply();
                        mainActivity.setDrawerData();

                        setToolbarIconTitle("PROFILE", true, false, false,
                                true, false, false, false);
                        enableDisableFields(false);
                        cameraIV.setVisibility(View.GONE);
                    } else {
                        CommonUtils.makeToast(msg, mainActivity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), mainActivity);
                hideProgressDialog();
            }
        });
    }

    private boolean isValidPhone() {
        return Validation.hasText(phoneNoET, getString(R.string.err_empty_phone_number));
    }

    private boolean isValidLName() {
        return Validation.hasText(lastNameET, getString(R.string.err_empty_lastname));
    }

    private boolean isValidFName() {
        return Validation.hasText(firstNameET, getString(R.string.err_empty_firstname));
    }

    private boolean isValidEmail() {
        if (!Validation.hasText(emailET, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(emailET, true, getString(R.string.error_email_invalid));
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void takePhotoFromCamera() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            photofile = Utils.getInstance(getActivity()).create_file(".jpg");
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            uri = FileProvider.getUriForFile(getActivity(), "dev.env.potriders.fileprovider", photofile);
        } else {
            photofile = Utils.getInstance(getActivity()).create_Image_File(".jpg");
            uri = Uri.fromFile(photofile);

        }
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(Intent.createChooser(cameraIntent, getString(R.string.lbl_capture)), TAKENPHOTO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.i("ProfileFragment" + " onActivityResult requestCode " + requestCode);
        if ((requestCode == TAKENPHOTO) && (resultCode == Activity.RESULT_OK)) {

            if (uri != null) {

                photo_path = photofile.getAbsolutePath();//uri.getPath();

                LogUtils.e(uri.getPath() + " imageFilePath " + new File(uri.toString()).getAbsolutePath());
                if (photo_path != null && photo_path.length() > 0) {
                    Glide.with(this).load(new File(photo_path)).asBitmap().centerCrop().into(new BitmapImageViewTarget(userIV) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            userIV.setImageDrawable(circularBitmapDrawable);
                        }
                    });
//                    arrImagePaths.add(photo_path);
                }
            } else {

                Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.toast_unable_to_selct_image), false);
            }

        } else if ((requestCode == SELECT_PHOTO) && (resultCode == Activity.RESULT_OK)) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            photo_path = cursor.getString(columnIndex);
            try {
                LogUtils.i("", "SELECT_PHOTO " + photo_path);
                Glide.with(this).load(new File(photo_path)).asBitmap().centerCrop().into(new BitmapImageViewTarget(userIV) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        userIV.setImageDrawable(circularBitmapDrawable);
                    }
                });
//                arrImagePaths.add(photo_path);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (photo_path != null && !photo_path.equals(""))
            LogUtils.i("ProfileFragment" + " onActivityResult photo_path " + photo_path);


    }

    public void show_popup() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.str_select_one));
        builder.setMessage(getString(R.string.str_select_gallery_msg));
        builder.setNegativeButton(getString(R.string.str_camera).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                takePhotoFromCamera();
            }
        });
        builder.setPositiveButton(getString(R.string.str_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), SELECT_PHOTO);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setanimationView(topLL);
        mainActivity.hideKeyboard();
    }

    public void enableDisableFields(boolean is_enabled) {
        firstNameET.setEnabled(is_enabled);
        lastNameET.setEnabled(is_enabled);
        emailET.setEnabled(is_enabled);
        phoneNoET.setEnabled(is_enabled);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("Profile", true, false, false,
                    true, false, false, false);
        }
    }

}
