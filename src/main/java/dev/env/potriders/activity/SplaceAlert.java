package dev.env.potriders.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.MainActivity;
import dev.env.potriders.app.PotRidersSessionManager;

import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.databinding.ActivitySplaceAlertBinding;
import dev.env.potriders.utils.PreferenceUtil;
import spencerstudios.com.bungeelib.Bungee;

public class SplaceAlert extends AppCompatActivity implements View.OnClickListener {


    ActivitySplaceAlertBinding binding;
    PotRidersSessionManager mAppManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splace_alert);

        mAppManager = new PotRidersSessionManager(SplaceAlert.this);



        initControls();
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.noBTN.setOnClickListener(this);
        binding.yesBTN.setOnClickListener(this);

        setanimationView(binding.logoIV);
        setanimationView(binding.alertLL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.noBTN:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.noBTN);

                finish();
                //Bungee.slideDown(SplaceAlert.this);
                break;

            case R.id.yesBTN:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.yesBTN);

                if (PreferenceUtil.getPref(SplaceAlert.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {
                    startActivity(new Intent(SplaceAlert.this, MainActivity.class));
                    finish();
                    Bungee.slideLeft(SplaceAlert.this);
                    return;
                }
                PreferenceUtil.getPref(SplaceAlert.this).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                startActivity(new Intent(SplaceAlert.this, Login.class));
                finish();
                Bungee.slideLeft(SplaceAlert.this);
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(SplaceAlert.this, R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }
}
