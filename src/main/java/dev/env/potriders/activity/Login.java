package dev.env.potriders.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.BaseActivity;
import dev.env.potriders.activity.viewActivity.MainActivity;
import dev.env.potriders.app.Potrider;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;
import dev.env.potriders.utils.Utils;
import dev.env.potriders.app.Validation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import spencerstudios.com.bungeelib.Bungee;


public class Login extends BaseActivity implements OnClickListener {

    private EditText mEmailEdt, mPwdEdt;
    private LinearLayout mMainTopLl;
    private Button btnSkip;

    private Potrider mPotrider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPotrider = (Potrider) getApplication();




        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mMainTopLl = findViewById(R.id.ll_main_top);
        mMainTopLl.setVisibility(View.GONE);

        findViewById(R.id.btn_register).setOnClickListener(this);
        findViewById(R.id.txt_fpwd).setOnClickListener(this);
        findViewById(R.id.btn_login).setOnClickListener(this);

        btnSkip = findViewById(R.id.btn_skip);
        btnSkip.setOnClickListener(this);

        mPwdEdt = findViewById(R.id.edt_pwd);
        mEmailEdt = findViewById(R.id.edt_email);

        setAnimationView(mMainTopLl);
        if (PreferenceUtil.getPref(Login.this).getBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false)) {
            btnSkip.setVisibility(View.GONE);
        } else {
            btnSkip.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.btn_login));

                mPotrider.hideKeyboard(Login.this);
                checkValidation();
                break;

            case R.id.txt_fpwd:
                mPotrider.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, ForgotPassword.class));
                break;

            case R.id.btn_register:
                mPotrider.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, Register.class));
                break;

            case R.id.btn_skip:
                mPotrider.hideKeyboard(Login.this);
                startActivity(new Intent(Login.this, MainActivity.class));
                finish();
                break;
        }
    }


    /**
     *  Method is used to initialized check validation...
     */
    private void checkValidation() {
        if (!isValidEmail()) {
            return;
        }
        if (!isValidatePassword()) {
            return;
        }
        if (Utils.isNetworkAvailable(Login.this)) {
            doLoginService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), Login.this);
        }
    }


    /**
     *  Method is used to initialized login user...
     */
    private void doLoginService() {

        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);

        Map<String, String> map = new HashMap<>();
        map.put("email", mEmailEdt.getText().toString().trim());
        map.put("password", mPwdEdt.getText().toString().trim());
        map.put("userType", Constants.USER_TYPE);
        map.put("deviceToken", "1132155d5ffjfbj");
        map.put("deviceType", Constants.DEVICE_TYPE);

        Call<String> call = apiInterface.userLogin(map);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        int id = jsonObject1.getInt("id");
                        String fname = jsonObject1.getString("fname");
                        String lname = jsonObject1.getString("lname");
                        String email = jsonObject1.getString("email");
                        String phone = jsonObject1.getString("phone");
                        String userType = jsonObject1.getString("userType");
                        String image = jsonObject1.getString("image");

                        CommonUtils.makeToast("Login successfully", Login.this);

                        PreferenceUtil.getPref(Login.this).edit().putInt(PreferenceKeys.USER_ID, id)
                                .putString(PreferenceKeys.FIRST_NAME, fname)
                                .putString(PreferenceKeys.LAST_NAME, lname)
                                .putString(PreferenceKeys.EMAIL, email)
                                .putString(PreferenceKeys.PHONE, phone)
                                .putString(PreferenceKeys.PROPIC, image)
                                .putBoolean(PreferenceKeys.IS_LOGIN, true).apply();

                        if (PreferenceUtil.getPref(Login.this).getBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false)) {

                            System.out.println("data login1 --->" + PreferenceUtil.getPref(Login.this).getBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false));

                            Intent intent2 = new Intent(Login.this, MainActivity.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent2);
                            finish();
                        } else {

                            System.out.println("data login2 --->" + PreferenceUtil.getPref(Login.this).getBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false));
                            PreferenceUtil.getPref(Login.this).edit().putBoolean(PreferenceKeys.IS_STARTED, true).apply();
                            startActivity(new Intent(Login.this, MainActivity.class));
                            finish();
                            Bungee.slideLeft(Login.this);
                        }
                    } else {
                        CommonUtils.makeToast(msg, Login.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), Login.this);
                hideProgressDialog();
            }
        });
    }

    private boolean isValidEmail() {
        if (!Validation.hasText(mEmailEdt, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(mEmailEdt, true, getString(R.string.error_email_invalid));
    }

    private boolean isValidatePassword() {
        if (!Validation.hasText(mPwdEdt, getString(R.string.err_empty_password))) {
            return false;
        }
        return Validation.isValidPassword(mPwdEdt, getString(R.string.error_pass_invalid));
    }


    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAnimationView(mMainTopLl);
    }
}
