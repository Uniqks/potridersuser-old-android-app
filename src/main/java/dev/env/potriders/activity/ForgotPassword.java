package dev.env.potriders.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.json.JSONException;
import org.json.JSONObject;

import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.BaseActivity;
import dev.env.potriders.app.Potrider;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.Utils;
import dev.env.potriders.app.Validation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import spencerstudios.com.bungeelib.Bungee;


public class ForgotPassword extends BaseActivity implements OnClickListener {


    private LinearLayout mMainLl;
    private EditText mEmailEdt;
    private Potrider mPotrider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mPotrider = (Potrider) getApplication();



        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        findViewById(R.id.btn_submit).setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);

        mEmailEdt = findViewById(R.id.edt_email);
        mMainLl = findViewById(R.id.ll_main);
        mMainLl.setVisibility(View.GONE);
        setAnimationView(mMainLl);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAnimationView(mMainLl);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_submit:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.btn_submit));

                mPotrider.hideKeyboard(ForgotPassword.this);
                checkValidation();
                break;

            case R.id.img_back:
                finish();
                break;
        }
    }


    /**
     *  Method is used to initialized check validation...
     */
    private void checkValidation() {
        if (!isValidEmail()) {
            return;
        }
        if (Utils.isNetworkAvailable(ForgotPassword.this)) {
            fpwdService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), ForgotPassword.this);
        }
    }


    /**
     *  Method is used to initialized forgot pwd
     */
    private void fpwdService() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);
        Call<String> call = apiInterface.forgotPassword(mEmailEdt.getText().toString().trim());

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    CommonUtils.makeToast(msg, ForgotPassword.this);
                    if (status == Constants.SUCCESS) {
                        finish();
                        Bungee.slideRight(ForgotPassword.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), ForgotPassword.this);
                hideProgressDialog();
            }
        });


    }


    private boolean isValidEmail() {
        if (!Validation.hasText(mEmailEdt, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(mEmailEdt, true, getString(R.string.error_email_invalid));
    }


    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        mMainLl.startAnimation(bottomUp);
        mMainLl.setVisibility(View.VISIBLE);
    }

}
