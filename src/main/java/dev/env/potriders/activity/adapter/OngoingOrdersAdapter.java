package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.model.CategoryDetails;

public class OngoingOrdersAdapter extends RecyclerView.Adapter<OngoingOrdersAdapter.MyViewHolder> {

    private ArrayList<CategoryDetails> horizontalList;
    Context context;

    WeedItemCick weedItemCick;

    public interface WeedItemCick {
        void onItemClick(int position);
        void onTrackDeliveryClick(int position);
        void onViewAddOns(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick){
        this.weedItemCick = weedItemCick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout topLL, addonsLL;
        CardView card_view;
        Button trackDeliveryBT, addOnsBT;

        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            topLL = itemView.findViewById(R.id.topLL);
            card_view = itemView.findViewById(R.id.card_view);
            trackDeliveryBT = itemView.findViewById(R.id.trackDeliveryBT);
            addOnsBT = itemView.findViewById(R.id.addOnsBT);
            addonsLL = itemView.findViewById(R.id.addonsLL);
        }
    }


    public OngoingOrdersAdapter(ArrayList<CategoryDetails> horizontalList, Context mContext) {
        this.horizontalList = horizontalList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onItemClick(position);
            }
        });

        holder.addOnsBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onViewAddOns(position);
                if (holder.addonsLL.getVisibility() == View.GONE)
                    holder.addonsLL.setVisibility(View.VISIBLE);
                else if (holder.addonsLL.getVisibility() == View.VISIBLE)
                    holder.addonsLL.setVisibility(View.GONE);

            }
        });

        holder.trackDeliveryBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onTrackDeliveryClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
