package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.model.CategoryDetails;
import dev.env.potriders.utils.CommonUtils;

public class FoodCategoryAdapter extends RecyclerView.Adapter<FoodCategoryAdapter.MyViewHolder> {

    private List<CategoryDetails> mData;
    Context context;
    FoodItemCick foodItemCick;


    public interface FoodItemCick {
        void onItemClick(int position);
    }

    public void setFoodItemCick(FoodItemCick foodItemCick) {
        this.foodItemCick = foodItemCick;
    }


    public FoodCategoryAdapter(List<CategoryDetails> data, Context mContext) {
        this.mData = data;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_food_catlist, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        CategoryDetails weedCatModel = mData.get(position);

        if (weedCatModel.getName() != null && !weedCatModel.getName().equals("")) {
            holder.weedNameTV.setText(weedCatModel.getName());
        } else {
        }

        if (weedCatModel.getImage() != null && !weedCatModel.getImage().equals("")) {

            if (!TextUtils.isEmpty(CommonUtils.getImageUrl(context)) && !TextUtils.isEmpty(weedCatModel.getImage())) {
                /*Glide.with(context)
                        .load(CommonUtils.getImageUrl(context) + "" + weedModel.getImage())
                        .asBitmap()
                        .placeholder(R.drawable.weed_default)
                        .error(R.drawable.weed_default)
                        .into(holder.imageIV);*/

                Picasso.get()
                        .load(CommonUtils.getImageUrl(context) + "" + weedCatModel.getImage())
                        .placeholder(R.drawable.weed_default)
                        .error(R.drawable.weed_default)
                        .into(holder.imageIV);
            }
        } else {
        }

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foodItemCick.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView card_view;
        TextView weedNameTV;
        ImageView imageIV;


        public MyViewHolder(View view) {
            super(view);

            card_view = view.findViewById(R.id.card_view);
            weedNameTV = view.findViewById(R.id.nameTV);
            imageIV = view.findViewById(R.id.imageIV);
        }
    }

}
