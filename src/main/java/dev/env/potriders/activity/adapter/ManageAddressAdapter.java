package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import dev.env.potriders.R;
import dev.env.potriders.model.Address;

public class ManageAddressAdapter extends RecyclerView.Adapter<ManageAddressAdapter.MyViewHolder> {
    private List<Address> addressList;
    Context context;

    WeedItemCick weedItemCick;

    public interface WeedItemCick {
        void onItemClick(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick) {
        this.weedItemCick = weedItemCick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout topLL;
        TextView textViewEdit, textViewDelete, textViewAddress, textViewPlaceName;


        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            topLL = itemView.findViewById(R.id.topLL);
            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            textViewDelete = itemView.findViewById(R.id.textViewDelete);
            textViewEdit = itemView.findViewById(R.id.textViewEdit);
            textViewPlaceName = itemView.findViewById(R.id.textViewPlaceName);
        }
    }


    public ManageAddressAdapter(List<Address> addressList, Context mContext) {
        this.addressList = addressList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_manage_address_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Address address = addressList.get(position);
        String adrs = address.getAddress1() + ", " + address.getAddress2() + ", " + address.getCity()
                + ", " + address.getState() + ", " + address.getCountry() + ", " + address.getPincode();
        holder.textViewAddress.setText(address.getAddress1());

        holder.textViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Work in progress...", Toast.LENGTH_SHORT).show();
            }
        });

        holder.textViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Work in progress...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }
}
