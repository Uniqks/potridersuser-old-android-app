package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.model.CategoryDetails;

public class PastOrdersAdapter extends RecyclerView.Adapter<PastOrdersAdapter.MyViewHolder> {

    private ArrayList<CategoryDetails> horizontalList;
    Context context;

    WeedItemCick weedItemCick;

    public interface WeedItemCick {
        void onItemClick(int position);
        void onTrackDeliveryClick(int position);
        void onViewAddOns(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick){
        this.weedItemCick = weedItemCick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout topLL, addonsLL;
        CardView card_view;
        Button rateBT;
        ImageView ivAddOns;

        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            topLL = itemView.findViewById(R.id.topLL);
            card_view = itemView.findViewById(R.id.card_view);
            rateBT = itemView.findViewById(R.id.rateBT);
            ivAddOns = itemView.findViewById(R.id.ivAddOns);
            addonsLL = itemView.findViewById(R.id.addonsLL);
        }
    }


    public PastOrdersAdapter(ArrayList<CategoryDetails> horizontalList, Context mContext) {
        this.horizontalList = horizontalList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.past_order_item_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onItemClick(position);
            }
        });

        holder.rateBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onViewAddOns(position);
            }
        });

        holder.ivAddOns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.addonsLL.getVisibility() == View.GONE) {
                    holder.ivAddOns.animate().rotation(180).start();
                    holder.ivAddOns.setImageResource(R.drawable.dskyup);
                    holder.addonsLL.setVisibility(View.VISIBLE);

                } else if (holder.addonsLL.getVisibility() == View.VISIBLE){
                    holder.ivAddOns.animate().rotation(180).start();
                    holder.ivAddOns.setImageResource(R.drawable.dskydown);
                    holder.addonsLL.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
