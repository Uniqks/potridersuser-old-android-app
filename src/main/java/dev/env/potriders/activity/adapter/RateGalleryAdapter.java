package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.util.ArrayList;

import dev.env.potriders.R;

public class RateGalleryAdapter extends RecyclerView.Adapter<RateGalleryAdapter.MyViewHolder> {

    private ArrayList<String> horizontalList;
    Context context;

    WeedItemCick weedItemCick;

    public interface WeedItemCick {
        void onItemClick(int position);
        void onClearClick(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick) {
        this.weedItemCick = weedItemCick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView galleryIV;
        ImageView clearIV;

        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            galleryIV = itemView.findViewById(R.id.galleryIV);
            clearIV = itemView.findViewById(R.id.clearIV);
        }
    }


    public RateGalleryAdapter(ArrayList<String> horizontalList, Context mContext) {
        this.horizontalList = horizontalList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_rate_gallery, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (horizontalList.get(position) != null && !horizontalList.get(position).equals("")) {
//            Picasso.with(context).load(new File(horizontalList.get(position))).placeholder(R.drawable.upload_images).into(holder.galleryIV);
            Glide.with(context)
                    .load(new File(horizontalList.get(position)))
                    .placeholder(R.drawable.upload_images)
                    .into(holder.galleryIV);
        }
        holder.clearIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onClearClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
