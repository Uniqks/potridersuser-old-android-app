package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import dev.env.potriders.R;
import dev.env.potriders.model.ProductDetails;
import dev.env.potriders.utils.CommonUtils;

public class WeedProductAdapter extends RecyclerView.Adapter<WeedProductAdapter.MyViewHolder> {

    private List<ProductDetails> mData;
    ArrayList<ProductDetails> itemData;
    Context context;
    WeedItemCick weedItemCick;
    CartItemCick cartItemCick;


    public interface WeedItemCick {
        void onItemClick(int position);
    }

    public interface CartItemCick {
        void onCartClick(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick) {
        this.weedItemCick = weedItemCick;
    }

    public void setCartItemCick(CartItemCick cartItemCick) {
        this.cartItemCick = cartItemCick;
    }

    public WeedProductAdapter(List<ProductDetails> data, Context mContext) {
        this.mData = data;
        this.context = mContext;
        this.itemData = new ArrayList<ProductDetails>();
        this.itemData.addAll(data);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_weed_item, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        ProductDetails weedModel = mData.get(position);

        if (weedModel.getName() != null && !weedModel.getName().equals("")) {
            holder.weedNameTV.setText(weedModel.getName());
        } else {
        }

        if (weedModel.getImage() != null && !weedModel.getImage().equals("")) {
            if (!TextUtils.isEmpty(CommonUtils.getImageUrl(context)) && !TextUtils.isEmpty(weedModel.getImage())) {
                Glide.with(context)
                        .load(CommonUtils.getImageUrl(context) + "" + weedModel.getImage())
                        .asBitmap()
                        .placeholder(R.drawable.weed_default)
                        .error(R.drawable.weed_default)
                        .into(holder.imageIV);
            }
        } else {
        }


        holder.rlAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartItemCick.onCartClick(position);
            }
        });

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onItemClick(position);
            }
        });

        holder.mOffPrTxt.setText("40% OFF");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public int filter(String text) {
        text = text.toLowerCase(Locale.getDefault());

        mData.clear();
        if (text.length() == 0) {
            mData.addAll(itemData);

        } else{
            for (ProductDetails list : itemData){
                if (list.getName().toLowerCase(Locale.getDefault()).contains(text)) {
                    mData.add(list);
                }
            }
        }
        notifyDataSetChanged();
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout topLL;
        CardView card_view;
        TextView weedNameTV, vegNonVegTV, ratingTV, typeTV, weightTV, priceRangeTV, mOffPrTxt;
        RelativeLayout rlAddCart;
        ImageView imageIV;


        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            topLL = itemView.findViewById(R.id.topLL);
            card_view = itemView.findViewById(R.id.card_view);
            mOffPrTxt = itemView.findViewById(R.id.txt_offpr);
            weedNameTV = itemView.findViewById(R.id.weedNameTV);
            vegNonVegTV = itemView.findViewById(R.id.vegNonVegTV);
            ratingTV = itemView.findViewById(R.id.ratingTV);
            typeTV = itemView.findViewById(R.id.typeTV);
            weightTV = itemView.findViewById(R.id.weightTV);
            priceRangeTV = itemView.findViewById(R.id.priceRangeTV);
            rlAddCart = itemView.findViewById(R.id.rlAddCart);
            imageIV = itemView.findViewById(R.id.imageIV);
        }
    }

}
