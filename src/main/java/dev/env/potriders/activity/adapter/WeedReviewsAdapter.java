package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.FullScreenViewActivity;
import dev.env.potriders.model.WeedReviewsModel;
import dev.env.potriders.utils.MySpannable;
import dev.env.potriders.utils.Utils;

public class WeedReviewsAdapter extends RecyclerView.Adapter<WeedReviewsAdapter.MyViewHolder> {
    private ArrayList<WeedReviewsModel> horizontalList;
    Context context;

    WeedItemCick weedItemCick;

    public interface WeedItemCick {
        void onItemClick(int position);
        void onSeeMoreClick();
    }

    public void setWeedItemCick(WeedItemCick weedItemCick) {
        this.weedItemCick = weedItemCick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView reviewsTV,tvReviewImageCount,tvAttachments;
        RatingBar ratingBar;
        ImageView ivReview1,ivReview2,ivReview3,ivSeeMore;
        RelativeLayout rlSeeMoreHover;
        LinearLayout llReviewImages;

        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            reviewsTV = itemView.findViewById(R.id.reviewsTV);
            tvReviewImageCount = itemView.findViewById(R.id.tvReviewImageCount);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            ivReview1 = itemView.findViewById(R.id.ivReview1);
            ivReview2 = itemView.findViewById(R.id.ivReview2);
            ivReview3 = itemView.findViewById(R.id.ivReview3);
            ivSeeMore = itemView.findViewById(R.id.ivSeeMore);
            rlSeeMoreHover = itemView.findViewById(R.id.rlSeeMoreHover);
            tvAttachments = itemView.findViewById(R.id.tvAttachments);
            llReviewImages = itemView.findViewById(R.id.llReviewImages);
        }
    }


    public WeedReviewsAdapter(ArrayList<WeedReviewsModel> horizontalList, Context mContext) {
        this.horizontalList = horizontalList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_weed_reviews, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.reviewsTV.setText(context.getString(R.string.str_lorem_ipsum));
        makeTextViewResizable(holder.reviewsTV, 3, "Read More", true);

        int image_count = 0;

        if (horizontalList.get(position).getStrWeedReviewImageCount() != null && !horizontalList.get(position).getStrWeedReviewImageCount().equals(""))
            image_count = Integer.parseInt(horizontalList.get(position).getStrWeedReviewImageCount());

        if (image_count == 0) {
            holder.llReviewImages.setVisibility(View.GONE);
            holder.tvAttachments.setVisibility(View.GONE);
        } else if (image_count == 1) {
            holder.ivReview2.setVisibility(View.GONE);
            holder.ivReview3.setVisibility(View.GONE);
            holder.ivSeeMore.setVisibility(View.GONE);
            holder.rlSeeMoreHover.setVisibility(View.GONE);
        } else if (image_count == 2) {
            holder.ivReview3.setVisibility(View.GONE);
            holder.ivSeeMore.setVisibility(View.GONE);
            holder.rlSeeMoreHover.setVisibility(View.GONE);
        } else if (image_count == 3) {
            holder.ivSeeMore.setVisibility(View.GONE);
            holder.rlSeeMoreHover.setVisibility(View.GONE);
        } else if (image_count == 4) {
            holder.rlSeeMoreHover.setVisibility(View.GONE);
        } else if (image_count > 3) {
            holder.tvReviewImageCount.setText("+"+(image_count - 4));
        }

        holder.ivReview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFullScreenActivity(position);
            }
        });

        holder.ivReview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFullScreenActivity(position);
            }
        });

        holder.ivReview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFullScreenActivity(position);
            }
        });

        holder.rlSeeMoreHover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFullScreenActivity(position);
            }
        });

    }

    public void openFullScreenActivity(int position) {
        Intent intent = new Intent(context, FullScreenViewActivity.class);

        ArrayList<String> arr_img_list = new ArrayList<>();
        if (Integer.parseInt(horizontalList.get(position).getStrWeedReviewImageCount()) > 0) {
            for (int i = 0; i < Integer.parseInt(horizontalList.get(position).getStrWeedReviewImageCount()); i++) {
                arr_img_list.add("");
            }
        } else {
            arr_img_list.add("");
        }

        if (arr_img_list.size() > 0){
//            intent.putExtra(Utils.EXTRA_IMG_LIST,arr_img_list);
            intent.putStringArrayListExtra(Utils.EXTRA_IMG_LIST,arr_img_list);
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    public void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false){
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
//                        makeTextViewResizable(tv, -1, "See Less", false);
                        makeTextViewResizable(tv, 3, "Read More", true);
                        weedItemCick.onSeeMoreClick();
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
//                        makeTextViewResizable(tv, 3, ".. See More", true);
                        makeTextViewResizable(tv, 3, "Read More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }
}
