package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import dev.env.potriders.R;

public class SearchAdapter extends BaseAdapter implements Filterable {

    ArrayList<String> fruits;
    Context context;
    ArrayList<String> mStringFilterList = new ArrayList<>();
    ValueFilter valueFilter;

    public SearchAdapter(Context context, ArrayList<String> fruits) {
        this.context = context;
        this.fruits = fruits;
    }

    @Override
    public int getCount() {
        return fruits.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {

            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.list_item, viewGroup, false);
        }

        TextView product_name = view.findViewById(R.id.product_name);

        product_name.setText(fruits.get(i)+"");

        return view;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {

            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<String> filterList=new ArrayList<String>();
                for(int i=0;i<mStringFilterList.size();i++){
                    if((mStringFilterList.get(i).toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        String result = mStringFilterList.get(i);
                        filterList.add(result);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=mStringFilterList.size();
                results.values=mStringFilterList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            fruits=(ArrayList<String>) results.values;
            notifyDataSetChanged();
        }
    }

}
