package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;


import com.squareup.picasso.Picasso;

import dev.env.potriders.R;
import dev.env.potriders.databinding.ChooseItemBinding;


public class ChangeLogAdapter extends PagerAdapter {

    Context cxt;

    SwipePager swipePager;

    public interface SwipePager {
        public void onSwipe(int pos);
    }

    public void setSwipePager(SwipePager swipePager) {
        this.swipePager = swipePager;
    }

    public int getCount() {
        return 2;
    }

    public ChangeLogAdapter(Context context) {
        cxt = context;
    }

    public Object instantiateItem(View collection, int position) {

        ChooseItemBinding binding;

        binding = DataBindingUtil.inflate(LayoutInflater.from(cxt), R.layout.choose_item, null, false);

        switch (position) {

            case 0:
                Picasso.get().load(R.drawable.bnr_weed).into(binding.logo);
                binding.title.setText(cxt.getString(R.string.slider_title1));
                binding.description.setText(cxt.getString(R.string.slider_description1));
                break;

            case 1:
                Picasso.get().load(R.drawable.bnr_plate).into(binding.logo);
                binding.title.setText(cxt.getString(R.string.slider_title2));
                binding.description.setText(cxt.getString(R.string.slider_description2));
                break;

        }

//        swipePager.onSwipe(position);

        ((ViewPager) collection).addView(binding.getRoot(), 0);

        return binding.getRoot();
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {

        ((ViewPager) arg0).removeView((View) arg2);

    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {

        return arg0 == ((View) arg1);
    }

    @Override
    public Parcelable saveState() {

        return null;
    }

}