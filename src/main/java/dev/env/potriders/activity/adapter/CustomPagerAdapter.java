package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.FullScreenViewActivity;
import dev.env.potriders.utils.Utils;

public class CustomPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    private ArrayList<String> _imagePaths;

    public CustomPagerAdapter(Context context, ArrayList<String> imagePaths) {
        this.context = context;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = layoutInflater.inflate(R.layout.item_row_viewpager, container,
                false);

        ImageView imgDisplay = (ImageView) viewLayout.findViewById(R.id.imageView);
        //listening to image click
        imgDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenActivity();
            }
        });

        Glide.with(context)
                .load(_imagePaths.get(position))
                .into(imgDisplay);
        (container).addView(viewLayout);

        return viewLayout;
    }

    public void openFullScreenActivity() {


       /* ArrayList<String> arr_img_list = new ArrayList<>();
        if (Integer.parseInt(horizontalList.get(position).getStrWeedReviewImageCount()) > 0) {
            for (int i = 0; i < Integer.parseInt(horizontalList.get(position).getStrWeedReviewImageCount()); i++) {
                arr_img_list.add("");
            }
        } else {
            arr_img_list.add("");
        }*/

        if (_imagePaths.size() > 0){
//            intent.putExtra(Utils.EXTRA_IMG_LIST,arr_img_list);
            Intent intent = new Intent(context, FullScreenViewActivity.class);
            intent.putStringArrayListExtra(Utils.EXTRA_IMG_LIST,_imagePaths);
            context.startActivity(intent);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
