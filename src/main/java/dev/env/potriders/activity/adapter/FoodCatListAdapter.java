package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dev.env.potriders.R;

public class FoodCatListAdapter extends RecyclerView.Adapter<FoodCatListAdapter.MyViewHolder> {
    private ArrayList<String> horizontalList;
    Context context;

    WeedItemCick weedItemCick;

    public interface WeedItemCick {
        void onItemClick(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick) {
        this.weedItemCick = weedItemCick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subCategoryTV;


        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();
            subCategoryTV = itemView.findViewById(R.id.subCategoryTV);
        }
    }


    public FoodCatListAdapter(ArrayList<String> horizontalList, Context mContext) {
        this.horizontalList = horizontalList;
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_food_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == 0) {
            holder.subCategoryTV.setBackgroundResource(R.drawable.category_blue_background);
            holder.subCategoryTV.setTextColor(ContextCompat.getColor(context,R.color.white));
        }
        else {
            holder.subCategoryTV.setBackgroundResource(R.drawable.category_gray_background);
            holder.subCategoryTV.setTextColor(ContextCompat.getColor(context,R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
