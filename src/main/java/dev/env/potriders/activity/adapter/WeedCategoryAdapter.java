package dev.env.potriders.activity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import dev.env.potriders.R;
import dev.env.potriders.model.CategoryDetails;
import dev.env.potriders.utils.CommonUtils;

public class WeedCategoryAdapter extends RecyclerView.Adapter<WeedCategoryAdapter.MyViewHolder> {

    private List<CategoryDetails> mData;
    ArrayList<CategoryDetails> itemData;
    Context context;
    WeedItemCick weedItemCick;


    public interface WeedItemCick {
        void onItemClick(int position);
    }

    public void setWeedItemCick(WeedItemCick weedItemCick) {
        this.weedItemCick = weedItemCick;
    }


    public WeedCategoryAdapter(List<CategoryDetails> data, Context mContext) {
        this.mData = data;
        this.context = mContext;
        this.itemData = new ArrayList<CategoryDetails>();
        this.itemData.addAll(data);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_weed_catlist, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        CategoryDetails weedCatModel = mData.get(position);

        if (weedCatModel.getName() != null && !weedCatModel.getName().equals("")) {
            holder.weedNameTV.setText(weedCatModel.getName());
        } else {
        }

        if (weedCatModel.getImage() != null && !weedCatModel.getImage().equals("")) {

            if (!TextUtils.isEmpty(CommonUtils.getImageUrl(context)) && !TextUtils.isEmpty(weedCatModel.getImage())) {
                /*Glide.with(context)
                        .load(CommonUtils.getImageUrl(context) + "" + weedModel.getImage())
                        .asBitmap()
                        .placeholder(R.drawable.weed_default)
                        .error(R.drawable.weed_default)
                        .into(holder.imageIV);*/

                Picasso.get()
                        .load(CommonUtils.getImageUrl(context) + "" + weedCatModel.getImage())
                        .placeholder(R.drawable.weed_default)
                        .error(R.drawable.weed_default)
                        .into(holder.imageIV);
            }
        } else {
        }

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weedItemCick.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public int filter(String text) {
        text = text.toLowerCase(Locale.getDefault());

        mData.clear();
        if (text.length() == 0) {
            mData.addAll(itemData);

        } else{
            for (CategoryDetails list : itemData){
                if (list.getName().toLowerCase(Locale.getDefault()).contains(text)) {
                    mData.add(list);
                }
            }
        }
        notifyDataSetChanged();
        return mData.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView card_view;
        TextView weedNameTV;
        ImageView imageIV;


        public MyViewHolder(View view) {
            super(view);

            card_view = view.findViewById(R.id.card_view);
            weedNameTV = view.findViewById(R.id.nameTV);
            imageIV = view.findViewById(R.id.imageIV);
        }
    }

}
