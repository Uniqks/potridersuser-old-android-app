package dev.env.potriders.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import dev.env.potriders.R;
import dev.env.potriders.activity.viewActivity.BaseActivity;
import dev.env.potriders.activity.viewActivity.MainActivity;
import dev.env.potriders.activity.viewActivity.TermsConditionsActivity;
import dev.env.potriders.retrofit.ApiClient;
import dev.env.potriders.retrofit.ApiInterface;
import dev.env.potriders.utils.CommonUtils;
import dev.env.potriders.utils.Constants;
import dev.env.potriders.utils.LogUtils;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.utils.PreferenceUtil;
import dev.env.potriders.utils.Utils;
import dev.env.potriders.app.Validation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import spencerstudios.com.bungeelib.Bungee;


public class Register extends BaseActivity implements OnClickListener {

    public static final int SELECT_PHOTO = 1;
    public static final int TAKENPHOTO = 0;
    public String photo_path = "";

    Uri uri;
    File photofile;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private int STORAGE_PERMISSION_CODE = 11;

    private Toolbar mToolbar;
    private TextView mTitleTxt;


    private LinearLayout mMainLl;
    private TextView mTermsTxt;
    private CheckBox mTermsCheck;
    private EditText mFnameEdt, mLnameEdt, mEmailEdt, mPwdEdt, mPhoneEdt;
    private ImageView mProImg;


    /**
     *  multimedia permission...
     */
    public static boolean hasPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mToolbar = findViewById(R.id.toolbar);





        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mTitleTxt = mToolbar.findViewById(R.id.textViewTitle);
        mTitleTxt.setText(getString(R.string.sign_up));
        mToolbar.findViewById(R.id.imageViewBack).setOnClickListener(this);


        findViewById(R.id.ll_login).setOnClickListener(this);
        findViewById(R.id.btn_signup).setOnClickListener(this);

        mMainLl = findViewById(R.id.ll_main);
        mMainLl.setVisibility(View.GONE);
        setAnimationView(mMainLl);

        mFnameEdt = findViewById(R.id.edt_fname);
        mLnameEdt = findViewById(R.id.edt_lname);
        mEmailEdt = findViewById(R.id.edt_email);
        mPwdEdt = findViewById(R.id.edt_pwd);
        mPhoneEdt = findViewById(R.id.edt_phone);

        mTermsCheck = findViewById(R.id.check_terms);
        mTermsTxt = findViewById(R.id.txt_terms);
        mTermsTxt.setOnClickListener(this);
        String strTerms = "By clicking this box, I have read the " + "<b>" + "Terms & Conditions " + "</b>" + "of this app.";
        mTermsTxt.setText(Html.fromHtml(strTerms));


        mProImg = findViewById(R.id.img_pro);
        findViewById(R.id.img_camera).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageViewBack:
                hideKeyboard();
                finish();
                break;

            case R.id.ll_login:
                hideKeyboard();
                Intent intent = new Intent(Register.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;

            case R.id.btn_signup:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(findViewById(R.id.btn_signup));

                hideKeyboard();
                checkValidation();
                break;

            case R.id.txt_terms:
                hideKeyboard();
                Intent intent2 = new Intent(Register.this, TermsConditionsActivity.class);
                startActivity(intent2);
                break;

            case R.id.img_camera:
                hideKeyboard();
                if (!hasPermissions(Register.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(Register.this, PERMISSIONS, STORAGE_PERMISSION_CODE);
                } else {
                    showPopup();
                }
                break;
        }
    }


    /**
     *  Method is used to check validation...
     */
    private void checkValidation() {

        if (!isValidFName()) {
            return;
        }
        if (!isValidLName()) {
            return;
        }
        if (!isValidEmail()) {
            return;
        }
        if (!isValidatePassword()) {
            return;
        }

        if (!isValidPhone()) {
            return;
        }

        if (!mTermsCheck.isChecked()) {
            Toast.makeText(Register.this, getString(R.string.err_accept_terms_conditions), Toast.LENGTH_SHORT).show();
            return;
        }

        if (Utils.isNetworkAvailable(Register.this)) {
            doRegisterService();
        } else {
            CommonUtils.makeToast(getString(R.string.no_internet_connection), Register.this);
        }
    }


    /**
     *  Method is used to register user...
     */
    private void doRegisterService() {
        showProgressDialog();

        ApiInterface apiInterface = ApiClient.getStringClient().create(ApiInterface.class);

        String name = mFnameEdt.getText().toString().trim();
        String lname = mLnameEdt.getText().toString().trim();
        String email = mEmailEdt.getText().toString().trim();
        String password = mPwdEdt.getText().toString().trim();
        String phone = mPhoneEdt.getText().toString().trim();

        Map<String, String> map = new HashMap<>();
        map.put("fname", name);
        map.put("lname", lname);
        map.put("email", email);
        map.put("password", password);
        map.put("userType", Constants.USER_TYPE);
        map.put("phone", phone);
        map.put("deviceToken", "1132155d5ffjfbj");
        map.put("deviceType", Constants.DEVICE_TYPE);
        File file = new File(photo_path);
        Call<String> call = null;
        if (photo_path.equalsIgnoreCase("")) {

            call = apiInterface.userRegister(map);
        } else {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
            call = apiInterface.userRegisterWithImage(name, lname, email, password, Constants.USER_TYPE,
                    phone, "1132155d5ffjfbj", Constants.DEVICE_TYPE, body);
        }


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                hideProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");
                    if (status == Constants.SUCCESS) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        int id = jsonObject1.getInt("id");
                        String fname = jsonObject1.getString("fname");
                        String lname = jsonObject1.getString("lname");
                        String email = jsonObject1.getString("email");
                        String phone = jsonObject1.getString("phone");
                        String userType = jsonObject1.getString("userType");
                        String image = jsonObject1.getString("image");


                        CommonUtils.makeToast("Register successfully", Register.this);
                        PreferenceUtil.getPref(Register.this).edit().putInt(PreferenceKeys.USER_ID, id)
                                .putString(PreferenceKeys.FIRST_NAME, fname)
                                .putString(PreferenceKeys.LAST_NAME, lname)
                                .putString(PreferenceKeys.EMAIL, email)
                                .putString(PreferenceKeys.PHONE, phone)
                                .putString(PreferenceKeys.PROPIC, image)
                                .putBoolean(PreferenceKeys.IS_LOGIN, true).apply();

                        if (PreferenceUtil.getPref(Register.this).getBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false)) {
                            Intent intent2 = new Intent(Register.this, MainActivity.class);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent2);
                            finish();

                        } else {

                            Intent intent2 = new Intent(Register.this, MainActivity.class);
                            startActivity(intent2);
                            finish();
                            Bungee.slideLeft(Register.this);
                        }


                    } else {
                        CommonUtils.makeToast(msg, Register.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CommonUtils.makeToast(getString(R.string.please_try_again), Register.this);
                hideProgressDialog();
            }
        });
    }

    private boolean isValidPhone() {
        return Validation.hasText(mPhoneEdt, getString(R.string.err_empty_phone_number));
    }

    private boolean isValidLName() {
        return Validation.hasText(mLnameEdt, getString(R.string.err_empty_lastname));
    }

    private boolean isValidFName() {
        return Validation.hasText(mFnameEdt, getString(R.string.err_empty_firstname));
    }

    private boolean isValidEmail() {
        if (!Validation.hasText(mEmailEdt, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(mEmailEdt, true, getString(R.string.error_email_invalid));
    }

    private boolean isValidatePassword() {
        if (!Validation.hasText(mPwdEdt, getString(R.string.err_empty_password))) {
            return false;
        }
        return Validation.isValidPassword(mPwdEdt, getString(R.string.error_pass_invalid));
    }


    /**
     *  Method is used to initialized set animation....
     */
    public void setAnimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    /**
     *  Method is used to initialized selcet image...
     */
    public void showPopup() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.str_select_one));
        builder.setMessage(getString(R.string.str_select_gallery_msg));
        builder.setNegativeButton(getString(R.string.str_camera).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                takePhotoFromCamera();
            }
        });
        builder.setPositiveButton(getString(R.string.str_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), SELECT_PHOTO);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void takePhotoFromCamera() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            photofile = Utils.getInstance(this).create_file(".jpg");
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            uri = FileProvider.getUriForFile(this, "dev.env.potriders.fileprovider", photofile);
        } else {
            photofile = Utils.getInstance(this).create_Image_File(".jpg");
            uri = Uri.fromFile(photofile);

        }
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(Intent.createChooser(cameraIntent, getString(R.string.lbl_capture)), TAKENPHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == TAKENPHOTO) && (resultCode == Activity.RESULT_OK)) {

            if (uri != null) {

                photo_path = photofile.getAbsolutePath();//uri.getPath();

                LogUtils.e(uri.getPath() + " imageFilePath " + new File(uri.toString()).getAbsolutePath());
                if (photo_path != null && photo_path.length() > 0) {
                    Glide.with(this)
                            .load(new File(photo_path))
                            .asBitmap()
                            .centerCrop()
                            .into(new BitmapImageViewTarget(mProImg) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    mProImg.setImageDrawable(circularBitmapDrawable);
                                }
                            });
                }
            } else {
                Utils.getInstance(this).Toast(this, getString(R.string.toast_unable_to_selct_image), false);
            }

        } else if ((requestCode == SELECT_PHOTO) && (resultCode == Activity.RESULT_OK)) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            photo_path = cursor.getString(columnIndex);
            try {
                LogUtils.i("", "SELECT_PHOTO " + photo_path);
                Glide.with(this)
                        .load(new File(photo_path))
                        .asBitmap()
                        .centerCrop()
                        .into(new BitmapImageViewTarget(mProImg) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                mProImg.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (photo_path != null && !photo_path.equals(""))
            LogUtils.i("Register" + " onActivityResult photo_path " + photo_path);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setAnimationView(mMainLl);
    }
}
