package dev.env.potriders.activity.dialog;


import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import dev.env.potriders.R;
import dev.env.potriders.activity.Login;
import dev.env.potriders.activity.Splash;
import dev.env.potriders.activity.viewActivity.ChooseOptionActivity;
import dev.env.potriders.activity.viewActivity.MainActivity;
import dev.env.potriders.app.PreferenceKeys;
import dev.env.potriders.databinding.DialogSplashAlertBinding;
import dev.env.potriders.utils.PreferenceUtil;
import spencerstudios.com.bungeelib.Bungee;


public class SplashAlertDialog extends DialogFragment implements View.OnClickListener {


    DialogSplashAlertBinding binding;






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_splash_alert, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);








        initControls();
        return binding.getRoot();
    }


    /**
     * Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.yesRl.setOnClickListener(this);
        binding.noRl.setOnClickListener(this);

        YoYo.with(Techniques.Tada)
                .duration(700)
                .playOn(binding.topRl);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.yesRl:
                SharedPreferences preferences = getActivity().getSharedPreferences("reg_pref", 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("is_register",true);
                editor.apply();
                getDialog().dismiss();

                if (PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceKeys.IS_STARTED, false)) {
                    if (PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceKeys.IS_LOGIN, false)) {

                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                        Bungee.slideLeft(getActivity());

                    } else {
                        PreferenceUtil.getPref(getActivity()).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                        startActivity(new Intent(getActivity(), Login.class));
                        getActivity().finish();
                        Bungee.slideLeft(getActivity());
                    }

                } else {
                    startActivity(new Intent(getActivity(), ChooseOptionActivity.class));
                    getActivity().finish();
                    Bungee.slideLeft(getActivity());
                }
                break;

            case R.id.noRl:
                getDialog().dismiss();
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().finish();
                /*if (PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceKeys.IS_STARTED, false)) {
                    if (PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceKeys.IS_LOGIN, false)) {

                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                        Bungee.slideLeft(getActivity());

                    } else {
                        PreferenceUtil.getPref(getActivity()).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                        startActivity(new Intent(getActivity(), Login.class));
                        getActivity().finish();
                        Bungee.slideLeft(getActivity());
                    }

                } else {
                    startActivity(new Intent(getActivity(), ChooseOptionActivity.class));
                    getActivity().finish();
                    Bungee.slideLeft(getActivity());
                }*/
                break;
        }
    }
}
