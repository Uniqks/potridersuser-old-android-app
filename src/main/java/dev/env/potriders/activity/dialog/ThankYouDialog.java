package dev.env.potriders.activity.dialog;


import android.animation.ValueAnimator;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import dev.env.potriders.R;
import dev.env.potriders.activity.fragment.TrackDeliveryFragment;
import dev.env.potriders.activity.viewActivity.MainActivity;
import dev.env.potriders.databinding.DialogThankYouBinding;
import dev.env.potriders.utils.Constants;


public class ThankYouDialog extends DialogFragment implements View.OnClickListener{

    DialogThankYouBinding binding;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_thank_you, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);



        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {


        binding.topRl.setVisibility(View.GONE);
        setAnimationDownView(binding.topRl);
        binding.trackOrderTV.setOnClickListener(this);


        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(1800L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                final float progress = (float) animation.getAnimatedValue();
                final float width = binding.logoIV.getWidth();
                final float translationX = width * progress;
                binding.logoIV.setTranslationX(translationX);
                binding.logoIV.setTranslationX(width - translationX);
            }
        });
        animator.start();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.trackOrderTV:
                //getActivity().getSupportFragmentManager().popBackStack();
                getDialog().dismiss();
                /*startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();*/

                ((MainActivity)getActivity()).slidingRootNav.setMenuLocked(true);
                TrackDeliveryFragment trackDeliveryFragment = new TrackDeliveryFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.mCLASS, "TrackOrder");
                trackDeliveryFragment.setArguments(bundle);
                ((MainActivity)getActivity()).pushFragment(trackDeliveryFragment, true, false, true,
                        TrackDeliveryFragment.class.getSimpleName(), null, false);
                break;
        }
    }


    /**
     *  Method is used to initialized animation...
     */
    public void setAnimationDownView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


}
