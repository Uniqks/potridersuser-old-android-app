package dev.env.potriders.activity.dialog;


import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import dev.env.potriders.R;
import dev.env.potriders.databinding.DialogAddToCartBinding;


public class AddToCartDialog extends DialogFragment implements View.OnClickListener{


    DialogAddToCartBinding binding;
    private int mWeedCount = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_to_cart, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);







        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.topRl.setVisibility(View.GONE);
        setAnimationDownView(binding.topRl);

        binding.closeIV.setOnClickListener(this);
        binding.addtocartLL.setOnClickListener(this);
        binding.decrementIV.setOnClickListener(this);
        binding.incrementIV.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized animation...
     */
    public void setAnimationDownView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    public void setAnimationUpView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.addtocartLL:
                Toast.makeText(getActivity(), "item added successfully.", Toast.LENGTH_SHORT).show();
                break;

            case R.id.decrementIV:
                changeQuantity(false);
                break;

            case R.id.incrementIV:
                changeQuantity(true);
                break;

            case R.id.closeIV:
                YoYo.with(Techniques.ZoomOut)
                        .duration(700)
                        .repeat(0)
                        .playOn(binding.topRl);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDialog().dismiss();
                    }
                }, 700);
                break;
        }
    }


    private void changeQuantity(boolean isPlus){
        if (isPlus){
            mWeedCount = mWeedCount + 1;
        } else {

            mWeedCount = mWeedCount - 1;
            if (mWeedCount < 1){
                mWeedCount = 1;
            }
        }
        binding.quantityTV.setText(String.valueOf(mWeedCount));
    }
}
