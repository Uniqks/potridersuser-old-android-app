package dev.env.potriders.app;

public class PreferenceKeys {

    public static final String IS_LOGIN = "IS_LOGIN";
    public static final String IS_STARTED = "IS_STARTED";
    public static final String IS_FROM_MAIN_ACTIVITY = "IS_FROM_MAIN_ACTIVITY";


    //    public static final String USER_NAME = "USER_NAME";

    //As per api param
    public static final String USER_ID = "USER_ID";
    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String USER_NAME = "username";
    // public static final String EMAIL = "email";
    //  public static final String PASSWORD = "password";

    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String EMAIL = "EMAIL";
    public static final String GENDER = "GENDER";
    public static final String PHONE = "PHONE";
    public static final String LOGIN_TYPE = "LOGIN_TYPE";
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
    public static final String PROPIC = "PROPIC";
    public static final String USER_CONTACT_NO = "USER_CONTACT_NO";

    public static final String ROLE = "ROLE";
    public static final String FROM_CURRENT = "FROM_CURRENT";
    public static final String FROM_GPLUS = "FROM_GPLUS";
    public static final String REDIRECT_GPLUS = "REDIRECT_GPLUS";
    public static final String FROM_FB = "FROM_FB";
    public static final String LOCATION = "LOCATION";
    public static final String BRANCH_ID = "BRANCH_ID";
    public static final String STORE_ID = "STORE_ID";
    public static final String APP_VERSION = "APP_VERSION";
    public static final String PUSH = "PUSH";
    public static final String TIME = "TIME";
    public static final String SETTING_EMAIL = "SETTING_EMAIL";
}