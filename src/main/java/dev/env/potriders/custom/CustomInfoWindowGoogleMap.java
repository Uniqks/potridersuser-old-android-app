package dev.env.potriders.custom;


import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import dev.env.potriders.R;


/**
 * Created by Seema on 12,February,2019
 */

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public CustomInfoWindowGoogleMap(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.map_custom_infowindow, null);

        TextView titleTv, snippetTv;
        titleTv = view.findViewById(R.id.titleTv);
        snippetTv = view.findViewById(R.id.snippetTv);

        String title = "", snippet = "";

        if (marker.getTitle() != null) {
            title = "<font color=#000000>" + marker.getTitle() + "</font>";
        }

        if (marker.getSnippet() != null) {
            snippet = "<br><font color=#379f00>" + marker.getSnippet() +"</font>";
        }

        String textInfo = title + snippet;
        titleTv.setText(Html.fromHtml(textInfo));

        return view;
    }
}
