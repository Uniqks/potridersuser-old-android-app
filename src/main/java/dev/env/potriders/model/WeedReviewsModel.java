package dev.env.potriders.model;

public class WeedReviewsModel {
    public String id;
    public String strUserPic;
    public String strUserName;
    public String strWeedReview;
    public String strWeedReviewDescription;
    public String strWeedReviewDateTime;
    public String strWeedReviewImageCount;

    public WeedReviewsModel(String id, String strUserPic, String strUserName, String strWeedReview, String strWeedReviewDescription,
                            String strWeedReviewDateTime, String strWeedReviewImageCount) {
        this.id = id;
        this.strUserPic = strUserPic;
        this.strUserName = strUserName;
        this.strWeedReview = strWeedReview;
        this.strWeedReviewDescription = strWeedReviewDescription;
        this.strWeedReviewDateTime = strWeedReviewDateTime;
        this.strWeedReviewImageCount = strWeedReviewImageCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrUserPic() {
        return strUserPic;
    }

    public void setStrUserPic(String strUserPic) {
        this.strUserPic = strUserPic;
    }

    public String getStrUserName() {
        return strUserName;
    }

    public void setStrUserName(String strUserName) {
        this.strUserName = strUserName;
    }

    public String getStrWeedReview() {
        return strWeedReview;
    }

    public void setStrWeedReview(String strWeedReview) {
        this.strWeedReview = strWeedReview;
    }

    public String getStrWeedReviewDescription() {
        return strWeedReviewDescription;
    }

    public void setStrWeedReviewDescription(String strWeedReviewDescription) {
        this.strWeedReviewDescription = strWeedReviewDescription;
    }

    public String getStrWeedReviewDateTime() {
        return strWeedReviewDateTime;
    }

    public void setStrWeedReviewDateTime(String strWeedReviewDateTime) {
        this.strWeedReviewDateTime = strWeedReviewDateTime;
    }

    public String getStrWeedReviewImageCount() {
        return strWeedReviewImageCount;
    }

    public void setStrWeedReviewImageCount(String strWeedReviewImageCount) {
        this.strWeedReviewImageCount = strWeedReviewImageCount;
    }
}
