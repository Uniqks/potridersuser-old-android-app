package dev.env.potriders.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private int status;
    public final static Parcelable.Creator<WeightModel> CREATOR = new Creator<WeightModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WeightModel createFromParcel(Parcel in) {
            return new WeightModel(in);
        }

        public WeightModel[] newArray(int size) {
            return (new WeightModel[size]);
        }

    };

    protected WeightModel(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((int) in.readValue((int.class.getClassLoader())));
    }

    public WeightModel() {
    }

    public WeightModel(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(status);
    }

    public int describeContents() {
        return 0;
    }
}
