package dev.env.potriders.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeedDetail implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("units_price")
    @Expose
    private String unitsPrice;
    @SerializedName("menu")
    @Expose
    private String menu;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("relaxed_per")
    @Expose
    private int relaxedPer;
    @SerializedName("euphoric_per")
    @Expose
    private int euphoricPer;
    @SerializedName("happy_per")
    @Expose
    private int happyPer;
    @SerializedName("uplifted_per")
    @Expose
    private int upliftedPer;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("cat_name")
    @Expose
    private CatName catName;
    @SerializedName("additional_images")
    @Expose
    private AdditionalImages additionalImages;
    @SerializedName("menu_name")
    @Expose
    private String menuName;

    @SerializedName("item_review")
    @Expose
    private List<ItemReview> itemReview = null;
    public final static Parcelable.Creator<WeedDetail> CREATOR = new Creator<WeedDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WeedDetail createFromParcel(Parcel in) {
            return new WeedDetail(in);
        }

        public WeedDetail[] newArray(int size) {
            return (new WeedDetail[size]);
        }

    };

    protected WeedDetail(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.desc = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.price = ((String) in.readValue((String.class.getClassLoader())));
        this.units = ((String) in.readValue((String.class.getClassLoader())));
        this.unitsPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.menu = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((int) in.readValue((int.class.getClassLoader())));
        this.relaxedPer = ((int) in.readValue((int.class.getClassLoader())));
        this.euphoricPer = ((int) in.readValue((int.class.getClassLoader())));
        this.happyPer = ((int) in.readValue((int.class.getClassLoader())));
        this.upliftedPer = ((int) in.readValue((int.class.getClassLoader())));
        this.created = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((int) in.readValue((int.class.getClassLoader())));
        this.catName = ((CatName) in.readValue((CatName.class.getClassLoader())));
        this.additionalImages = ((AdditionalImages) in.readValue((AdditionalImages.class.getClassLoader())));
        this.menuName = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.itemReview, (ItemReview.class.getClassLoader()));
    }

    public WeedDetail() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getUnitsPrice() {
        return unitsPrice;
    }

    public void setUnitsPrice(String unitsPrice) {
        this.unitsPrice = unitsPrice;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRelaxedPer() {
        return relaxedPer;
    }

    public void setRelaxedPer(int relaxedPer) {
        this.relaxedPer = relaxedPer;
    }

    public int getEuphoricPer() {
        return euphoricPer;
    }

    public void setEuphoricPer(int euphoricPer) {
        this.euphoricPer = euphoricPer;
    }

    public int getHappyPer() {
        return happyPer;
    }

    public void setHappyPer(int happyPer) {
        this.happyPer = happyPer;
    }

    public int getUpliftedPer() {
        return upliftedPer;
    }

    public void setUpliftedPer(int upliftedPer) {
        this.upliftedPer = upliftedPer;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CatName getCatName() {
        return catName;
    }

    public void setCatName(CatName catName) {
        this.catName = catName;
    }

    public AdditionalImages getAdditionalImages() {
        return additionalImages;
    }

    public void setAdditionalImages(AdditionalImages additionalImages) {
        this.additionalImages = additionalImages;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public List<ItemReview> getItemReview() {
        return itemReview;
    }

    public void setItemReview(List<ItemReview> itemReview) {
        this.itemReview = itemReview;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(desc);
        dest.writeValue(image);
        dest.writeValue(price);
        dest.writeValue(units);
        dest.writeValue(unitsPrice);
        dest.writeValue(menu);
        dest.writeValue(category);
        dest.writeValue(type);
        dest.writeValue(relaxedPer);
        dest.writeValue(euphoricPer);
        dest.writeValue(happyPer);
        dest.writeValue(upliftedPer);
        dest.writeValue(created);
        dest.writeValue(status);
        dest.writeValue(catName);
        dest.writeValue(additionalImages);
        dest.writeValue(menuName);
        dest.writeList(itemReview);
    }

    public int describeContents() {
        return 0;
    }

    public static class CatName implements Parcelable {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private int type;
        @SerializedName("cover_image")
        @Expose
        private String coverImage;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("address2")
        @Expose
        private String address2;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("phone")
        @Expose
        private int phone;
        @SerializedName("status")
        @Expose
        private int status;
        public final static Parcelable.Creator<CatName> CREATOR = new Creator<CatName>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public CatName createFromParcel(Parcel in) {
                return new CatName(in);
            }

            public CatName[] newArray(int size) {
                return (new CatName[size]);
            }

        };

        protected CatName(Parcel in) {
            this.id = ((int) in.readValue((int.class.getClassLoader())));
            this.name = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((int) in.readValue((int.class.getClassLoader())));
            this.coverImage = ((String) in.readValue((String.class.getClassLoader())));
            this.image = ((String) in.readValue((String.class.getClassLoader())));
            this.address1 = ((String) in.readValue((String.class.getClassLoader())));
            this.address2 = ((String) in.readValue((String.class.getClassLoader())));
            this.city = ((String) in.readValue((String.class.getClassLoader())));
            this.state = ((String) in.readValue((String.class.getClassLoader())));
            this.pincode = ((String) in.readValue((String.class.getClassLoader())));
            this.country = ((String) in.readValue((String.class.getClassLoader())));
            this.phone = ((int) in.readValue((int.class.getClassLoader())));
            this.status = ((int) in.readValue((int.class.getClassLoader())));
        }

        public CatName() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getCoverImage() {
            return coverImage;
        }

        public void setCoverImage(String coverImage) {
            this.coverImage = coverImage;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public int getPhone() {
            return phone;
        }

        public void setPhone(int phone) {
            this.phone = phone;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(name);
            dest.writeValue(type);
            dest.writeValue(coverImage);
            dest.writeValue(image);
            dest.writeValue(address1);
            dest.writeValue(address2);
            dest.writeValue(city);
            dest.writeValue(state);
            dest.writeValue(pincode);
            dest.writeValue(country);
            dest.writeValue(phone);
            dest.writeValue(status);
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class AdditionalImages implements Parcelable {

        public final static Parcelable.Creator<AdditionalImages> CREATOR = new Creator<AdditionalImages>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public AdditionalImages createFromParcel(Parcel in) {
                return new AdditionalImages(in);
            }

            public AdditionalImages[] newArray(int size) {
                return (new AdditionalImages[size]);
            }

        };

        protected AdditionalImages(Parcel in) {
        }

        public AdditionalImages() {
        }

        public void writeToParcel(Parcel dest, int flags) {
        }

        public int describeContents() {
            return 0;
        }

    }

    public static class ItemReview implements Parcelable {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("user_id")
        @Expose
        private int userId;
        @SerializedName("item_id")
        @Expose
        private int itemId;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("rate_star")
        @Expose
        private int rateStar;
        @SerializedName("status")
        @Expose
        private int status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("fname")
        @Expose
        private String fname;
        @SerializedName("lname")
        @Expose
        private String lname;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("userType")
        @Expose
        private int userType;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("forgot_key")
        @Expose
        private String forgotKey;
        @SerializedName("deviceToken")
        @Expose
        private String deviceToken;
        @SerializedName("deviceType")
        @Expose
        private String deviceType;
        public final static Parcelable.Creator<ItemReview> CREATOR = new Creator<ItemReview>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public ItemReview createFromParcel(Parcel in) {
                return new ItemReview(in);
            }

            public ItemReview[] newArray(int size) {
                return (new ItemReview[size]);
            }

        };

        protected ItemReview(Parcel in) {
            this.id = ((int) in.readValue((int.class.getClassLoader())));
            this.userId = ((int) in.readValue((int.class.getClassLoader())));
            this.itemId = ((int) in.readValue((int.class.getClassLoader())));
            this.description = ((String) in.readValue((String.class.getClassLoader())));
            this.rateStar = ((int) in.readValue((int.class.getClassLoader())));
            this.status = ((int) in.readValue((int.class.getClassLoader())));
            this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
            this.fname = ((String) in.readValue((String.class.getClassLoader())));
            this.lname = ((String) in.readValue((String.class.getClassLoader())));
            this.password = ((String) in.readValue((String.class.getClassLoader())));
            this.email = ((String) in.readValue((String.class.getClassLoader())));
            this.phone = ((String) in.readValue((String.class.getClassLoader())));
            this.userType = ((int) in.readValue((int.class.getClassLoader())));
            this.image = ((String) in.readValue((String.class.getClassLoader())));
            this.forgotKey = ((String) in.readValue((String.class.getClassLoader())));
            this.deviceToken = ((String) in.readValue((String.class.getClassLoader())));
            this.deviceType = ((String) in.readValue((String.class.getClassLoader())));
        }

        public ItemReview() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getItemId() {
            return itemId;
        }

        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getRateStar() {
            return rateStar;
        }

        public void setRateStar(int rateStar) {
            this.rateStar = rateStar;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getUserType() {
            return userType;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getForgotKey() {
            return forgotKey;
        }

        public void setForgotKey(String forgotKey) {
            this.forgotKey = forgotKey;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(userId);
            dest.writeValue(itemId);
            dest.writeValue(description);
            dest.writeValue(rateStar);
            dest.writeValue(status);
            dest.writeValue(createdAt);
            dest.writeValue(updatedAt);
            dest.writeValue(fname);
            dest.writeValue(lname);
            dest.writeValue(password);
            dest.writeValue(email);
            dest.writeValue(phone);
            dest.writeValue(userType);
            dest.writeValue(image);
            dest.writeValue(forgotKey);
            dest.writeValue(deviceToken);
            dest.writeValue(deviceType);
        }

        public int describeContents() {
            return 0;
        }
    }


}
