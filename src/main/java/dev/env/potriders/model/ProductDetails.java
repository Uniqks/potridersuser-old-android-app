package dev.env.potriders.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetails implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("units_price")
    @Expose
    private String unitsPrice;
    @SerializedName("menu")
    @Expose
    private String menu;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("cat_name")
    @Expose
    private CatName catName;
    @SerializedName("additional_images")
    @Expose
    private List<Object> additionalImages = null;
    @SerializedName("menu_name")
    @Expose
    private String menuName;
    public final static Parcelable.Creator<ProductDetails> CREATOR = new Creator<ProductDetails>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductDetails createFromParcel(Parcel in) {
            return new ProductDetails(in);
        }

        public ProductDetails[] newArray(int size) {
            return (new ProductDetails[size]);
        }

    };

    protected ProductDetails(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.desc = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.price = ((String) in.readValue((String.class.getClassLoader())));
        this.units = ((String) in.readValue((String.class.getClassLoader())));
        this.unitsPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.menu = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((int) in.readValue((int.class.getClassLoader())));
        this.created = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((int) in.readValue((int.class.getClassLoader())));
        this.catName = ((CatName) in.readValue((CatName.class.getClassLoader())));
        in.readList(this.additionalImages, (java.lang.Object.class.getClassLoader()));
        this.menuName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ProductDetails() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getUnitsPrice() {
        return unitsPrice;
    }

    public void setUnitsPrice(String unitsPrice) {
        this.unitsPrice = unitsPrice;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public CatName getCatName() {
        return catName;
    }

    public void setCatName(CatName catName) {
        this.catName = catName;
    }

    public List<Object> getAdditionalImages() {
        return additionalImages;
    }

    public void setAdditionalImages(List<Object> additionalImages) {
        this.additionalImages = additionalImages;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(desc);
        dest.writeValue(image);
        dest.writeValue(price);
        dest.writeValue(units);
        dest.writeValue(unitsPrice);
        dest.writeValue(menu);
        dest.writeValue(category);
        dest.writeValue(type);
        dest.writeValue(created);
        dest.writeValue(status);
        dest.writeValue(catName);
        dest.writeList(additionalImages);
        dest.writeValue(menuName);
    }

    public int describeContents() {
        return 0;
    }


    public static class CatName implements Parcelable {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private int type;
        @SerializedName("cover_image")
        @Expose
        private String coverImage;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("address2")
        @Expose
        private String address2;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("phone")
        @Expose
        private int phone;
        @SerializedName("status")
        @Expose
        private int status;
        public final static Parcelable.Creator<CatName> CREATOR = new Creator<CatName>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public CatName createFromParcel(Parcel in) {
                return new CatName(in);
            }

            public CatName[] newArray(int size) {
                return (new CatName[size]);
            }

        };

        protected CatName(Parcel in) {
            this.id = ((int) in.readValue((int.class.getClassLoader())));
            this.name = ((String) in.readValue((String.class.getClassLoader())));
            this.type = ((int) in.readValue((int.class.getClassLoader())));
            this.coverImage = ((String) in.readValue((String.class.getClassLoader())));
            this.image = ((String) in.readValue((String.class.getClassLoader())));
            this.address1 = ((String) in.readValue((String.class.getClassLoader())));
            this.address2 = ((String) in.readValue((String.class.getClassLoader())));
            this.city = ((String) in.readValue((String.class.getClassLoader())));
            this.state = ((String) in.readValue((String.class.getClassLoader())));
            this.pincode = ((String) in.readValue((String.class.getClassLoader())));
            this.country = ((String) in.readValue((String.class.getClassLoader())));
            this.phone = ((int) in.readValue((int.class.getClassLoader())));
            this.status = ((int) in.readValue((int.class.getClassLoader())));
        }

        public CatName() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getCoverImage() {
            return coverImage;
        }

        public void setCoverImage(String coverImage) {
            this.coverImage = coverImage;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public int getPhone() {
            return phone;
        }

        public void setPhone(int phone) {
            this.phone = phone;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(id);
            dest.writeValue(name);
            dest.writeValue(type);
            dest.writeValue(coverImage);
            dest.writeValue(image);
            dest.writeValue(address1);
            dest.writeValue(address2);
            dest.writeValue(city);
            dest.writeValue(state);
            dest.writeValue(pincode);
            dest.writeValue(country);
            dest.writeValue(phone);
            dest.writeValue(status);
        }

        public int describeContents() {
            return 0;
        }

    }
}
