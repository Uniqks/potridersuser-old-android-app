package dev.env.potriders.utils;

public class Constants {


    public static final String mCLASS = "class";
    public static final String mID = "id";


    public static final String APP_KEY = "NUTTAG01012018YB!@TRACKER358snr4hdn";

    public static final String INTENT_MAP_VIEW_DATA_KEY = "INTENT_MAP_VIEW_DATA_KEY";
    public static final String CHANNEL_ID = "channel-01";


    public static final int REQUEST_CODE_CAMERA = 18888;
    public static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 17777;
    public static final int REQUEST_CODE_READ_EXTERNAL_STORAGE = 16666;


    public static final int REQUEST_CODE_ACCESS_FINE_LOCATION = 15678;
    public static final int REQUEST_CHECK_SETTINGS = 12222;

    public static final long LOCATION_UPDATE = 3600000; // 1 min = 1 * 60 * 1000 ms


    public static final String LOAD_DEVICE_PROFILE = "LOAD_DEVICE_PROFILE";
    public static final String LOAD_USER_PROFILE = "LOAD_USER_PROFILE";

    public static final String GEOFENCE_F_LATITUDE_KEY = "GEOFENCE_F_LATITUDE_KEY";
    public static final String GEOFENCE_F_LONGITUDE_KEY = "GEOFENCE_F_LONGITUDE_KEY";
    public static final String GEOFENCE_F_RADIUS_KEY = "GEOFENCE_F_RADIUS_KEY";
    public static final String GEOFENCE_F_ZOOM_LEVEL_KEY = "GEOFENCE_F_ZOOM_LEVEL_KEY";
    public static final String START_DATE = "START_DATE";
    public static final String END_DATE = "END_DATE";


    public static final int REQUEST_CODE_FIRST_STEP_ACTIVITY = 12;
    public static final int RESULT_CODE_FIRST_STEP_ACTIVITY = 15;

    public static final int REQUEST_CODE_DEVICE_SETTINGS = 16;
    public static final int RESULT_CODE_DEVICE_SETTINGS = 19;

    public static final int REQUEST_CODE_ADD_EMIAL = 17;
    public static final int RESULT_CODE_ADD_EMAIL = 21;

    public static final int REQUEST_CODE_SMS_NOTIFICATION = 18;
    public static final int RESULT_CODE_SMS_NOTIFICATION = 20;


    public static final String LOAD_GEOFENCE_LIST = "LOAD_GEOFENCE_LIST";

    public static final String SMS_NOTIFICATION_DATA_KEY = "SMS_NOTIFICATION_DATA_KEY";
    public static final String EMAIL_NOTIFICATION_DATA_KEY = "EMAIL_NOTIFICATION_DATA_KEY";
    public static final String APP_NOTIFICATION_DATA_KEY = "APP_NOTIFICATION_DATA_KEY";
    public static final String SHARE_TRACKER_EMAIL_LIST = "SHARE_TRACKER_EMAIL_LIST";

    public static final int SUCCESS_STATUS_CODE = 200;

    public static final String DEVICE_TYPE = "android";
    public static final String USER_TYPE = "1";
    public static final int SUCCESS = 200;

    public static final String NOTIFICATION_MSG_KEY = "NOTIFICATION_MSG_KEY";
    public static final String LOAD_SHARED_DEVICE_LIST = "LOAD_SHARED_DEVICE_LIST";
    public static final String NOTIFICATION_CHANNEL_ID = "NOTIFICATION_CHANNEL_ID";
    public static final int NOTIFICATION_ID = 5;


    //tracking mode values
    public static final String KIDS_TRACKING_MODE_VAL = "1";
    public static final String ELDER_TRACKING_MODE_VAL = "2";
    public static final String VEHICLES_TRACKING_MODE_VAL = "3";
    public static final String PETS_TRACKING_MODE_VAL = "4";
    public static final String TRAVEL_TRACKING_MODE_VAL = "5";
    public static final String OTHERS_TRACKING_MODE_VAL = "6";

    //terms & condition url
    public static final String TERMS_CONDITION_URL = "https://nuttag.com.au/pages/seek-tracker-terms-of-service-and-privacy-policy";

    //help center url
    public static final String HELP_CENTER_URL = "https://www.nuttag.com.au/help";

    //privacy policy
    public static final String PRIVACY_POLICY_URL = "https://nuttag.com.au/pages/seek-tracker-terms-of-service-and-privacy-policy";

    //web portal login
    public static final String WEB_PORTAL_LOGIN_URL = "https://staging.nuttag.com.au/login";


}
