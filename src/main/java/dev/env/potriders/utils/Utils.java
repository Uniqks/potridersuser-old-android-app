package dev.env.potriders.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dev.env.potriders.R;
import dev.env.potriders.app.Potrider;

public class Utils {

    Context context;
    static Utils utils;
    SharedPreferences pref;
    Editor edit;

    public static LocationManager locationManager;



    public static final boolean SHOULD_PRINT_LOG = true;

    public static final String KEY_IS_LOGGED_IN = "isLoggedIn";

    public static final String USER_ID = "user_id";

    public static final String USER_APIKEY = "api_key";
    public static final String USER_FNAME = "user_fname";
    public static final String USER_LNAME = "user_lname";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PIC = "user_pic";

    public static final String USER_PASSWORD = "user_pwd";
    public static final String USER_MOBILE = "user_mobile";


    public static final String USER_LAT = "user_lat";
    public static final String USER_LNG = "user_lng";


    public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public static Geocoder geocoder = null;
    public static Timer taxiAroundTimer = null;


    public static final boolean IS_DEBUG = true;

    public static final String EXTRA_IMG_LIST = "extra_img_list";
    public static final String EXTRA_SEARCH_LIST = "extra_search_list";
    public static final String EXTRA_SELECTED_SEARCH_POS = "extra_selected_search_pos";
    public static final String EXTRA_SEARCH_TITLE = "extra_search_title";
    public static final String TITLE_BRAND = "Brand";
    public static final String TITLE_TYPE = "Type";
    public static final String TITLE_WEIGHT = "Weight";
    public static final String TITLE_MORE_OPTIONS = "More Options";


    public static final int REQUEST_CODE_SEARCH_BRAND_WEED = 0001;
    public static final int REQUEST_CODE_SEARCH_TYPE_WEED = 0002;
    public static final int REQUEST_CODE_SEARCH_WEIGHT_WEED = 0003;
    public static final int REQUEST_CODE_SEARCH_MORE_OPTIONS_WEED = 0004;
    public static final int REQUEST_CODE_SEARCH_BRAND_FOOD = 0005;
    public static final int REQUEST_CODE_SEARCH_TYPE_FOOD = 0006;
    public static final int REQUEST_CODE_SEARCH_WEIGHT_FOOD = 0007;
    public static final int REQUEST_CODE_SEARCH_BRAND_WEED_ADVANCED = 0010;
    public static final int REQUEST_CODE_SEARCH_TYPE_WEED_ADVANCED = 0011;
    public static final int REQUEST_CODE_SEARCH_WEIGHT_WEED_ADVANCED = 0012;

    // IS_DEBUG = false --> Live
    // IS_DEBUG = true --> Osco


    public Utils(Context context) {
        this.context = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        edit = pref.edit();
    }


    public synchronized static Geocoder getGeocoderInstance(Context context) {
        if (geocoder == null) {
            geocoder = new Geocoder(context);
        }
        return geocoder;
    }


    public void setBoolean(String key, boolean value) {
        edit.putBoolean(key, value);
        edit.commit();
    }

    public boolean getBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    public void setString(String key, String value) {
        edit.putString(key, value);
        edit.commit();
    }

    public String getString(String key) {
        return pref.getString(key, "");
    }

    public void setInt(String key, int value) {
        edit.putInt(key, value);
        edit.commit();
    }

    public int getInt(String key) {
        return pref.getInt(key, 0);
    }

    public void setFloat(String key, float value) {
        edit.putFloat(key, value);
        edit.commit();
    }

    public float getFloat(String key) {
        return pref.getFloat(key, 0);
    }


    public void setLong(String key, long value) {
        edit.putLong(key, value);
        edit.commit();
    }

    public long getLong(String key) {
        return pref.getLong(key, 0);
    }

    public void clearPref() {

        edit.clear();
        edit.commit();
    }


    public static int getConnectivityStatus(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static boolean isNetworkAvailable(Context context) {

        int conn = getConnectivityStatus(context);

        if (conn == TYPE_WIFI) {
            //status = "Wifi enabled";
            //status="Internet connection available";
            return true;
        } else if (conn == TYPE_MOBILE) {
            //status = "Mobile data enabled";
            //status="Internet connection available";
            return true;
        } else if (conn == TYPE_NOT_CONNECTED) {
            //status = "Not connected to Internet";
            return false;
        }
        return false;
    }
	
    public void Toast(final Activity activity, final String msg, final boolean type) {

        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), msg,
                Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        if (type) {
            snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        } else {
            snackBarView.setBackgroundColor(Color.RED);
        }
        snackbar.show();

    }


    public boolean isValidMail(String email2) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();

	    /*if(!check)
	    {
	        txtEmail.setError("Not Valid Email");
	    }*/
        return check;
    }



    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) Potrider.getGlobalContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }



    public static void clearInstance() {
        utils = null;
        locationManager = null;
    }

    public synchronized static Utils getInstance(Context context) {
        if (utils == null) {
            utils = new Utils(Potrider.getGlobalContext());
        }
        return utils;
    }

    public File create_Image_File(String ext)
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+ context.getString(R.string.app_name));
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        // String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(fmt.format(now) + ext);

        return mediaFile;
    }

    public File create_file(String ext)
    {
        // File folderPath = new File(Environment.getExternalStorageDirectory() +"/"+ getString(R.string.app_name));
        File shareQr = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!shareQr.exists()) {
            shareQr.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        File newFile =null;//=  File.createTempFile( fmt.format(now) + ".png",shareQr);

        try {
            newFile= File.createTempFile(fmt.format(now).toString(),  /* prefix */ext,         /* suffix */shareQr      /* directory */);
            LogUtils.e(" newFile "+ newFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFile;
    }

}
