package dev.env.potriders.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import dev.env.potriders.R;
import dev.env.potriders.app.PreferenceKeys;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okio.Buffer;


/**
 * Created by Seema on 3/11/18.
 */

//Common Methods
public class CommonUtils {

    private static CommonUtils mInstance;
    public static ProgressDialog dialog;

    public synchronized static CommonUtils getInstance() {
        if (mInstance == null)
            mInstance = new CommonUtils();
        return mInstance;
    }


    public static final Pattern pwdpattern = Pattern.compile("(?=.*\\d)(?=.*[A-Z]).{6,20}$");

    public static void showLoader(Context context) {
        if (dialog == null) {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Please wait");
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

        }
        dialog.show();
    }

    public static void showLoader(Context context, String message) {
        if (dialog == null) {
            dialog = new ProgressDialog(context);
            dialog.setMessage(message);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

        } else {
            dialog.setMessage(message);
        }
        dialog.show();
    }

    public static String getImageUrl(Context context) {
        String url = "";
        url = PreferenceUtil.getPref(context).getString(PreferenceKeys.IMAGE_URL, "");
        return url;
    }

  /*  public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }*/

    public static void hideLoader() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean isInternetAvailable(Context context) {

        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            // test for connection
            if (cm.getActiveNetworkInfo() != null
                    && cm.getActiveNetworkInfo().isAvailable()
                    && cm.getActiveNetworkInfo().isConnected()) {
                return true;
            } else {
                Log.e("", "Internet Connection Not Available");
                return false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static String getDateFromTimestamp(long timeStamp, String dateFormat) {

        try {
            DateFormat sdf = new SimpleDateFormat(dateFormat);
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getConvertedDate(String oldDateFormat, String newDateFormat, String dates) {
        String dateConverted = "";
        try {

            SimpleDateFormat df = new SimpleDateFormat(oldDateFormat);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date dateNew = df.parse(dates);
            SimpleDateFormat df2 = new SimpleDateFormat(newDateFormat);
            df2.setTimeZone(TimeZone.getDefault());
            dateConverted = df2.format(dateNew);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateConverted;
    }

    public static String getFormattedDateOfStay(String oldDateFormat, String dates) {

        String convertedDates = "";
        Date dateNew;
        SimpleDateFormat df = new SimpleDateFormat(oldDateFormat);
        try {
            dateNew = df.parse(dates);

            Calendar cal = Calendar.getInstance();
            cal.setTime(dateNew);
            //2nd of march 2015
            int day = cal.get(Calendar.DATE);

            switch (day) {
                case 1:
                case 21:
                case 31:
                    return new SimpleDateFormat("MMMM d'st', yyyy").format(dateNew);
                case 2:
                case 22:
                    return new SimpleDateFormat("MMMM d'nd', yyyy").format(dateNew);
                case 3:
                case 23:
                    return new SimpleDateFormat("MMMM d'rd', yyyy").format(dateNew);
                default:
                    return new SimpleDateFormat("MMMM d'th', yyyy").format(dateNew);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedDates;
    }


    public static String getCurrentTime(String format) {
        String currentTime = "";

        if (format != null) {
            long currentMilli = System.currentTimeMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(currentMilli);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            currentTime = simpleDateFormat.format(calendar.getTime());

            return currentTime;
        } else {
            return currentTime;
        }

    }


    public static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    public static int getCurrentHour() {
        int hour = 0;
        Calendar cal = Calendar.getInstance(); //Create Calendar-Object
        cal.setTime(new Date());               //Set the Calendar to now
        hour = cal.get(Calendar.HOUR_OF_DAY); //Get the hour from the calendar

        return hour;
    }

    public static String getTwoDecimalValue(Double values) {

        String resultString = "";
        try {
            //String twopointDemialValue = String.format("%.2f", values);
            NumberFormat format = NumberFormat.getCurrencyInstance();
            DecimalFormatSymbols dfs = new DecimalFormatSymbols();
            dfs.setCurrencySymbol("$");
            format.setMinimumFractionDigits(2);
            ((DecimalFormat) format).setDecimalFormatSymbols(dfs);
            resultString = format.format(values);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultString;
    }

    public static int getCurrentMinute() {
        int minutes = 0;
        Calendar cal = Calendar.getInstance(); //Create Calendar-Object
        cal.setTime(new Date());               //Set the Calendar to now
        minutes = cal.get(Calendar.MINUTE); //Get the hour from the calendar

        return minutes;
    }

    public static int getCurrentSecond() {
        int second = 0;
        Calendar cal = Calendar.getInstance(); //Create Calendar-Object
        cal.setTime(new Date());               //Set the Calendar to now
        second = cal.get(Calendar.SECOND); //Get the hour from the calendar

        return second;
    }

    public static String getCurrentTimeInDesiredFormat(String formatToGet) {
        try {
            String str = "";
            //return 24 hour format as kk is used
            SimpleDateFormat sdf = new SimpleDateFormat(formatToGet);
            str = sdf.format(new Date());
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void makeToast(String message, Context mContext) {
        try {
            Toast toast = Toast.makeText(mContext, "" + message, Toast.LENGTH_LONG);
            View view = toast.getView();
            //view.setBackgroundResource(R.drawable.toast_bg);
            TextView text = (TextView) view.findViewById(android.R.id.message);
            //text.setTextColor(getColor(mContext, R.color.white));
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getIncresedTimeByOneSec(String date, String time) {
        String newDateTime = "";
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        try {
            Date currentDate = dateFormat.parse(date + " " + time);
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.MINUTE, 1);
            newDateTime = dateFormat.format(cal.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //get current date time with Calendar()
        return newDateTime;
    }

    public static String getTwoDigits(int value) {
        String hourString = "";
        hourString = value < 10 ? "0" + value : "" + value;
        return hourString;
    }

    public static void sout(String object) {
        System.out.println("" + object);
    }

    public static int getPixelValue(Context context, int dp) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, resources.getDisplayMetrics());
    }

    public static String convertTimeToTwelveHFmt(String timeToConvert) {
        try {
            final String time = timeToConvert;

            try {
                final SimpleDateFormat sdf24H = new SimpleDateFormat("kk");
                final SimpleDateFormat sdf12H = new SimpleDateFormat("h:mm a");
                final Date dateObj = sdf24H.parse(time);

                // Log.d("con t: ","cov t: "+sdf12H.format(dateObj));
                return sdf12H.format(dateObj);
            } catch (final ParseException e) {
                e.printStackTrace();
                return "";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    public static boolean isInternetConnected(Context context) {
        boolean isConnected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            return isConnected;
        } catch (Exception e) {
            e.printStackTrace();
            return isConnected;
        }
    }

    public static final int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static String getStringText(Context context, int textToget) {
        String textToReturn = "";
        try {
            textToReturn = context.getResources().getString(textToget);
            return textToReturn;
        } catch (Exception e) {
            e.printStackTrace();
            return textToReturn;
        }

    }

    public static Drawable getDrawables(Context context, int drawables) {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return context.getResources().getDrawable(drawables, null);
            } else {
                return context.getResources().getDrawable(drawables);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static Date convertTimeToDate(String times) {
        Date dtToReturn = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("kk:mm");
            try {
                Date date = format.parse(times);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                dtToReturn = calendar.getTime();

                return dtToReturn;
            } catch (ParseException e) {

                e.printStackTrace();
                return dtToReturn;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return dtToReturn;
        }
    }


    /*public static void sout(Object object) {
        try {
            System.out.println("" + object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static OkHttpClient getOkHttpClient() {

        /*HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);*/


        return new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                /*.addInterceptor(logging)*/
                .build();
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    public static PackageInfo appVersionNameandCode(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return pInfo;
    }

    public static void cancelAllNotifications(Context context) {
        try {
            // Clear all notification

            NotificationManager nMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            nMgr.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





  /*  //serach screen top auto rotate  images
    public static void setImageGlideSearchImages(Context context, String image, ImageView imageView) {
        try {
            Glide.with(context).
                    load(image).
                    crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.search_top_bg)
                    .error(R.drawable.search_top_bg)
                    .crossFade()
                    //.animate(R.anim.fadeinloadimage)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
/*
    public static void setImageGlide(Context context, String image, ImageView imageView) {
        try {
            Glide.with(context).
                    load(image.trim()).
                    apply(new RequestOptions().
                            placeholder(R.mipmap.ic_launcher)
                            .error(R.mipmap.ic_launcher)
                            .fitCenter()
                            .dontAnimate())
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

/*
    public static void setImageGlideProfile(Context context, String image, ImageView imageView) {
        try {
            Glide.with(context).
                    load(image.trim()).
                    apply(new RequestOptions().
                            placeholder(R.drawable.profile)
                            .error(R.drawable.profile)
                            .fitCenter()
                            .dontAnimate())
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


  /*  public static void setImageGlideOfflineMode(Context context, String image, final ImageView imageView) {
        try {
            Glide.with(context).asBitmap().apply(new RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).
                    override(300, 300)).load(image.trim()).into(new BaseTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    imageView.setImageBitmap(resource);

                    Bitmap bitmapToSet = CommonUtils.toGrayscale(resource);
                    if (bitmapToSet != null) {
                        imageView.setImageBitmap(bitmapToSet);
                    }

                }

                @Override
                public void getSize(SizeReadyCallback cb) {
                }

                @Override
                public void removeCallback(SizeReadyCallback cb) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public static int getScreenWidth() {
        int screenWidth = 0;
        try {
            screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return screenWidth;
    }

    public static String getCalendarScreenDate(Date date, String dateFormat) {
        String mDate = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            mDate = sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDate;
    }

   /* public static int getScreenSize(Context context) {
        return context.getResources().getInteger(R.integer.screen_size);
    }*/

    public static void openKeyBoard(Context mContext, View mEditText) {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* //Expandable Text view

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore
            , final Context mContext) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore, mContext), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + "... " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore, mContext), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore, mContext), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }*/

  /*  private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore
            , final Context mContext) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        //makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, CommonUtils.getStringText(mContext, R.string.view_more_text), true, mContext);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }*/


    public static void strikeThroughText(TextView textView) {
        try {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void setPorgressDialogIntermediateColor(ProgressDialog progressDialog, Context context) {
        try {

            Drawable drawable = new ProgressBar(context).getIndeterminateDrawable().mutate();
            drawable.setColorFilter(ContextCompat.getColor(context, R.color.colorAccent),
                    PorterDuff.Mode.SRC_IN);
            progressDialog.setIndeterminateDrawable(drawable);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setErrorMessage(EditText editText, String message) {
        try {
            if (editText != null) {
                editText.requestFocus();
                editText.setError(message);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getValidString(String textToValidate) {
        String validatedString = "";
        try {
            if (textToValidate != null
                    && !TextUtils.isEmpty(textToValidate)
                    && textToValidate.trim().length() > 0) {
                validatedString = textToValidate;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return validatedString;
    }

    public static void openCallDialer(Context mContext, String numberToDial) {
        String number = "";
        try {
            if (isValidString(numberToDial)) {
                number = numberToDial;
            }
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            mContext.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getValidStrWithNullTxt(String textToValidate) {
        String validatedString = "";
        try {
            if (textToValidate != null &&
                    !TextUtils.isEmpty(textToValidate)
                    && textToValidate.trim().length() > 0
                    && !textToValidate.toLowerCase().equalsIgnoreCase("null")
                    ) {
                validatedString = textToValidate;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return validatedString;
    }


    public static boolean isValidString(String textToValidate) {

        try {
            if (textToValidate != null &&
                    !TextUtils.isEmpty(textToValidate)
                    && textToValidate.trim().length() > 0
                    ) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

   /* public static boolean isValidStringWithNull(String textToValidate) {

        try {
            if (textToValidate != null &&
                    !TextUtils.isEmpty(textToValidate)
                    && textToValidate.trim().length() > 0
                    && !textToValidate.toLowerCase().equalsIgnoreCase("null")
                    ) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean isValidLanguageWithTrue(String textToValidate) {

        try {
            if (textToValidate != null &&
                    !TextUtils.isEmpty(textToValidate)
                    && textToValidate.trim().length() > 0
                    && !textToValidate.toLowerCase().equalsIgnoreCase("null")
                    && !textToValidate.toLowerCase().equalsIgnoreCase("false")
                    ) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }



    public static boolean isValidStringWithNullZero(String textToValidate) {

        try {
            if (textToValidate != null &&
                    !TextUtils.isEmpty(textToValidate)
                    && textToValidate.trim().length() > 0
                    && !textToValidate.toLowerCase().equalsIgnoreCase("null")
                    && !textToValidate.equalsIgnoreCase("0")
                    ) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
*/
    /*public static String getStrWithDollarSign(String str) {
        //default value
        String strDollarAppended = "$0";
        try {
            if (isValidStringWithNull(str)) {
                strDollarAppended = "$" + str;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strDollarAppended;
    }

    public static String getStrWithPercentage(String str) {
        //default value
        String strDollarAppended = "";
        try {
            if (isValidStringWithNull(str)) {
                strDollarAppended = str + "%";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strDollarAppended;
    }*/





    public static void hideHeaderViewBelowL(View views) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            views.setVisibility(View.GONE);
        }
    }

    public static String formatPhoneNumber(String unformattedNumber) {
        String formattedNumber = "";
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                formattedNumber = PhoneNumberUtils.formatNumber(unformattedNumber, Locale.getDefault().getCountry());
            } else {
                formattedNumber = PhoneNumberUtils.formatNumber(unformattedNumber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedNumber;
    }


    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        try {
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                if (bitmapDrawable.getBitmap() != null) {
                    return bitmapDrawable.getBitmap();
                }
            }
            if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_4444); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }



    /*public static LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            if (address.size() > 0) {
                Address location = address.get(0);
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }*/

    //hide keyboard
    public static void hideKeyboard(Activity activity) {
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                view.clearFocus();
                InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isValidParams(String parameterName, JSONObject jsonObject) {
        boolean validParamsToRet = false;
        if (!jsonObject.isNull(parameterName)) {
            validParamsToRet = true;
        } else {
            validParamsToRet = false;
        }

        return validParamsToRet;
    }

    public static String getNotificationOverSpeedEmailValueAddDevice(String stringToVerify) {
        String stringToReturn = "N";
        stringToReturn = stringToVerify.equalsIgnoreCase("") ? "N" : stringToVerify;
        return stringToReturn;
    }

    public static boolean isValidPhoneNumber(String stringToVerify) {
        boolean valueToReturn = false;
        if (isValidString(stringToVerify) && stringToVerify.trim().length() >= 10) {
            //valid phone number
            valueToReturn = true;
        } else {
            //invalid
            valueToReturn = false;
        }

        return valueToReturn;
    }

    public static Bitmap toGrayscale(Bitmap bmpOriginal) {
        Bitmap bmpGrayscale = null;
        try {
            int width, height;
            height = bmpOriginal.getHeight();
            width = bmpOriginal.getWidth();

            bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bmpGrayscale);
            Paint paint = new Paint();
            ColorMatrix cm = new ColorMatrix();
            cm.setSaturation(0);
            ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
            paint.setColorFilter(f);
            c.drawBitmap(bmpOriginal, 0, 0, paint);
            return bmpGrayscale;
        } catch (Exception e) {
            e.printStackTrace();
            return bmpGrayscale;
        }
    }

    public static boolean isValidContactNumber(EditText editText) {
        boolean isValidNUmberToReturn = false;
        String value = editText.getText().toString().trim();
        if (value != null && value.length() < 10) {
            isValidNUmberToReturn = false;
        } else {
            isValidNUmberToReturn = true;
        }

        return isValidNUmberToReturn;
    }

    public static void openUrlInBrowser(Context mContext, String url) {
        if (isValidString(url)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            mContext.startActivity(intent);
        }
    }


    public static String localToUTC(String dateFormat, String datesToConvert) {


        String dateToReturn = datesToConvert;

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeZone.getDefault());
        Date gmt = null;

        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat(dateFormat);
        sdfOutPutToSend.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {

            gmt = sdf.parse(datesToConvert);
            dateToReturn = sdfOutPutToSend.format(gmt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }


    public static String uTCToLocal(String dateFormatInPut, String dateFomratOutPut, String datesToConvert) {


        String dateToReturn = datesToConvert;

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormatInPut);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date gmt = null;

        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat(dateFomratOutPut);
        sdfOutPutToSend.setTimeZone(TimeZone.getDefault());

        try {

            gmt = sdf.parse(datesToConvert);
            dateToReturn = sdfOutPutToSend.format(gmt);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }


    public static String replace(String target) {

        String replacement = null;
        if (target.equals("Large")) {
            replacement = "L";
        } else if (target.equals("Medium")) {
            replacement = "M";

        } else if (target.equals("Small")) {
            replacement = "S";
        }

        return replacement;
    }


}
