package dev.env.potriders.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Azim Ansari on 11/3/2015.
 * Utility for secure preference
 */
public class PreferenceUtil {

    private static final String PREFERENCE_FILE = "SmartQ";
    private static final String PASSWORD = "SmartQ";
    private static SharedPreferences preferences;

    public synchronized static SharedPreferences getPref(Context context) {
        if (preferences == null)
            preferences = new ObscuredSharedPreferences(context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE), PASSWORD);
        return preferences;
    }
}