package dev.env.potriders.retrofit;

import java.util.List;
import java.util.Map;

import dev.env.potriders.model.Address;
import dev.env.potriders.model.CommonModel;
import dev.env.potriders.model.CategoryDetails;
import dev.env.potriders.model.WeedDetail;
import dev.env.potriders.model.ProductDetails;
import dev.env.potriders.model.WeightModel;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by MTPC76 on 10/31/2017.
 */

public interface ApiInterface {


    @FormUrlEncoded
    @POST("login.php")
    Call<String> userLogin(@FieldMap Map<String, String> paramsMap);

    @FormUrlEncoded
    @POST("register.php")
    Call<String> userRegister(@FieldMap Map<String, String> map);

    @Multipart
    @POST("register.php")
    Call<String> userRegisterWithImage(@Part("fname") String fname,
                                       @Part("lname") String lname,
                                       @Part("email") String email,
                                       @Part("password") String password,
                                       @Part("userType") String userType,
                                       @Part("phone") String phone,
                                       @Part("deviceToken") String deviceToken,
                                       @Part("deviceType") String deviceType,
                                       @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("forgot.php")
    Call<String> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("update_profile.php")
    Call<String> updateProfile(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("change_password.php")
    Call<String> changePassword(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("pages.php")
    Call<String> getPages(@Field("page") int page);

    @FormUrlEncoded
    @POST("cat_list.php")
    Call<CommonModel<List<CategoryDetails>>> getCategoryList(@Field("type") int type);

    @POST("unit_list.php")
    Call<CommonModel<List<WeightModel>>> getWeightList();

    @FormUrlEncoded
    @POST("item_list.php")
    Call<CommonModel<List<ProductDetails>>> getWeedItemList(@Field("type") int type);

    @FormUrlEncoded
    @POST("item_detail.php")
    Call<CommonModel<WeedDetail>> getWeedDetail(@Field("item_id") int item_id);

    @FormUrlEncoded
    @POST("address_list")
    Call<CommonModel<List<Address>>> getAddressList(@Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("contacts")
    Call<String> submitContactUsData(@FieldMap Map<String, String> map);

    @Multipart
    @POST("update_profile.php")
    Call<String> updateProfileWithImage(@Part("user_id") String user_id,
                                        @Part("fname") String fname,
                                        @Part("lname") String lname,
                                        @Part("email") String email,
                                        @Part("phone") String phone,
                                        @Part MultipartBody.Part image);


}
