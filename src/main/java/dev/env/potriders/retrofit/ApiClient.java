package dev.env.potriders.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by MTPC76 on 10/31/2017.
 */

public class ApiClient {

    public static final String BASE_URL = "http://potriders.com/admin/api/";
    private static Retrofit retrofit = null;
    private static Retrofit retrofit1 = null;


    public static Retrofit getStringClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        if (retrofit1 == null) {
            retrofit1 = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(new OkHttpClient.Builder()
                            .addInterceptor(loggingInterceptor)
                            .connectTimeout(700, TimeUnit.SECONDS)
                            .readTimeout(700, TimeUnit.SECONDS).build())
                    .addConverterFactory(new ToStringConverterFactory())
                    .build();
        }
        return retrofit1;
    }


    public static Retrofit getClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(new OkHttpClient.Builder()
                            .addInterceptor(loggingInterceptor)
                            .connectTimeout(700, TimeUnit.SECONDS)
                            .readTimeout(700, TimeUnit.SECONDS).build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
